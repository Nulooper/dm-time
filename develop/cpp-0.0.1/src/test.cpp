#include "IR.h"
#include <fstream>
#include <time.h>

using namespace std;

void readLines(const string& filename, vector<string>& lines)
{
    lines.clear();
    ifstream fin;
    fin.open(filename.c_str(), std::ifstream::in);
    string line;
    while(getline(fin, line))
    {
        lines.push_back(line);
    }
    fin.close();
}

void writeLines(const string& filename, const vector<string>& lines)
{
    ofstream fout;
    fout.open(filename.c_str(), std::ofstream::out | std::ofstream::app);
    int size = lines.size();
    for(int i = 0; i < size; ++i)
    {
        fout << lines[i];
    }
    fout.close();
}

void test(const vector<string>& test_days) 
{
    vector<string> unknown_lines;
    readLines("final_rules.txt", unknown_lines);
    int size = unknown_lines.size();
    vector<string> results;
    std::vector<string> items;
    for(int i = 0; i < size; ++i)
    {
        string combine_result_line = "" + unknown_lines[i] + "\n";
        int days_size = test_days.size();
        for(int j = 0; j < days_size; ++j)
        {
            string out_times;  
            IR::search(unknown_lines[i], test_days[j], out_times);
            combine_result_line += test_days[j]+ ": " + out_times +"\n" ;
        }
        results.push_back(combine_result_line);
    }
    writeLines("unknown.results", results);
}


int main()
{
    std::cout << "Tesing---" << std::endl;
    vector<string> test_days;
    for(int i = 1; i <= 10; ++i)
    {
        test_days.push_back(IR::int2String(20140712 + i));
        test_days.push_back(IR::int2String(20140113 + i));
    }
    clock_t start, finish;
    double total_time;
    start = clock();


    std::cout << "Testing---" << endl;
    test(test_days);
    //testTimeRule(test_days);
    
    //testWeekTimeRule(test_days);
    
    //testMonthTimeRule(test_days);
    std::cout << "---OK!" << endl;


    finish = clock();
    total_time = (double)(finish - start) / CLOCKS_PER_SEC;
    cout << "程序的运行时间为:[" << total_time << "]秒!" << endl;
}
