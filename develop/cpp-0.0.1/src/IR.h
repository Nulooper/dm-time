/////////////////////////////////////////////////////////////
/*Filename   : IR.h                                        */
/*Create Date: 2014-07-04                                  */
/*Author     : Wang Zhiwei 王志伟                           */
/*Version	 : 0.0.1                                       */

/*changed	 : 2014-08-16 by Wang Zhiwei 王志伟             */
/*reversion  : 0.0.2                                       */
/////////////////////////////////////////////////////////////

#ifndef _IR_H_
#define _IR_H_

#include <iostream>
#include <string>
#include <map>
#include <unordered_map>
#include <vector>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

using namespace std;

const static std::string RETURN_CLOSE = "";//描述NO类型规则:如果关闭，则返回空串.
const static std::string RETURN_UNKNOWN = "NULL";//未知景点默认开门时间.
const static std::string RETURN_OPEN = "00:00-24:00";//ALWAYS类型规则默认时间.

const static std::string OPEN_CLOSE_DELIM = "|";//规则中open_rules和close_rules之间的分隔符.
const static std::string OPEN_RULE_DELIM = "--";//open_rules中多种类型之间的分隔符.
const static std::string RULE_TYPE_DELIM = "&";//规则与类型之间的分隔符.

const static std::string TYPE_DAY_TIME = "DAY_TIME_RULE";//优先级:5
const static std::string TYPE_MONTH_WEEK_TIME = "MONTH_WEEK_TIME_RULE";//优先级:4
const static std::string TYPE_WEEK_TIME = "WEEK_TIME_RULE";//优先级:3
const static std::string TYPE_MONTH_TIME = "MONTH_TIME_RULE";//优先级:2
const static std::string TYPE_ONLY_TIME = "TIME_RULE";//优先级:1


const int mon_day[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};//每月的天数.

class IR
{
public:
    static bool search(const std::string& rule, const std::string& in_date, std::string& out_times);
    
public:
    //处理DAY_TIME_RULE
    static bool checkDayTimeRule(const std::string& in_rule, const std::string& in_date, std::string& out_times);
    //处理MONTH_WEEK_TIME_RULE
    static bool checkMonthWeekTimeRule(const std::string& in_rule, const std::string& in_date, std::string& out_times);
    //处理WEEK_TIME_RULE
    static bool checkWeekTimeRule(const std::string& in_rule, const std::string& in_date, std::string& out_times);
    //处理MONTH_TIME_RULE
    static bool checkMonthTimeRule(const std::string& in_rule, const std::string& in_date, std::string& out_times);
    //处理TIME_RULE
    static bool checkTimeRule(const std::string& in_rule, const std::string& in_date, std::string& out_times);

public:
	static void vector2String(const std::vector<std::string>& time_pair_vec, const std::string& delim, std::string& out_str);
    static void split(const std::string& in_str, const std::string& delim, std::vector<std::string>& out_vec);
    static std::string int2String(int in_number);
    static int string2int(const std::string& in_str);
    static std::string getStrBetween(std::string in_str, std::string left, bool is_left_forward, std::string right, bool is_right_forward);
    static bool isInInterval(const std::string& in_number_str, const std::string& interval_left_str, const std::string& interval_right_str);

    static int getWDay(const time_t& t, int zone = 8);
    static time_t toTime(const std::string& s, int zone = 8);
    static int compareDayStr(const std::string& a,const std::string& b);
    static std::string getDayOfWeek(const std::string& in_date);
};

bool IR::search(const std::string& rule, const std::string& in_date, std::string& out_times)
{
	std::vector<std::string> rule_items;
	split(rule, OPEN_CLOSE_DELIM, rule_items);
	if(rule_items.size() != 2)
	{
		out_times = RETURN_UNKNOWN;
		return false;
	}

	//step1. 首先检索close_rules, 如果命中，直接返回结果.
	//NOTE: close_rules只有两种类型:NO, DAY_TIME_RULE.
	std::vector<std::string> close_items;
	split(rule_items[1], RULE_TYPE_DELIM, close_items);
	std::string close_rule = close_items[0];
	std::string close_type = close_items[1];
	std::string normalized_date = in_date.substr(4);
	if(TYPE_DAY_TIME == close_type)
	{
		close_rule = getStrBetween(close_rule, "{", true, "}", false);
		std::vector<std::string> black_day_vec;
	    split(close_rule, ",", black_day_vec);
	    for(std::vector<std::string>::iterator iter = black_day_vec.begin(); iter != black_day_vec.end(); ++iter)
	    {
	    	if(normalized_date == *iter)
	    	{
	    		out_times = RETURN_CLOSE;
	    		return true;
	    	}
	    }
	}
	
	//step2. 检索open_rules. 不用考虑close_rules(step1已经解决).
	std::vector<std::string> open_rules;
	split(rule_items[0], OPEN_RULE_DELIM, open_rules);
	std::map<std::string, std::string> opentype_rule_map;
	for(std::vector<std::string>::iterator iter = open_rules.begin(); iter != open_rules.end(); ++iter)
	{
		std::vector<std::string> tmp_vec;
		split(*iter, OPEN_RULE_DELIM, tmp_vec);
		opentype_rule_map[tmp_vec[0]] = tmp_vec[1];
	}

	//根据优先级依次进行时间规则检索.
	std::unordered_map<std::string, std::string>::const_iterator end_iter = opentype_rule_map.end();
	if (opentype_rule_map.find("ALWAYS") != end_iter)
	{
		out_times = RETURN_OPEN;
		return true;
	}
	else if (opentype_rule_map.find("NO") != end_iter)
	{
		out_times = RETURN_CLOSE;
		return true;
	}
	else if (opentype_rule_map.find("UNKNOWN") != end_iter)
	{
		out_times = RETURN_UNKNOWN;
		return true;
	}
	else if (opentype_rule_map.find(TYPE_DAY_TIME) != end_iter)
	{
		if(checkDayTimeRule(opentype_rule_map[TYPE_DAY_TIME], in_date, out_times))
		{
			return true;
		}
	}
	else if (opentype_rule_map.find(TYPE_MONTH_WEEK_TIME) != end_iter)
	{
		if (checkMonthWeekTimeRule(opentype_rule_map[TYPE_MONTH_WEEK_TIME], in_date, out_times))
		{
			return true;
		}
	}
	else if (opentype_rule_map.find(TYPE_WEEK_TIME) != end_iter)
	{
		if (checkWeekTimeRule(opentype_rule_map[TYPE_WEEK_TIME], in_date, out_times))
		{
			return true;
		}
	}
	else if (opentype_rule_map.find(TYPE_MONTH_TIME) != end_iter)
	{
		if (checkMonthTimeRule(opentype_rule_map[TYPE_MONTH_TIME], in_date, out_times))
		{
			return true;
		}
	}
	else if (opentype_rule_map.find(TYPE_ONLY_TIME) != end_iter)
	{
		if (checkTimeRule(opentype_rule_map[TYPE_ONLY_TIME], in_date, out_times))
		{
			return true;
		}
	}
	else
	{
		out_times = RETURN_UNKNOWN;
		return false;
	}

	out_times = RETURN_UNKNOWN;
	return false;
}

/********************************************//**
 * \brief 处理DAY_TIME_RULE; 检索输入日期是否在给定的in_rule中. 如果在，则返回对应的时间.
 *
 * \param in_rule 字符串, 格式为:{0515-0930:06:00:00-22:00:00},{1001-1231:06:00:00-21:30:00}
 * \param in_date 字符串, 格式为:20140915
 * \param out_times 字符串, 格式为:09:00-12:00,13:00-15:00
 * \return bool 如果能够检索到返回true, 否则返回false.
 *
 ***********************************************/
bool IR::checkDayTimeRule(const std::string& in_rule, const std::string& in_date, std::string& out_times)
{
	std::string normalized_date = in_date.substr(4);

	std::vector<std::string> time_pair_vec;
	split(in_rule, ",", time_pair_vec);
	std::string time_pair, left_date, right_date;
	for(std::vector<std::string>::iterator iter = time_pair_vec.begin(); iter != time_pair_vec.end(); ++iter)
	{
		left_date = getStrBetween(*iter, "{", true, "-", true);
		right_date = getStrBetween(*iter, "-", true, ":", true);
		if ((compareDayStr(left_date, normalized_date) <= 0) && (compareDayStr(normalized_date, right_date) <= 0))
		{
			out_times = getStrBetween(*iter, ":", true, "}", false);
			return true;
		}
	}

	return false;
}

/********************************************//**
 * \brief 处理MONTH_WEEK_TIME_RULE; 检索输入日期是否在给定的in_rule中. 如果在，则返回对应的时间.
 *
 * \param in_rule 字符串, 格式为:{10-12_1-5:08:00:00-18:00:00},{1-2_1-5:08:00:00-18:00:00}
 * \param in_date 字符串, 格式为:20140915
 * \param out_times 字符串, 格式为:09:00-12:00,13:00-15:00
 * \return bool 如果能够检索到返回true, 否则返回false.
 *
 ***********************************************/
bool IR::checkMonthWeekTimeRule(const std::string& in_rule, const std::string& in_date, std::string& out_times)
{
	std::vector<std::string> vec;
	split(in_rule, ",", vec);
	std::string normalized_date = in_date.substr(4);
	std::string in_month = normalized_date.substr(0, normalized_date.length() / 2);
	std::string in_weekday = getDayOfWeek(in_date);

	std::string weekday_pair;
	std::string month_left, month_right;
	for(std::vector<string>::iterator iter = vec.begin(); iter != vec.end(); ++iter)
	{
		month_left = getStrBetween(*iter, "{", true, "-", true);
		month_right = getStrBetween(*iter, "-", true, "_", true);
		if (isInInterval(in_month, month_left, month_right))
		{
			weekday_pair = getStrBetween(*iter, "_", true, ":", true);
			std::vector<std::string> number_vec;
			split(weekday_pair, "-", number_vec);
			if (isInInterval(in_weekday, number_vec[0], number_vec[1]))
			{
				out_times = getStrBetween(*iter, ":", true, "}", false);
				return true;
			}
		}
	}

	return false;
}

/********************************************//**
 * \brief 处理WEEK_TIME_RULE; 检索输入日期是否在给定的in_rule中. 如果在，则返回对应的时间.
 *
 * \param in_rule 字符串, 格式为:{1-1:09:00-18:00},{2-2:09:00-18:00},{3-3:09:00-18:00},{4-4:09:00-18:00}
 * \param in_date 字符串, 格式为:20140915
 * \param out_times 字符串, 格式为:09:00-12:00,13:00-15:00
 * \return bool 如果能够检索到返回true, 否则返回false.
 *
 ***********************************************/
bool IR::checkWeekTimeRule(const std::string& in_rule, const std::string& in_date, std::string& out_times)
{
	std::string in_weekday = getDayOfWeek(in_date);
	std::vector<std::string> vec;
	split(in_rule, ",", vec);
	std::string weekday_left, weekday_right;
	for (std::vector<std::string>::iterator iter = vec.begin(); iter != vec.end(); ++iter)
	{
		weekday_left = getStrBetween(*iter, "{", true, "-", true);
		weekday_right = getStrBetween(*iter, "-", true, ":", true);
		if(isInInterval(in_weekday, weekday_left, weekday_right))
		{
			out_times = getStrBetween(*iter, ":", true, "}", false);
			return true;
		}
	}

	return false;
}

/********************************************//**
 * \brief 处理MONTH_TIME_RULE; 检索输入日期是否在给定的in_rule中. 如果在，则返回对应的时间.
 *
 * \param in_rule 字符串, 格式为:{2-2:09:00-18:00},{3-3:09:00-18:00},{4-4:09:00-18:00}
 * \param in_date 字符串, 格式为:20140915
 * \param out_times 字符串, 格式为:09:00-12:00,13:00-15:00
 * \return bool 如果能够检索到返回true, 否则返回false.
 *
 ***********************************************/
bool IR::checkMonthTimeRule(const std::string& in_rule, const std::string& in_date, std::string& out_times)
{
	std::string normalized_date = in_date.substr(4);
	std::string in_month = normalized_date.substr(0, normalized_date.size() / 4);
	std::vector<std::string> vec;
	split(in_rule, ",", vec);
	std::string month_left, month_right;
	for(std::vector<std::string>::iterator iter = vec.begin(); iter != vec.end(); ++iter)
	{	
		month_left = getStrBetween(*iter, "{", true, "-", true);
		month_right = getStrBetween(*iter, "-", true, ":", true);
		if (isInInterval(in_month, month_left, month_right))
		{
			out_times = getStrBetween(*iter, ":", true, "}", false);
			return true;
		}
	}
	return false;
}

/********************************************//**
 * \brief 处理TIME_RULE; 检索输入日期是否在给定的in_rule中. 如果在，则返回对应的时间.
 *
 * \param in_rule 字符串, 格式为:{09:00:00-17:00:00}
 * \param in_date 字符串, 格式为:20140915, 输入日期暂时没有使用.
 * \param out_times 字符串, 格式为:09:00-12:00,13:00-15:00
 * \return bool 如果能够检索到返回true, 否则返回false.
 *
 ***********************************************/
bool IR::checkTimeRule(const std::string& in_rule, const std::string& in_date, std::string& out_times)
{
	out_times = getStrBetween(in_rule, "{", true, "}", false);

	return true;
}

/********************************************//**
 * \brief 将给定字符串in_str按照分隔符delim进行分割，结果存储在out_vec中
 *
 * \param in_str 给定的字符串
 * \param delim 分隔符
 * \param out_vec 存储使用分隔符分割后的子串
 * \return void 
 *
 ***********************************************/
void IR::split(const std::string& in_str, const std::string& delim, std::vector<std::string>& out_vec)
{
	out_vec.clear();
    
    std::string::size_type pos = 0;
    std::string::size_type len = in_str.length();
    std::string::size_type delim_len = delim.length();
    if (0 == len || 0 == delim_len)
    {
    	return;
    }
    while (pos < len)
    {
        std::string::size_type find_pos = in_str.find(delim, pos);
        if (find_pos == -1)
        {
            out_vec.push_back(in_str.substr(pos, len - pos));
            break;
        }
        out_vec.push_back(in_str.substr(pos, find_pos - pos));
        pos = find_pos + delim_len;
    }
}

std::string IR::int2String(int in_number)
{
    std::stringstream ss;
	ss << in_number;
    std::string number_str;
	ss >> number_str;
    
	return number_str;
}

int IR::string2int(const std::string& in_str)
{
    std::stringstream ss;
	ss << in_str;
	int number;
	ss >> number;
    
	return number;
}

std::string IR::getStrBetween(std::string in_str, std::string left, bool is_left_forward, std::string right, bool is_right_forward)
{
	std::size_t left_index;
	if (is_left_forward)
	{
		left_index = in_str.find(left);
	}
	else
	{
		left_index = in_str.rfind(left);
	}

	std::size_t right_index;
	if (is_right_forward)
	{
		right_index = in_str.find(right);
	}
	else
	{
		right_index = in_str.rfind(right);
	}

	return in_str.substr(left_index + left.length(), right_index - left_index - right.length());
}

bool IR::isInInterval(const std::string& in_number_str, const std::string& interval_left_str, const std::string& interval_right_str)
{
	int in_number = string2int(in_number_str);
	int left = string2int(interval_left_str);
	int right = string2int(interval_right_str);

	return left <= in_number && in_number <= right;
}

time_t IR::toTime(const std::string& s, int zone)
{
	const char* s_c = s.c_str();
	char* pstr;
	long year, month, day, hour, min, sec;
	year = strtol(s_c, &pstr, 10);
	month = (year / 100) % 100;
	day = (year % 100);
	year = year / 10000;
	hour = min = sec = 0;
	if(*pstr)
	{
		hour = strtol(++pstr, &pstr, 10);
		if(*pstr)
		{
			min = strtol(++pstr, &pstr, 10);
			if (*pstr)
			{
				sec = strtol(++pstr, &pstr, 10);
			}
		}
	}
	//printf("%d %d %d\n", hour,min,sec);

    int leap_year = (year - 1969) / 4;	//year-1-1968
    int i = (year - 2001) / 100;		//year-1-2000
    leap_year -= ((i / 4) * 3 + i % 4);
    int day_offset = 0;
    for (i = 0; i < month - 1; i++)
    	day_offset += mon_day[i];
    bool isLeap = ((year % 4 == 0 && year % 100 != 0)||(year % 400 == 0));
    if (isLeap && month>2)
    	day_offset += 1;
    day_offset += (day-1);
    day_offset += ((year - 1970) * 365 + leap_year);
    int hour_offset = hour - zone;
    time_t ret = day_offset * 86400 + hour_offset * 3600 + min * 60 + sec;

    return ret;
}

/**
* 比较两个给定日期. 相等返回0,大于返回正值表示天数,小于返回负值表示天数. 
*
* a: 0814(月日)
* b: 0814(月日)
*/
int IR::compareDayStr(const std::string& a, const std::string& b)
{
	if (a == b)
		return 0;

	time_t a_t = toTime("2014" + a + "_00:00", 0);
	time_t b_t = toTime("2014" + b + "_00:00", 0);

	return (b_t - a_t) / 86400;
}

int IR::getWDay(const time_t& t, int zone)
{
	struct tm TM;
	time_t nt = t + zone * 3600;
	gmtime_r(&nt, &TM);
	int ret = (TM.tm_wday + 6) % 7 + 1;
	return ret;
}

std::string IR::getDayOfWeek(const std::string& in_date)
{
	
	time_t tt = IR::toTime(in_date+"_00:00");
    int wd = IR::getWDay(tt);

	return int2String(wd);
	//return "周1"; 
}

#endif