# -*- coding:utf-8 -*-

import sys
from datetime import date, time, datetime

reload(sys)
sys.setdefaultencoding('utf-8')

month_days = [u'1月31日', u'2月29日', u'3月31日', u'4月30日', u'5月31日', u'6月30日', u'7月31日', u'8月31日', u'9月30日', u'10月31日', u'11月30日', u'12月31日']



def combine(time_dict):
	"""对规范化后的景点时间规则字典根据类型进行合并.

	测试通过.
	"""
	rule_time_dict = {}
	#合并ST, ST_ST
	print 'Begin to combine_2_ST---'
	rule_time_dict['ST'] = combine_2_ST(time_dict)
	print '---OK!'

	#合并M_ST,SM,SM_ST,SM_ST_SM_ST,M
	print 'Begin to combine_2_M_ST---'
	rule_time_dict['M_ST'] = combine_2_M_ST(time_dict)
	print '---OK!'

	#合并ST_WAC,SW,SW_ST,SW_ST_ST,SW_ST_WAC,W,W_ST,W_ST_ST,W_SW_ST
	print 'Begin to combine_2_W_ST---'
	rule_time_dict['W_ST'] = combine_2_W_ST(time_dict)
	print '---OK!'

	#合并SD，SD_ST,​DAC,D_M_ST,D_ST,M_D_ST
	print 'Begin to combine_2_SD_ST---'
	rule_time_dict['SD_ST'], black_day_list = combine_2_SD_ST(time_dict)
	print '---OK!'

	#对类型SM_SW_ST不做任何合并
	print 'Begin to process SM_SW_ST---'
	if time_dict.has_key('SM_SW_ST'):
		rule_time_dict['SM_SW_ST'] = time_dict['SM_SW_ST']
	print '--OK!'

	black_num_day_list = []
	for day in black_day_list:
		black_num_day_list.append(daystr_to_daynumstr(day))

	return rule_time_dict, black_num_day_list


def combine_2_ST(time_dict):
	"""将仅包含时间类型合并为ST类型.

	测试通过.
	"""

	if time_dict.has_key('ST') and time_dict.has_key('ST_ST'):
		return time_dict['ST_ST'][:1]

	elif time_dict.has_key('ST'):
		return time_dict['ST'][:1]

	elif time_dict.has_key('ST_ST'):
		return time_dict['ST_ST'][:1]

	else:
		return []

def combine_2_M_ST(time_dict):
	"""将仅包含月时间的各种类型合并为M_ST类型.

	测试通过.
	"""

	m_anyST_dict = {}

	if time_dict.has_key('M'):
		values = time_dict['M']
		for v in values:
			month = int(v[:v.find('月')])
			m_anyST_dict[month] = str(month) + '_09:00:00-18:00:00'

	elif time_dict.has_key('M_ST'):
		values = time_dict['M_ST']
		for v in values:
			month = int(v[:v.find('月')])
			m_anyST_dict[month] = str(month) + '_' + v.split('_')[1]

	elif time_dict.has_key('SM'):
		values = time_dict['SM']
		for v in values:
			m_items = v.split('-')
			l_num = int(m_items[0][:m_items[0].find('月')])
			r_num = int(m_items[1][:m_items[1].find('月')])
			for i in range(l_num, r_num + 1):
				m_anyST_dict[i] = str(i) + '_09:00:00-18:00:00'

	elif time_dict.has_key('SM_ST'):
		values = time_dict['SM_ST']
		for v in values:
			v_items = v.split('_')
			t_part = v_items[1]
			m_part = v_items[0]
			m_items = m_part.split('-')
			l_num = int(m_items[0][:m_items[0].find('月')])
			r_num = int(m_items[1][:m_items[1].find('月')])
			for i in range(l_num, r_num + 1):
				m_anyST_dict[i] = str(i) + '_' + t_part

	elif time_dict.has_key('SM_ST_SM_ST'):
		values = time_dict['SM_ST_SM_ST']
		for v in values:
			v_items = v.split('_')
			sm1 = v_items[0]
			t1 = v_items[1]
			sm1_items = sm1.split('-')
			sm1_l_num = int(sm1_items[0][:sm1_items[0].find('月')])
			sm1_r_num = int(sm1_items[1][:sm1_items[1].find('月')])
			for i in range(sm1_l_num, sm1_r_num + 1):
				m_anyST_dict[i] = str(i) + '_' + t1
			sm2 = v_items[2]
			t2 = v_items[3]
			sm2_items = sm2.split('-')
			sm2_l_num = int(sm2_items[0][:sm2_items[0].find('月')])
			sm2_r_num = int(sm2_items[1][:sm2_items[1].find('月')])
			for i in range(sm2_l_num, sm2_r_num + 1):
				m_anyST_dict[i] = str(i) + '_' + t2
	
	return m_anyST_dict.values()

def combine_2_W_ST(time_dict):
	"""将仅含有周时间的各种类型合并为W_ST类型.

	测试通过.
	"""

	w_any_t_dict = {}
	black_list = set()
	if time_dict.has_key('ST_WAC'):
		values = time_dict['ST_WAC']
		for v in values:
			v_items = v.split('_')
			black_w = int(v_items[1])
			black_list.add(black_w)
			t_part = v_items[0]
			for i in range(1, black_w):
				w_any_t_dict[i] = str(i) + '_' + t_part
			for i in range(black_w + 1, 7):
				w_any_t_dict[i] = str(i) + '_' + t_part
				
	elif time_dict.has_key('SW'):
		values = time_dict['SW']
		for v in values:
			v_items = v.split('-')
			for i in range(int(v_items[0]), int(v_items[1]) + 1):
				w_any_t_dict[i] = str(i) + '_' + '09:00:00-18:00:00'

	elif time_dict.has_key('SW_ST'):
		values = time_dict['SW_ST']
		for v in values:
			v_items = v.split('_')
			w_part = v_items[0]
			t_part = v_items[1]
			w_items = w_part.split('-')
			l_num = int(w_items[0])
			r_num = int(w_items[1])
			for i in range(l_num, r_num + 1):
				w_any_t_dict[i] = str(i) + '_' + t_part

	elif time_dict.has_key('SW_ST_ST'):
		values = time_dict['SW_ST_ST']
		for v in values:
			index = v.find('_')
			w_part = v[:index]
			t_part = v[index + 1:]
			w_items = w_part.split('-')
			l_num = int(w_items[0])
			r_num = int(w_items[1])
			for i in range(l_num, r_num + 1):
				w_any_t_dict[i] = str(i) + '_' + t_part

	elif time_dict.has_key('SW_ST_WAC'):
		values = time_dict['SW_ST_WAC']
		for v in values:
			v_items = v.split('_')
			w_part = v_items[0]
			t_part = v_items[1]
			black_list.add(int(v_items[2]))
			w_items = w_part.split('-')
			l_num = int(w_items[0])
			r_num = int(w_items[1])
			for i in range(l_num, r_num + 1):
				w_any_t_dict[i] = str(i) + '_' + t_part

	elif time_dict.has_key('W_ST'):
		values = time_dict['W_ST']
		for v in values:
			v_items = v.split('_')
			w_any_t_dict[int(v_items[0])] = v_items[0] + '_' + v_items[1]

	elif time_dict.has_key('W'):
		values = time_dict['W']
		for v in values:
			w_any_t_dict[int(v)] = v + '_' + '09:00:00-18:00:00'

	elif time_dict.has_key('W_ST_ST'):
		values = time_dict['W_ST_ST']
		for v in values:
			index = v.find('_')
			w_part = v[:index]
			t_part = v[index:]
			w_any_t_dict[int(w_part)] = w_part + '_' + t_part

	elif time_dict.has_key('W_SW_ST'):
		values = time_dict['W_SW_ST']
		for v in values:
			v_items = v.split('_')
			w_part = v_items[0]
			sw_part = v_items[1]
			sw_items = sw_part.split('-')
			t_part = v_items[2]
			w_any_t_dict[int(w_part)] = w_part + '_' + t_part
			l_num = int(sw_items[0])
			r_num = int(sw_items[1])
			for i in range(l_num, r_num + 1):
				w_any_t_dict[i] = str(i) + '_' + t_part

	for w in black_list:
		if w_any_t_dict.has_key(w):
			del w_any_t_dict[w]

	return w_any_t_dict.values()

def combine_2_SD_ST(time_dict):



	any_sd_dict = {}
	black_set = set()
	if time_dict.has_key('SD'):
		values = time_dict['SD']
		for v in values:
			any_sd_dict[v] = v + '_09:00:00-18:00:00'

	elif time_dict.has_key('SD_ST'):
		values = time_dict['SD_ST']
		for v in values:
			v_items = v.split('_')
			sd_part = v_items[0]
			st_part = v_items[1]
			check_and_add(sd_part, any_sd_dict, st_part)

	elif time_dict.has_key('DAC'):
		values = time_dict['DAC']
		for v in values:
			black_set.add(v)

	elif time_dict.has_key('D_M_ST'):
		values = time_dict['D_M_ST']
		for v in values:
			v_items = v.split('_')
			dm_part = v_items[0]
			dm_items = dm_part.split('-')
			d_part = dm_items[0]
			m_part = dm_items[1]
			st_part = v_items[1]
			sd = d_part + '-' + month_to_day(m_part)
			check_and_add(sd, any_sd_dict, st_part)

	elif time_dict.has_key('D_ST'):
		values = time_dict['D_ST']
		for v in values:
			v_items = v.split('_')
			d_part = v_items[0]
			sd = d_part + '-' + d_part
			t_part = v_items[1]
			check_and_add(sd, any_sd_dict, t_part)

	elif time_dict.has_key('M_D_ST'):
		values = time_dict['M_D_ST']
		for v in values:
			print 'M_D_ST:', v
			v_items = v.split('_')
			md_part = v_items[0]
			md_items = md_part.split('-')
			m_part = md_items[0]
			d_part = md_items[1]
			t_part = v_items[1]
			sd = month_to_day(m_part) + '-' + d_part
			check_and_add(sd, any_sd_dict, t_part)


	return any_sd_dict.values(), list(black_set)

#仅作为combine_2_SD_ST的辅助函数
def check_and_add(sd_part, in_out_dict, st_part):
	"""
	"""
	isOverlap = False
	for key in in_out_dict.keys():
		if isDateIntervalOverlap(sd_part, key, 'SD'):
			isOverlap = True
			break
	if not isOverlap:
		in_out_dict[sd_part] = sd_part + '_' + st_part



######################################-combine########################################

def isDateIntervalOverlap(interval1, interva12, grain):

	items1 = interval1.split('-')
	items2 = interva12.split('-')

	return isIntervalOverlap(items1[0], items1[1], items2[0], items2[1], grain)

def isIntervalOverlap(left1, right1, left2, right2, grain):
	"""判断区间[left1, right1]和区间[left2, right2]是否相交"""

	if grain == 'SM':
		l_num1 = int(left1[:left1.find('月')])
		r_num1 = int(right1[:right1.find('月')])
		l_num2 = int(left2[:left2.find('月')])
		r_num2 = int(right2[:right2.find('月')])

		return not(r_num1 < l_num2 or r_num2 < l_num1)

	elif grain == 'SD':

		return isDateOverlap(day_to_date(left1), day_to_date(right1), 
			day_to_date(left2), day_to_date(right2))

	elif grain == 'SDM':
		r_num1 = month_to_day(l_num1)
		r_num2 = month_to_day(l_num2)
		return isDateOverlap(l_num1, r_num1, l_num2, r_num2)

	elif grain == 'SMD':
		l_num1 = month_to_day(l_num1)
		l_num2 = month_to_day(l_num2)
		return isDateOverlap(l_num1, r_num1, l_num2, r_num2)

	else:
		return False

def isDateOverlap(l_date1, r_date1, l_date2, r_date2):
	"""判断两个日期区间是否相交"""

	return not(compare_date(r_date1, l_date2) or compare_date(r_date2, l_date1))

def compare_date(d1, d2):
	"""比较两个日期的大小

	Args:
		d1: date类型
		d2: date类型

	Returns:
	    若d1小于d2，则返回True， 否则返回False.
	"""
	return d1 < d2

def day_to_date(day):
	"""将字符串表示的日期day转为date对象.

	测试通过.
	"""

	year, month, day = get_y_m_d(day)
	if day < 1 or day > 12:
		day = 15

	return date(year, month, day)

def date_to_day(in_date):
	"""将date对象转换为字符串形式

	格式为XX月XX日
	"""
	month = in_date.month
	day = in_date.day
	
	return str(month) + '月' + str(day) + '日'

def daystr_to_daynumstr(in_day):
	"""将形如9月15日规范化为0915.

	"""
	year, month, day = get_y_m_d(in_day)
	month_str = str(month)
	if month < 10:
		month_str = '0' + str(month)
	day_str = str(day)
	if day < 10:
		day_str = '0' + str(day)

	return month_str + day_str

def get_y_m_d(d):
	""""获取年月日

	Args:
		dt: 形如02月15日

	Returns:
	    year: 年
	    month: 月
	    day: 日
	"""
	now = date.today()
	year = now.year

	month_index = d.find('月')
	month = int(d[:month_index])
	day_index = d.find('日')
	day = int(d[month_index - day_index : day_index])

	return year, month, day

def isTimeOverlap(l_time1, r_time1, l_time2, r_time2):
	""""判断两个时间区间是否相交."""

	return not(compare_time(r_time1, l_time2) or compare_time(r_time2, l_time1))

def compare_time(t1, t2):
	"""比较两个时间的大小."""

	h1, m1, s1 = get_h_m_s(t1)
	time1 = time(h1, m1, s1)
	h2, m2, s2 = get_h_m_s(t2)
	time2 = time(h2, m2, s2)

	return time1 < time2

def get_h_m_s(t):
	"""获取时，分钟，秒"""

	items= t.split(':')

	return int(items[0]), int(items[1]), int(items[2])

def month_to_day(month):
	"""从月转换到天

	例如: 3月转换为3月30日
	"""
	month_number = int(month[:month.find('月')])

	return month_days[month_number - 1]


if __name__ == "__main__":
	print '__main__'