=========说明文档=======
1. 当前目录下文件和文件夹说明
    combine.py, rule_creator.py, transform.py: 三个脚本文件.
    README.txt: 说明文件.
    example: 该目录下的文件是PatternSystem生成的中间文件.

2. 脚本执行
    启动脚本是rule_creator.py, 该脚本将PatternSystem生成的中间文件(如example目录下的case_result.txt)转换成最后需要存入数据库中的景点规则文件.

3. 脚本执行示例
    python rule_creator.py ./example/case_result.txt ./final_rules.txt