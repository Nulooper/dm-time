# -*- coding:utf-8 -*-

import sys
import os
import combine
import transform
reload(sys)
sys.setdefaultencoding('utf-8')

OPEN_CLOSE_DELIM = '|'
RULE_TYPE_DELIM = '&'
OPEN_RULE_DELIM = '--'

UNKNOWN_RULE = 'UNKNOWN' + RULE_TYPE_DELIM + 'UNKNOWN' + OPEN_CLOSE_DELIM + 'UNKNOWN' + RULE_TYPE_DELIM + 'UNKNOWN'
AC_RULE = 'NO' + RULE_TYPE_DELIM + 'NO' + OPEN_CLOSE_DELIM + 'ALWAYS' + RULE_TYPE_DELIM + 'ALWAYS'
AO_RULE = 'ALWAYS' + RULE_TYPE_DELIM + 'ALWAYS' + OPEN_CLOSE_DELIM + 'NO' + RULE_TYPE_DELIM + 'NO'

def batch_create_rules(spot_list):
	"""根据景点时间信息列表批量创建最终的开关门时间规则.


	测试通过.
	"""
	final_rules = []
	i = 0
	for spot in spot_list:
		if spot:
			i = i + 1
			print 'Begin to transform ', i, 'spot!', '|'.join(spot)
			time_dict = transform.create_dict(spot)
			final_rules.append(create_rule(time_dict) + '\n')
			print i, 'OK!'
	return final_rules


def create_rule(time_dict):
	"""针对一个景点的时间字典信息，创建最终的时间开放规则.

	字典中只有可能ST,M_ST,W_ST,SD_ST和SM_SW_ST几种类型

	"""
	if time_dict is None or len(time_dict) == 0:
		return UNKNOWN_RULE

	if time_dict.has_key('AC'):
		return AC_RULE

	if time_dict.has_key('AO'):
		return AO_RULE

	combined_dict, black_day_list = combine.combine(time_dict)
	candidate_open_rules = []
	if len(combined_dict) == 0:
		return UNKNOWN_RULE

	else:
		if combined_dict.has_key('ST'):
			st_rules = []
			for st in combined_dict['ST']:
				st_rules.append(','.join(st.split('_')))
			if len(st_rules) != 0:
				candidate_open_rules.append('{' + (','.join(st_rules)) + '}' + RULE_TYPE_DELIM + 'TIME_RULE')

		if combined_dict.has_key('W_ST'):
			w_st_rules = []
			for w_st in combined_dict['W_ST']:
				items = w_st.split('_')
				w = items[0]
				st = items[1]
				w_st_rules.append('{' + str(w) + '-' + str(w) + ':' + st + '}')
			if len(w_st_rules) != 0:
				candidate_open_rules.append(','.join(w_st_rules) + RULE_TYPE_DELIM + 'WEEK_TIME_RULE')

		if combined_dict.has_key('M_ST'):
			m_st_rules = []
			for m_st in combined_dict['M_ST']:
				items = m_st.split('_')
				m = items[0]
				st = items[1]
				m_st_rules.append('{' + str(m) + '-' + str(m) + ':' + st + '}')
			if len(m_st_rules) != 0:
				candidate_open_rules.append(','.join(m_st_rules) + RULE_TYPE_DELIM + 'MONTH_TIME_RULE')

		if combined_dict.has_key('SD_ST'):
			sd_st_rules = []
			for sd_st in combined_dict['SD_ST']:
				items = sd_st.split('_')
				sd = items[0]
				####规范日期格式为:9月15日===>0915
				sd_items = sd.split('-')
				sd = combine.daystr_to_daynumstr(sd_items[0]) + '-' + combine.daystr_to_daynumstr(sd_items[1])

				st = items[1]
				sd_st_rules.append('{' + sd + ':' + st + '}')
			if len(sd_st_rules) != 0:
				candidate_open_rules.append(','.join(sd_st_rules) + RULE_TYPE_DELIM + 'DAY_TIME_RULE')

		if combined_dict.has_key('SM_SW_ST'):
			sm_sw_st_rules = []
			for sm_sw_st in combined_dict['SM_SW_ST']:
				index = sm_sw_st.rfind('_')
				sm_sw_part = sm_sw_st[:index]
				sm_sw_part = sm_sw_part.replace('月', '')
				sm_sw_part = sm_sw_part.replace('周', '')
				st_part = sm_sw_st[index + 1:]
				sm_sw_st_rules.append('{' + sm_sw_part + ':' + st_part + '}')
			if len(sm_sw_st_rules) != 0:
				candidate_open_rules.append(','.join(sm_sw_st_rules) + RULE_TYPE_DELIM + 'MONTH_WEEK_TIME_RULE')

	close_rule = ''
	if len(black_day_list) == 0:
		close_rule = close_rule + 'NO' + RULE_TYPE_DELIM + 'NO'
	else:
		close_rule = close_rule + ('{' + ','.join(black_day_list) + '}' + RULE_TYPE_DELIM + 'DAY_TIME_RULE')

	return OPEN_RULE_DELIM.join(candidate_open_rules) + OPEN_CLOSE_DELIM + close_rule

def main():

	if(len(sys.argv) != 3):
		print "\t Please enter following args:\n"
		print "\t input_file_path: input file path created by PatternSystem."
		print "\t output_rule_path: rule output file path."
		print "Example:\t python rule_creator.py ./case_result.txt ./final_rules.txt"
		return

	input_path = sys.argv[1]
	output_path = sys.argv[2]

	print 'Begin to read input file.'
	spot_list = transform.read_by_spot(input_path)
	print 'Read input file OK!'

	print 'Begin to batch create rules.'
	final_rules = batch_create_rules(spot_list)
	print 'batch create rules OK!'

	print 'Begin to write final_rules.'
	output_file = open(output_path, 'w')
	output_file.writelines(final_rules)
	output_file.close()
	print 'Write final rules OK!'

if __name__ == '__main__':
	main()


