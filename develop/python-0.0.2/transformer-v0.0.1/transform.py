# -*- coding: utf-8 -*-

import sys
import os
import combine
reload(sys)
sys.setdefaultencoding('utf-8')

#季度的默认时间
#NOTE: 由于很难根据日期获取该日期所处的季度,所以对季度时间使用默认值.
#默认值选择标准:
SEASOM_DEFAULT_TIME = '10:00:00-15:00:00'

month_days = ['1月31日', '2月29日', '3月31日', '4月30日', '5月31日', '6月30日', '7月31日', '8月31日', '9月30日', '10月31日', '11月30日', '12月31日']

#step1: 读入PatternSystem输出文件, 存入列表.
def read_by_spot(input_path):
	"""读入文件，返回一个列表，列表的元素表示一个景点所有可能的时间文本，
	   元素类型也是列表list.
	
	Args:
	    input_path: PatternSystem生成的文件路径.
	
	Returns:
	    返回一个列表list, 其中每一个元素也是一个列表list.
	"""
	input_file = open(input_path, 'r')
	spot_list = []
	spot = []
	for line in input_file:
		line = line.strip()
		if len(line) == 0:
			continue
		if line.startswith('INPUT'):
			spot_list.append(spot)
			spot = []
		else:
			if line.startswith('\"time_span'):
				if line.endswith(','):
					line = line[:-1]
				spot.append(line)
	spot_list.append(spot)
	input_file.close()

	return spot_list[1:]

def create_dict(spot):
	"""spot为列表类型，每一个元素时如下格式的字符串:
	"time_span_84000_1" : "ST:09:00:00-19:00:00"

	测试通过.
	"""
	print 'Begin to create dict:'
	#step1: 构造类型时间文本字典
	print 'step1---'
	tmp_dict = {}
	for rule_text in spot:
		
		key, value = split_key_value(rule_text)
		if tmp_dict.has_key(key):
			tmp_dict[key].append(value)
		else:
			values = []
			values.append(value)
			tmp_dict[key] = values
	print '---OK!'
	
	#########规范化###################

	#step2: 对字典进行时间类型规约
	#例如, M_M, M_M_M, M_M_M_M规约到M类型
	print 'step2---'
	reduce_to_M('M_M', tmp_dict)
	reduce_to_M('M_M_M', tmp_dict)
	reduce_to_M('M_M_M_M', tmp_dict)
	print '---OK!'

	#step3: 对W_ST类型时间规约
	#例如: W_W_ST, W_W_W_ST, W_W_W_W_ST类型规约到W_ST类型
	print 'step3---'
	reduce_to_W_ST('W_W_ST', tmp_dict)
	reduce_to_W_ST('W_W_W_ST', tmp_dict)
	reduce_to_W_ST('W_W_W_W_ST', tmp_dict)
	print '---OK!'

	#step4: 对S_ST类型时间进行规范化
	#例如: spring,autumn_10:00:00-18:00:00规范化为:
				#autumn_10:00:00-18:00:00
				#spring_10:00:00-18:00:00
	print 'step4---'
	normalize_S_ST(tmp_dict)
	print '---OK!'

	#step5: 对S类型时间进行规范化
	#例如: summer,autumn规范化为:
			#summer
			#autumn
	print 'step5---'
	normalize_S(tmp_dict)
	print '---OK!'

	#step6: 对S_ST_ST类型时间进行规范化
	#例如: summer,spring_09:30:00-13:00:00_14:00:00-19:00:00规范化为:
		#summer_09:30:00-13:00:00_14:00:00-19:00:00
		#spring_09:30:00-13:00:00_14:00:00-19:00:00
	print 'step6---'
	normalize_S_ST_ST(tmp_dict)
	print '---OK!'

	#step7: 对SW_ST类型时间进行规范化
	print 'step7---'
	normalize_SW_ST(tmp_dict)
	print '---OK!'

	#step8: 对S_SW_ST类型时间进行规范化
	print 'step8---'
	normalize_S_SW_ST(tmp_dict)
	print '---OK!'

	#step9: 对SW_ST_WAC类型时间进行规范化
	print 'step9---'
	normalize_SW_ST_WAC(tmp_dict)
	print '---OK!'

	#step10: 对SW类型时间进行规范化
	print 'step10---'
	normalize_SW(tmp_dict)
	print '---OK!'

	#step11: 对SM类型时间进行规范化
	print 'step11---'
	normalize_SM(tmp_dict)
	print '---OK!'

	#step12: 对SD类型时间进行规范化 
	print 'step12---'
	normalize_SD(tmp_dict)
	print '---OK!'

	#step13: 对SW_ST_ST类型时间进行规范化
	print 'step13---'
	normalize_SW_ST_ST(tmp_dict)
	print '---OK!'

	#step14: 对SM_SW_ST类型时间进行规范化
	print 'step14---'
	normalize_SM_SW_ST(tmp_dict)
	print '---OK!'

	#step15: 对SM_ST类型时间进行规范化
	print 'step15---'
	normalize_SM_ST(tmp_dict)
	print '---OK!'

	#step16: 对SD_ST类型时间进行规范化
	print 'step16---'
	normalize_SD_ST(tmp_dict)
	print '---OK!'

	return tmp_dict

def reduce_to_M(input_type, in_out_dict):
	"""将m_type规约到M类型.

	测试通过.
	"""

	if in_out_dict.has_key(input_type):
		values = in_out_dict[input_type]
		for value in values:
			items = value.split('_')
			if in_out_dict.has_key('M'):
				for item in items:
					in_out_dict['M'].append(item)

		#从字典中删除input_type项
		in_out_dict.pop(input_type)

def reduce_to_W_ST(input_type, in_out_dict):
	"""将input_type规约到W_ST类型.

	测试通过.
	"""
	if in_out_dict.has_key(input_type):
		values = in_out_dict[input_type]
		for value in values:
			items = value.split('_')
			w_items = items[:-1]
			if in_out_dict.has_key('W_ST'):
				for w_item in w_items:
					in_out_dict['W_ST'].append(w_item + '_' + items[-1])

		#从字典中删除input_type项
		in_out_dict.pop(input_type)

def normalize_S_ST(in_out_dict):
	"""对S_ST类型时间进行规范化.

	具体规范化操作如下:
	将形如spring,autumn_10:00:00-18:00:00规范化为:
	autumn_10:00:00-18:00:00
	spring_10:00:00-18:00:00

	测试通过.
	"""
	"""
	if in_out_dict.has_key('S_ST'):
		values = in_out_dict['S_ST']
		to_remove = []
		for value in values:
			if ',' in value:
				value_items = value.split('_')
				s_items = value_items[0].split(',')
				for s in s_items:
					values.append(s + '_' + value_items[1])
				to_remove.append(value)

		for item in to_remove:
			values.remove(item)"""
	if in_out_dict.has_key('S_ST'):
		if in_out_dict.has_key('ST'):
			in_out_dict['ST'].append(SEASOM_DEFAULT_TIME)
		else:
			values = ['09:00-18:00']
			in_out_dict['ST'] = values
		del in_out_dict['S_ST']

def normalize_S_ST_ST(in_out_dict):
	"""对S_ST_ST类型时间进行规范化.

	具体规范化操作如下:
	将形如summer,spring_09:30:00-13:00:00_14:00:00-19:00:00规范化为:
	summer_09:30:00-13:00:00_14:00:00-19:00:00
	spring_09:30:00-13:00:00_14:00:00-19:00:00

	测试通过.
	"""
	"""
	if in_out_dict.has_key('S_ST_ST'):
		values = in_out_dict['S_ST_ST']
		to_remove = []
		for value in values:
			if ',' in value:
				index = value.find('_')
				s_part = value[:index]
				t_part = value[index + 1:]
				s_items = s_part.split(',')
				for s in s_items:
					values.append(s + '_' + t_part)
				to_remove.append(value)

		for item in to_remove:
			values.remove(item)"""
	if in_out_dict.has_key('S_ST_ST'):
		if in_out_dict.has_key('ST'):
			in_out_dict['ST'].append(SEASOM_DEFAULT_TIME)
		else:
			values = [SEASOM_DEFAULT_TIME]
			in_out_dict['ST'] = values
		del in_out_dict['S_ST_ST']

def normalize_S_SW_ST(in_out_dict):
	"""对S_SW_ST类型时间进行规范化.

	具体规范化操作如下:
	将形如summer,spring_7-4_10:00:00-21:00:00规范化为:
	summer_7-7_10:00:00-21:00:00
	summer_1-4_10:00:00-21:00:00
	spring_7-7_10:00:00-21:00:00
	spring_1-4_10:00:00-21:00:00

	测试通过.
	"""
	"""
	if in_out_dict.has_key('S_SW_ST'):
		values = in_out_dict['S_SW_ST']
		print 'values:', values
		to_remove = []
		for value in values:
			index1 = value.find('_')
			index2 = value.rfind('_')
			sw_part = value[index1 + 1 : index2]
			s_part = value[:index1]
			t_part = value[index2 + 1:]

			sw_items = sw_part.split('-')
			l_num1 = int(sw_items[0])
			r_num1 = int(sw_items[1])
			#step1: 规范化S, 如果sw_part有问题，再规范化sw
			if ',' in s_part:
				s_items = s_part.split(',')
				tmp_value1 = s_items[0] + "_" + sw_part + "_" + t_part
				tmp_value2 = s_items[1] + "_" + sw_part + "_" + t_part

				
				if l_num1 > r_num1:
					tmp_value1 = s_items[0] + '_' + str(l_num1) + '-' + '7' + '_' + t_part
					tmp_value2 = s_items[0] + '_' + '1' + '-' + str(r_num1) + '_' + t_part
					tmp_value3 = s_items[1] + '_' + str(l_num1) + '-' + '7' + '_' + t_part
					tmp_value4 = s_items[1] + '_' + '1' + '-' + str(r_num1) + '_' + t_part
					values.append(tmp_value1)
					values.append(tmp_value2)
					values.append(tmp_value3)
					values.append(tmp_value4)
					to_remove.append(value)
				else:
					values.append(tmp_value1)
					values.append(tmp_value2)
					to_remove.append(value)

			elif l_num1 > r_num1:
				tmp_value1 = s_part + '_' + str(l_num1) + '-' + '7' + '_' + t_part
				tmp_value2 = s_part + '_' + '1' + '-' + str(r_num1) + '_' + t_part
				values.append(tmp_value1)
				values.append(tmp_value2)
				to_remove.append(value)

		for item in to_remove:
			values.remove(item)"""
	if in_out_dict.has_key('S_SW_ST'):
		if in_out_dict.has_key('ST'):
			in_out_dict['ST'].append(SEASOM_DEFAULT_TIME)
		else:
			values = [SEASOM_DEFAULT_TIME]
			in_out_dict['ST'] = values
		del in_out_dict['S_SW_ST']


def normalize_S(in_out_dict):
	"""对S类型时间进行规范化.

	具体规范化操作如下:
	将形如summer,autumn规范化为:
	summer
	autumn

	测试通过.
	"""
	"""
	if in_out_dict.has_key('S'):
		values = in_out_dict['S']
		to_remove = []
		for value in values:
			if ',' in value:
				s_items = value.split(',')
				for s in s_items:
					values.append(s)
				to_remove.append(value)

		for item in to_remove:
			values.remove(item)"""
	if in_out_dict.has_key('S'):
		if in_out_dict.has_key('ST'):
			in_out_dict['ST'].append(SEASOM_DEFAULT_TIME)
		else:
			values = [SEASOM_DEFAULT_TIME]
			in_out_dict['ST'] = values
		del in_out_dict['S']


def normalize_SW_ST(in_out_dict):
	"""对SW_ST类型时间进行规范化.

	具体规范化操作如下:
	将形如6-4_10:00:00-17:50:00规范化为:
	6-7_10:00:00-17:50:00
	1-4_10:00:00-17:50:00

	测试通过.
	"""
	#print 'in_out_dict', in_out_dict
	if in_out_dict.has_key('SW_ST'):
		values = in_out_dict['SW_ST']
		to_remove = []
		for value in values:
			index = value.find('_')
			w_part = value[:index]
			t_part = value[index + 1:]
			items = w_part.split('-')
			l_num = int(items[0])
			r_num = int(items[1])
			if l_num > r_num:
				values.append(str(l_num) + '-' + '7' + '_' + t_part)
				values.append('1' + '-' + str(r_num) + '_' + t_part)
				to_remove.append(value)

		for item in to_remove:
			values.remove(item)

def normalize_SW_ST_WAC(in_out_dict):
	"""对SW_ST_WAC类型时间进行规范化

	具体规范化操作如下:
	将形如3-1_12:00:00-17:00:00_2规范为如下:
	3-7_12:00:00-17:00:00_2
	1-1_12:00:00-17:00:00_2

	测试通过.
	"""
	if in_out_dict.has_key('SW_ST_WAC'):
		values = in_out_dict['SW_ST_WAC']
		to_remove = []
		for value in values:
			index = value.find('_')
			sw_part = value[:index]
			t_part = value[index + 1:]
			sw_items = sw_part.split('-')
			l_num = int(sw_items[0])
			r_num = int(sw_items[1])
			if l_num > r_num:
				tmp_value1 = str(l_num) + '-' + '7' + '_' + t_part
				tmp_value2 = '1' + '-' + str(r_num) + '_' + t_part
				values.append(tmp_value1)
				values.append(tmp_value2)
				to_remove.append(value)

		for item in to_remove:
			values.remove(item)

def normalize_SW_ST_ST(in_out_dict):
	"""对SW_ST_ST类型时间进行规范化

	具体规范化操作如下:
	将形如5-3_09:30:00-12:00:00_14:00:00-18:00:00规范化为:
	5-7_09:30:00-12:00:00_14:00:00-18:00:00
	1-3_09:30:00-12:00:00_14:00:00-18:00:00

	"""
	if in_out_dict.has_key('SW_ST_ST'):
		values = in_out_dict['SW_ST_ST']
		to_remove = []
		for value in values:
			index = value.find('_')
			sw_part = value[:index]
			t_part = value[index + 1:]
			sw_items = sw_part.split('-')
			l_num = int(sw_items[0])
			r_num = int(sw_items[1])
			if l_num > r_num:
				tmp_value1 = str(l_num) + '-' + '7_' + t_part
				tmp_value2 = '1' + '-' + str(r_num) + '_' + t_part
				values.append(tmp_value1)
				values.append(tmp_value2)
				to_remove.append(value)

		for item in to_remove:
			values.remove(value)


def normalize_SW(in_out_dict):
	"""对SW类型时间进行规范化

	具体规范化操作如下:
	将形如6-4规范化为:
	6-7
	1-4
	"""
	if in_out_dict.has_key('SW'):
		values = in_out_dict['SW']
		to_remove = []
		for value in values:
			w_items = value.split('-')
			l_num = int(w_items[0])
			r_num = int(w_items[1])
			if l_num > r_num:
				tmp_value1 = str(l_num) + '-' + '7'
				tmp_value2 = '1' + '-' + str(r_num)
				values.append(tmp_value1)
				values.append(tmp_value2)
				to_remove.append(value)

		for item in to_remove:
			values.remove(item)

def normalize_SM(in_out_dict):
	"""对SM类型时间进行规范化, 同时对时间数字的合法性进行验证.

	具体规范化操作如下:
	将形如10月-4月规范化为:
	10月-12月
	1月-4月

	将形如1月-50587471月规范化为:
	1月-12月

	将形如19月-8月规范化为:
	1月-8月

	"""
	if in_out_dict.has_key('SM'):
		values = in_out_dict['SM']
		#print 'values:', values
		to_remove = []
		for value in values:
			m_items = value.split('-')
			l_num = int(m_items[0][:m_items[0].find('月')])
			r_num = int(m_items[1][:m_items[1].find('月')])


			if l_num > r_num:
				if l_num < 0 or l_num > 12:
					l_num = 1
				if r_num < 0 or r_num > 12:
					r_num = 12
				if l_num > r_num:					
					tmp_value1 = str(l_num) + '月-' + '12月'
					tmp_value2 = '1' + '月-' + str(r_num) + '月'
					values.append(tmp_value1)
					values.append(tmp_value2)
					to_remove.append(value)
				else:
					tmp_value = str(l_num) + '月-' + str(r_num) + '月'
					values.append(tmp_value)
					to_remove.append(value)
			else:
				if l_num < 0 or l_num > 12:
					l_num = 1
					if r_num < 0 or r_num > 12:
						tmp_value = str(l_num) + '月-' + str(r_num) + '月'
						values.append(tmp_value)
						to_remove.append(value)
				elif r_num < 0 or r_num > 12:
					r_num = 12
					tmp_value = str(l_num) + '月-' + str(r_num) + '月'
					values.append(tmp_value)
					to_remove.append(value)
				else:
					continue

		for item in to_remove:
			values.remove(item)

def normalize_SM_SW_ST(in_out_dict):
	"""将SM_SW_ST类型时间进行规范化

	具体规范化步骤如下:
	将形如11月-3月_6-3_09:30:00-16:30:00规范化为
	11月-12月_6-7_09:30:00-16:30:00
	11月-12月_1-3_09:30:00-16:30:00
	1月-3月_6-7_09:30:00-16:30:00
	1月-3月_1-3_09:30:00-16:30:00

	测试通过.
	"""

	if in_out_dict.has_key('SM_SW_ST'):
		values = in_out_dict['SM_SW_ST']
		to_remove = []
		for value in values:
			lindex = value.find('_')
			rindex = value.rfind('_')

			sm_part = value[:lindex]
			sm_items = sm_part.split('-')
			l_m_num = int(sm_items[0][:sm_items[0].find('月')])
			#处理月份数字特别大的情况, 如果大于12则取12，如果小于1则取1.
			#出现数字特别大的原因是PatternSystem识别出错.
			if l_m_num > 12:
				l_m_num = 12
			elif l_m_num < 1:
				l_m_num = 1
			r_m_num = int(sm_items[1][:sm_items[1].find('月')])
			if r_m_num > 12:
				r_m_num = 12
			elif r_m_num < 1:
				r_m_num = 1

			sw_part = value[lindex + 1 : rindex]
			sw_items = sw_part.split('-')
			l_w_num = int(sw_items[0])
			r_w_num = int(sw_items[1])

			t_part = value[rindex + 1 : ]

			if l_m_num > r_m_num:
				tmp_value1 = sm_items[0] + '-12月_' + sw_part + '_' + t_part
				tmp_value2 = '1月-' + sm_items[1] + '_' + sw_part + '_'+ t_part
				if l_w_num > r_w_num:
					tmp_sw_part1 = str(l_w_num) + '-7'
					tmp_sw_part2 = '1-' + str(r_w_num)
					tmp_value1 = sm_items[0] + '-12月_' + tmp_sw_part1 + '_' + t_part
					tmp_value2 = sm_items[0] + '-12月_' + tmp_sw_part2 + '_' + t_part
					tmp_value3 = '1月-' + sm_items[1] + '_' + tmp_sw_part1 + '_'+ t_part
					tmp_value4 = '1月-' + sm_items[1] + '_' + tmp_sw_part2 + '_'+ t_part
					values.append(tmp_value1)
					values.append(tmp_value2)
					values.append(tmp_value3)
					values.append(tmp_value4)
					to_remove.append(value)
				else:
					values.append(tmp_value1)
					values.append(tmp_value2)
					to_remove.append(value)
			else:
				if l_w_num > r_w_num:
					tmp_sw_part1 = str(l_w_num) + '-7'
					tmp_sw_part2 = '1-' + str(r_w_num)
					tmp_value1 = sm_part + '_' + tmp_sw_part1 + '_' + t_part
					tmp_value2 = sm_part + '_' + tmp_sw_part2 + '_' + t_part
					values.append(tmp_value1)
					values.append(tmp_value2)
					to_remove.append(value)

		for item in to_remove:
			values.remove(item)

def normalize_SM_ST(in_out_dict):
	"""将SM_ST类型时间进行规范化

	具体规范化步骤如下:
	将形式为11月-2月_09:45:00-16:15:00规范化为:
	11月-12月_09:45:00-16:15:00
	1月-2月_09:45:00-16:15:00


	"""
	if in_out_dict.has_key('SM_ST'):
		values = in_out_dict['SM_ST']
		to_remove = []
		for value in values:
			index = value.find('_')
			sm_part = value[:index]
			t_part = value[index + 1:]
			sm_items = sm_part.split('-')
			l_num = int(sm_items[0][:sm_items[0].find('月')])
			r_num = int(sm_items[1][:sm_items[1].find('月')])
			if l_num > r_num:
				tmp_value1 = sm_items[0] + '-' + '12月_' + t_part
				tmp_value2 = '1月-' + sm_items[1] + '_' + t_part
				values.append(tmp_value1)
				values.append(tmp_value2)
				to_remove.append(value)

		for item in to_remove:
			values.remove(item)


def normalize_SD(in_out_dict):
	"""将SD类型时间进行规范化

	具体规范化操作如下:
	将形如10月1日-5月31日规范化为:
	10月1日-12月31日
	1月1日-5月31日

	测试通过.
	"""
	if in_out_dict.has_key('SD'):
		values = in_out_dict['SD']
		to_remove = []
		for value in values:
			d_items = value.split('-')
			if combine.compare_date(combine.day_to_date(d_items[1]), combine.day_to_date(d_items[0])):
				print d_items[0], d_items[1]
				tmp_value1 = d_items[0] + '-' + '12月31日'
				tmp_value2 = '1月1日' + '-' + d_items[1]
				values.append(tmp_value1)
				values.append(tmp_value2)
				to_remove.append(value)

		for item in to_remove:
			values.remove(item)

def normalize_SD_ST(in_out_dict):
	"""将SD_ST类型时间进行规范化

	具体规范化操作如下:
	将形如11月1日-3月31日_10:00:00-16:00:00规范化为:
	11月1日-12月31日_10:00:00-16:00:00
	1月1日-3月31日_10:00:00-16:00:00

	"""
	if in_out_dict.has_key('SD_ST'):
		values = in_out_dict['SD_ST']
		to_remove = []
		for value in values:
			index = value.find('_')
			sd_part = value[:index]
			t_part = value[index + 1:]
			sd_items = sd_part.split('-')
			if combine.compare_date(combine.day_to_date(sd_items[1]), combine.day_to_date(sd_items[0])):
				tmp_value1 = sd_items[0] + '-' + '12月31日_' + t_part
				tmp_value2 = '1月1日' + '-' + sd_items[1] + '_' + t_part
				values.append(tmp_value1)
				values.append(tmp_value2)
				to_remove.append(value)

		for item in to_remove:
			values.remove(item)


def split_key_value(time_span):
	"""切分输入串，返回key和value
	
	输入串格式为: "time_span_84000_1" : "ST:09:00:00-19:00:00"
	例如输入串: "time_span_84000_1" : "ST:09:00:00-19:00:00"
	返回key=ST, value=09:00:00-19:00:00

	测试通过.
	
	"""
	first_colon_index = time_span.find(":")
	#获取时间规则文本, 例如:ST:09:00:00-19:00:00
	rule_text = time_span[first_colon_index + 3 : -1]
	first_colon_index = rule_text.find(":")
	#获取key, 例如:ST
	key = rule_text[:first_colon_index]
	#获取value, 例如:09:00:00-19:00:00
	value = rule_text[first_colon_index + 1 : ]

	#value后处理
	value = value.replace('am', '')
	value = value.replace('pm', '')
	
	return key, value

def main():
	if(len(sys.argv) != 3):
		print "\t Please enter following args:\n"
		print "\t ps_file_path: input file path created by PatternSystem."
		print "\t rule_file_path: rule output file path."

	input_path = sys.argv[1]
	output_path = sys.argv[2]
	spot_list = read_by_spot(input_path)
	rule_list = transform2(spot_list)
	output_file = open(output_path, 'w+')
	output_file.writelines(rule_list)
	output_file.close()

def just_test():
	spot_list = read_by_spot('case_result.txt')
	print spot_list[0]
	print len(spot_list)
	dicts = transform(spot_list)
	print len(dicts)
	type_dict = {}
	for item in dicts:
		#print item
		for key in item.keys():
			#print key, item[key]
			if type_dict.has_key(key):
				type_dict[key].extend(item[key])
			else:
				type_dict[key] = item[key]
	for key in type_dict.keys():
		tmp_set = set(type_dict[key])
		print '===========', key, '==========='
		print '\n'.join(tmp_set)
	
if __name__ == "__main__":
	#just_test()
	##### 测试reduce_to_M
	print "===========testing reduce_to_M========="
	spot = [u'"time_span_85500_1" : "M:11月"', 
			u'"time_span_85531_1" : "M_M_M:3月_4月_9月"', 
			u'"time_span_85530_1" : "M_M:10月_12月"', 
			u'"time_span_85532_1" : "M_M_M_M:5月_6月_9月_10月"']

	spot_dict = create_dict(spot)
	print spot_dict

	##### 测试reduce_to_W_ST
	print "===========testing reduce_to_W_ST========="
	spot = ['"time_span_90000_1" : "W_ST:5_00:15:00-13:30:00"',
		'"time_span_90011_1" : "W_W_ST:3_4_00:15:00-13:30:00"',
		'"time_span_90015_1" : "W_W_W_ST:1_2_4_10:00:00-20:00:00"',
		'"time_span_90016_1" : "W_W_W_W_ST:1_2_4_5_09:00:00-17:00:00"']
	spot_dict = create_dict(spot)
	values = spot_dict['W_ST']
	print values
	print len(values)

	##### 测试normalize_S_ST
	print "===========testing normalize_S_ST========="
	spot = ['"time_span_85702_1" : "S_ST:winter_10:00:00-17:30:00"',
		'"time_span_85702_2" : "S_ST:summer_10:00:00-18:30:00"',
		'"time_span_85702_2" : "S_ST:summer,winter_10:00:00-18:30:00"']
	spot_dict = create_dict(spot)
	print spot_dict
	
	##### 测试normalize_S
	print "===========testing normalize_S========="
	spot = ['"time_span_85700_1" : "S:winter"',
		'"time_span_85700_2" : "S:summer"',
		'"time_span_85700_1" : "S:summer,autumn"']
	spot_dict = create_dict(spot)
	print spot_dict

	##### 测试normalize_S_ST_ST
	print "===========testing normalize_S_ST_ST========="
	spot = ['"time_span_85703_2" : "S_ST_ST:winter_10:45:00-13:15:00_16:00:00-18:45:00"',
		'"time_span_85703_1" : "S_ST_ST:summer_10:45:00-13:15:00_16:00:00-19:45:00"',
		'"time_span_85703_1" : "S_ST_ST:spring,summer_10:45:00-13:15:00_16:00:00-19:45:00"']
	spot_dict = create_dict(spot)
	print spot_dict

	##### 测试normalize_SW_ST
	print "===========testing normalize_SW_ST========="
	spot = ['"time_span_90010_1" : "SW_ST:1-4_10:00:00-18:00:00"',
		'"time_span_90010_1" : "SW_ST:6-4_10:00:00-18:00:00"',
		'"time_span_90010_1" : "SW_ST:7-4_10:00:00-18:00:00"']
	spot_dict = create_dict(spot)
	print spot_dict

	##### 测试normalize_S_SW_ST
	print "===========testing normalize_S_SW_ST========="
	spot = ['"time_span_85704_1" : "S_SW_ST:winter,spring_6-5_10:00:00-17:00:00"',
		'"time_span_85704_1" : "S_SW_ST:winter,summer_2-5_10:00:00-17:00:00"',
		'"time_span_85704_1" : "S_SW_ST:winter_2-5_10:00:00-17:00:00"']
	spot_dict = create_dict(spot)
	print spot_dict
	print len(spot_dict['S_SW_ST'])

	##### 测试normalize_SW_ST_WAC
	print "===========testing normalize_SW_ST_WAC========="
	spot = ['"time_span_90081_1" : "SW_ST_WAC:6-7_10:00:00-18:00:00_1"',
		'"time_span_90081_1" : "SW_ST_WAC:2-7_10:00:00-17:00:00_1"',
		'"time_span_90081_1" : "SW_ST_WAC:3-1_12:00:00-17:00:00_2"']
	spot_dict = create_dict(spot)
	print spot_dict
	print len(spot_dict['SW_ST_WAC'])

	##### 测试normalize_SW
	print "===========testing normalize_SW========="
	spot = ['"time_span_90070_1" : "SW:3-5"',
		'"time_span_90070_1" : "SW:4-7"',
		'"time_span_90070_1" : "SW:4-3"']
	spot_dict = create_dict(spot)
	print spot_dict
	print len(spot_dict['SW'])

	##### 测试normalize_SM
	print "===========testing normalize_SM========="
	spot = ['"time_span_85502_1" : "SM:1月-50587471月"',
		'"time_span_85502_1" : "SM:27月-9月"',
		'"time_span_85502_1" : "SM:11月-3月"']
	spot_dict = create_dict(spot)
	print len(spot)
	print spot_dict
	print len(spot_dict['SM'])

	##### 测试normalize_SD
	print "===========testing normalize_SD========="
	spot = ['"time_span_85601_1" : "SD:8月1日-8月30日"',
		'"time_span_85601_1" : "SD:10月1日-4月30日"',
		'"time_span_85601_1" : "SD:9月15日-5月14日"']
	spot_dict = create_dict(spot)
	print len(spot)
	print '_'.join(spot_dict['SD'])
	print len(spot_dict['SD'])

	##### 测试normalize_SM_SW_ST
	print "===========testing normalize_SM_SW_ST========="
	spot = ['"time_span_85550_1" : "SM_SW_ST:11月-2月_1-5_08:30:00-16:00:00"',
		'"time_span_85550_1" : "SM_SW_ST:3月-10月_1-6_08:30:00-18:00:00"',
		'"time_span_85550_1" : "SM_SW_ST:5月-3月_4-2_10:00:00-17:00:00"']
	spot_dict = create_dict(spot)
	print len(spot)
	print '_'.join(spot_dict['SM_SW_ST'])
	print len(spot_dict['SM_SW_ST'])

	##### 测试normalize_SM_ST
	print "===========testing normalize_SM_ST========="
	spot = ['"time_span_85540_1" : "SM_ST:11月-12月_09:15:00-16:45:00"',
		'"time_span_85540_1" : "SM_ST:3月-10月_10:00:00-18:00:00"',
		'"time_span_85540_2" : "SM_ST:10月-2月_09:00:00-17:00:00"']
	spot_dict = create_dict(spot)
	print len(spot)
	print '_'.join(spot_dict['SM_ST'])
	print len(spot_dict['SM_ST'])

	##### 测试normalize_SD_ST
	print "===========testing normalize_SD_ST========="
	spot = ['"time_span_85605_1" : "SD_ST:4月10日-10月10日_06:00:00-23:00:00"',
		'"time_span_85605_1" : "SD_ST:4月1日-10月15日_09:00:00-18:00:00"',
		'"time_span_85605_2" : "SD_ST:11月1日-3月31日_10:00:00-17:00:00"']
	spot_dict = create_dict(spot)
	print len(spot)
	print '_'.join(spot_dict['SD_ST'])
	print len(spot_dict['SD_ST'])

