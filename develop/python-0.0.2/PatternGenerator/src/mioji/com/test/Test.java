package mioji.com.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

public class Test {

	public static String normalizeTimeSubStr1(String inputTime) {
		if (null == inputTime || inputTime.isEmpty()) {
			return "";
		}
		System.out.println("ORG: inputTime[" + inputTime + "]");
		Pattern subTimePattern = Pattern.compile("周\\d\\d[:]\\d\\d");
		Matcher matcher = subTimePattern.matcher(inputTime);

		Pattern digitPattern = Pattern.compile("\\d");
		int matchCount = 0;
		while (matcher.find()) {
			int start = matcher.start() + matchCount;
			int end = matcher.end() + matchCount;
			String subStr = inputTime.substring(start, end);
			Matcher digitMatcher = digitPattern.matcher(subStr);
			while (digitMatcher.find()) {
				int digitStart = digitMatcher.start();
				int digitEnd = digitMatcher.end();
				String digitStr = subStr.substring(digitStart, digitEnd);
				subStr = subStr.replaceFirst(digitStr, digitStr + "0");
				++matchCount;
				break;
			}
			inputTime = inputTime.replaceFirst(inputTime.substring(start, end),
					subStr);
		}
		System.out.println("END: inputTime[" + inputTime + "]");

		return inputTime;
	}

	/**
	 * 将“图书馆:周1至周51​​0:00至15:00”规范会为形式"图书馆:周1至周5冒号1​​0:00至15:00"
	 * 
	 * @param inputTime
	 * @return
	 */
	public static String normalizeTimeSubStr2(String inputTime) {
		if (null == inputTime || inputTime.isEmpty()) {
			return "";
		}
		// System.out.println("ORG: inputTime[" + inputTime + "]");
		Pattern subTimePattern = Pattern.compile("周\\d{3}:\\d{2}");
		Matcher matcher = subTimePattern.matcher(inputTime);

		Pattern digitPattern = Pattern.compile("\\d");
		int matchCount = 0;
		while (matcher.find()) {
			int start = matcher.start() + 2 * matchCount;
			int end = matcher.end() + 2 * matchCount;
			String subStr = inputTime.substring(start, end);
			// System.out.println("subStr[" + subStr + "]");
			Matcher digitMatcher = digitPattern.matcher(subStr);
			while (digitMatcher.find()) {
				int digitStart = digitMatcher.start();
				int digitEnd = digitMatcher.end();
				String digitStr = subStr.substring(digitStart, digitEnd);
				subStr = subStr.replaceFirst(digitStr, digitStr + "冒号");
				// System.out.println("subStr[" + subStr + "]");
				++matchCount;
				break;
			}
			inputTime = inputTime.replaceFirst(inputTime.substring(start, end),
					subStr);
		}
		// System.out.println("END: inputTime[" + inputTime + "]");

		return inputTime;
	}

	public static void filterNull(String srcPath, String tarPath)
			throws IOException {
		if (null == srcPath || null == tarPath || srcPath.isEmpty()
				|| tarPath.isEmpty()) {
			return;
		}
		List<String> lines = FileUtils.readLines(new File(srcPath));
		List<String> tarLines = new ArrayList<String>();
		for (String line : lines) {
			line = line.toLowerCase();
			if ("null".equals(line)) {
				continue;
			}
			line = line.replaceAll("-", "至");
			// line = line.replaceAll(":", "冒号");
			line = line.replaceAll("&", ",");
			line = normalizeTimeSubStr1(line);
			line = normalizeTimeSubStr2(line);
			tarLines.add(line);
		}
		FileUtils.writeLines(new File(tarPath), tarLines);
	}

	public static int countType(String path) throws IOException {
		List<String> lines = FileUtils.readLines(new File(path));
		Set<String> typeSet = new HashSet<String>();
		for (String line : lines) {
			if (line.contains("time_span")) {
				String tmpSubStr = line.substring(line.indexOf(":") + 1);
				tmpSubStr = tmpSubStr.substring(tmpSubStr.indexOf("\"") + 1);
				String[] items = tmpSubStr.split(":");
				typeSet.add(items[0]);
			}
		}
		List<String> typeList = new ArrayList<String>(typeSet);
		Collections.sort(typeList);
		for (String type : typeList) {
			System.out.println(type);
		}
		return typeSet.size();
	}

	public static void main(String[] args) throws IOException {
		// filterNull("recog.text.reg", "recog.valid");
		System.out.println(countType("case_result.txt"));
	}

}
