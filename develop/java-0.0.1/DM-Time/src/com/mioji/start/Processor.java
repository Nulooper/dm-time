package com.mioji.start;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.mioji.utils.Constants;

/**
 * 
 * @author Wang Zhiwei
 * 
 */
public class Processor {

	/**
	 * 第1步: 去噪，删除无用信息，格式初步规范化。
	 */
	public static void step1() {
		try {
			List<String> lines = FileUtils.readLines(new File(
					"vid_opendesc.txt"), Charset.forName("UTF-8"));
			List<String> newLines = new ArrayList<String>();
			String newLine = null;
			for (String line : lines) {
				newLine = line;
				newLine = newLine.replaceAll("每周", "周");
				newLine = newLine.replaceAll("星期", "周");
				for (String src : Constants.replaceMap.keySet()) {
					newLine = newLine.replaceAll(src,
							Constants.replaceMap.get(src));
				}
				newLines.add(newLine);
			}
			FileUtils.writeLines(new File("step1_clean_1"), newLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 第2步：分离出NULL和非NULL，得到处理后的文件clean_2
	 */
	public static void step2() {
		try {
			List<String> lines = FileUtils.readLines(new File("step1_clean_1"),
					Charset.forName("UTF-8"));
			List<String> nullLines = new ArrayList<String>();
			List<String> generalLines = new ArrayList<String>();
			for (String line : lines) {
				if (line.contains("NULL")) {
					nullLines.add(line);
				} else {
					generalLines.add(line);
				}
			}
			FileUtils.writeLines(new File("step2_null"), nullLines);
			FileUtils.writeLines(new File("step2_clean_2"), generalLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 第3步：分离出全年和非全年，得到文件step3_whole_year和step3_clean_3
	 */
	public static void step3() {
		try {
			List<String> lines = FileUtils.readLines(new File("step2_clean_2"),
					Charset.forName("UTF-8"));
			List<String> wholeYearLines = new ArrayList<String>();
			List<String> generalLines = new ArrayList<String>();

			String[] items = null;
			for (String line : lines) {
				line = line.trim();
				items = line.split("\t");
				if (items.length >= 3) {
					if (items[2].equals("全年") || items[2].equals("全年开放")
							|| items[2].equals("常年") || items[2].equals("常年开放")) {
						wholeYearLines.add(line);
					} else {
						generalLines.add(line);
					}
				} else {
					generalLines.add(line);
				}
			}

			FileUtils.writeLines(new File("step3_whole_year"), wholeYearLines);
			FileUtils.writeLines(new File("step3_clean_3"), generalLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 第4步： 分离出“景点信息块”中仅包含时间区间。分别得到文件step4_only_time和step4_clean_4
	 */
	public static void step4() {
		try {
			List<String> lines = FileUtils.readLines(new File("step3_clean_3"),
					Charset.forName("UTF-8"));
			List<String> onlyTimeLines = new ArrayList<String>();
			List<String> generalLines = new ArrayList<String>();

			String regex = "^((\\d{1,2}[.:]\\d{1,2})\\s*[-~至到和]\\s*(\\d{1,2}[.:]\\d{1,2})\\s*[.,&;]*\\s*[对外开放]*[开放]*)+$";
			Pattern pattern = Pattern.compile(regex);

			String[] items = null;
			for (String line : lines) {
				line = line.trim();
				items = line.split("\t");
				if (items.length >= 3) {
					Matcher matcher = pattern.matcher(items[2]);
					if (matcher.matches()) {
						System.out.println(line);
						onlyTimeLines.add(line);
					} else {
						generalLines.add(line);
					}
				} else {
					generalLines.add(line);
				}
			}
			FileUtils.writeLines(new File("step4_clean_4"), generalLines);
			FileUtils.writeLines(new File("step4_only_time"), onlyTimeLines);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 第5步: 分离出满足模式"周1-周6 07：00-09：00"。得到文件step5_week_time和step5_clean_5
	 */
	public static void step5() {
		try {
			List<String> lines = FileUtils.readLines(new File("step4_clean_4"),
					Charset.forName("UTF-8"));
			List<String> weekTimeLines = new ArrayList<String>();
			List<String> generalLines = new ArrayList<String>();

			String regex = "(周\\d\\s*[-~至到]\\s*周\\d|周\\d\\s*|)(\\s*[,.;:&]*\\s*(\\d{1,2}[.:]\\d{1,2})\\s*[-~至到和]\\s*(\\d{1,2}[.:]\\d{1,2})\\s*[,.;:&]*\\s*)+";
			Pattern pattern = Pattern.compile(regex);

			String[] items = null;
			Matcher matcher = null;
			for (String line : lines) {
				line = line.trim();
				items = line.split("\t");
				if (items.length >= 3) {
					matcher = pattern.matcher(items[2]);
					if (matcher.matches()) {
						weekTimeLines.add(line);
					} else {
						generalLines.add(line);
					}
				} else {
					generalLines.add(line);
				}
			}
			FileUtils.writeLines(new File("step5_week_time"), weekTimeLines);
			FileUtils.writeLines(new File("step5_clean_5"), generalLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 第6步：分离出满足模式"周1-周2 07:00-09:00 周2-周3 09:00-13:00",
	 * 生成文件step6_week_time_week_time和step6_clean_6.
	 */
	public static void step6() {
		try {
			List<String> lines = FileUtils.readLines(new File("step5_clean_5"),
					Charset.forName("UTF-8"));
			List<String> weekTimeWeekTimeLines = new ArrayList<String>();
			List<String> generalLines = new ArrayList<String>();

			String regex = "(周\\d\\s*[-~至到]\\s*周\\d|周\\d\\s*)(\\s*[,;.:]*\\s*\\d{1,2}\\s*[.:]\\s*\\d{1,2}\\s*[-~至到]\\s*\\d{1,2}\\s*[.:]\\s*\\d{1,2}\\s*[,.:;&]*\\s*)+((周\\d\\s*[-~至到]\\s*周\\d\\s*|周\\d\\s*)(\\s*[:,;.]*\\s*\\d{1,2}\\s*[.:]\\s*\\d{1,2}\\s*[-~至到]\\s*\\d{1,2}\\s*[.:]\\s*\\d{1,2}\\s*[:,;.]*\\s*)+)";
			Pattern pattern = Pattern.compile(regex);

			String[] items = null;
			for (String line : lines) {
				line = line.trim();
				items = line.split("\t");
				if (items.length >= 3) {
					Matcher matcher = pattern.matcher(items[2].trim());
					if (matcher.matches()) {
						weekTimeWeekTimeLines.add(line);
					} else {
						generalLines.add(line);
					}
				} else {
					generalLines.add(line);
				}
			}

			FileUtils.writeLines(new File("step6_week_time_week_time"),
					weekTimeWeekTimeLines);
			FileUtils.writeLines(new File("step6_clean_6"), generalLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 第7步：分离出满足模式"10月1日-3月31日 10:00-17:004月1日-9月30日 10:00-18:00",生成文件
	 * step7_date_time_date_time和文件step7_clean_7.
	 */
	public static void step7() {
		try {
			List<String> lines = FileUtils.readLines(new File("step6_clean_6"),
					Charset.forName("UTF-8"));
			List<String> dateTimeDateTimeLines = new ArrayList<String>();
			List<String> generalLines = new ArrayList<String>();

			String regex = "([,.;:]*\\s*[0-9]{1,2}月[0-9]{1,2}日\\s*[-~至到]\\s*[0-9]{1,2}月[0-9]{1,2}日\\s*[:,]*\\s*[0-9]{1,2}[.:][0-9]{1,2}[-~至到][0-9]{1,2}\\s*[.:][0-9]{1,2}\\s*[,.;:]*\\s*)+";
			Pattern pattern = Pattern.compile(regex);

			String[] items = null;
			for (String line : lines) {
				line = line.trim();
				items = line.split("\t");
				if (items.length >= 3) {
					Matcher matcher = pattern.matcher(items[2].trim());
					if (matcher.matches()) {
						dateTimeDateTimeLines.add(line);
					} else {
						generalLines.add(line);
					}
				} else {
					generalLines.add(line);
				}
			}

			FileUtils.writeLines(new File("step7_date_time_date_time"),
					dateTimeDateTimeLines);
			FileUtils.writeLines(new File("step7_clean_7"), generalLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 第8步：分理处满足模式"(周1-周2 09：00-10：00)*"，得到文件step8_week_time_n和step8_clean_8
	 */
	public static void step8() {
		try {
			List<String> lines = FileUtils.readLines(new File("step7_clean_7"),
					Charset.forName("UTF-8"));
			List<String> weekTimeNLines = new ArrayList<String>();
			List<String> generalLines = new ArrayList<String>();

			String regex = "(((春季|夏季|秋季|冬季|春秋季|)周[0-7][-~至到和、]周[0-7]\\s*[,.:;]*\\s*|[(]?周[0-7][、,.:;和]*\\s*)\\s*[,.:;]*\\s*([从]?[0-9]{1,2}[:.][0-9]{1,2}\\s*[-~至到和]\\s*[0-9]{1,2}[.:][0-9]{1,2}\\s*[.,;:]*\\s*[)]?)*(周[1-7](休息|闭馆)|))*";
			Pattern pattern = Pattern.compile(regex);

			String[] items = null;
			for (String line : lines) {
				line = line.trim();
				items = line.split("\t");
				if (items.length >= 3) {
					Matcher matcher = pattern.matcher(items[2].trim());
					if (matcher.matches()) {
						weekTimeNLines.add(line);
					} else {
						generalLines.add(line);
					}
				} else {
					generalLines.add(line);
				}
			}

			FileUtils.writeLines(new File("step8_week_time_n"), weekTimeNLines);
			FileUtils.writeLines(new File("step8_clean_8"), generalLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 第9步：分离模式没有任何景点信息模块。得到文件step9_empty和step9_clean_9.
	 */
	public static void step9() {
		try {
			List<String> lines = FileUtils.readLines(new File("step8_clean_8"),
					Charset.forName("UTF-8"));
			List<String> emptyLines = new ArrayList<String>();
			List<String> generalLines = new ArrayList<String>();

			String[] items = null;
			for (String line : lines) {
				line = line.trim();
				items = line.split("\t");
				if (items.length < 3) {
					emptyLines.add(line);
				} else {
					generalLines.add(line);
				}
			}

			FileUtils.writeLines(new File("step9_empty"), emptyLines);
			FileUtils.writeLines(new File("step9_clean_9"), generalLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 第10步: 分离出模式"2月12日-2月15日"或者"2月-3月", 得到文件step10_month和step10_clean_10
	 */
	public static void step10() {
		try {
			List<String> lines = FileUtils.readLines(new File("step9_clean_9"),
					Charset.forName("UTF-8"));
			List<String> dateWeekTimeNLines = new ArrayList<String>();
			List<String> generalLines = new ArrayList<String>();

			String regex = "[0-9]{1,2}月([0-9]{1,2}日|)\\s*[-~至到和]\\s*[0-9]{1,2}月([0-9]{1,2}日|)";
			Pattern pattern = Pattern.compile(regex);

			String[] items = null;
			for (String line : lines) {
				line = line.trim();
				items = line.split("\t");
				if (items.length >= 3) {
					Matcher matcher = pattern.matcher(items[2].trim());
					if (matcher.matches()) {
						dateWeekTimeNLines.add(line);
					} else {
						generalLines.add(line);
					}
				} else {
					generalLines.add(line);
				}
			}

			FileUtils.writeLines(new File("step10_month"), dateWeekTimeNLines);
			FileUtils.writeLines(new File("step10_clean_10"), generalLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 第11步：分理出模式："2月-9月 13：00-16：00", 得到文件step11_month_time和step11_clean_11
	 */
	public static void step11() {

		try {
			List<String> lines = FileUtils.readLines(
					new File("step10_clean_10"), Charset.forName("UTF-8"));
			List<String> monthTimeNLines = new ArrayList<String>();
			List<String> generalLines = new ArrayList<String>();

			String regex = "([,.:;、]*\\s*(每年|)[0-9]{1,2}(月|)(中|中旬|下旬|)\\s*([-~至到和][0-9]{1,2}月(中|中旬|下旬|)|)([及|和|,][0-9]{1,2}月|)\\s*[.,;:、]\\s*(\\s*[,.;:&]*[0-9]{1,2}[.:][0-9]{1,2}[-~至到和][0-9]{1,2}[:.][0-9]{1,2}(pm|am|)\\s*[,.;:&]*)+\\s*[,.:;、]*\\s*)+(开放参观|)";
			Pattern pattern = Pattern.compile(regex);

			String[] items = null;
			for (String line : lines) {
				line = line.trim();
				items = line.split("\t");
				if (items.length >= 3) {
					Matcher matcher = pattern.matcher(items[2].trim());
					if (matcher.matches()) {
						monthTimeNLines.add(line);
					} else {
						generalLines.add(line);
					}
				} else {
					generalLines.add(line);
				}
			}

			FileUtils
					.writeLines(new File("step11_month_time"), monthTimeNLines);
			FileUtils.writeLines(new File("step11_clean_11"), generalLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] agrs) {
		Processor.step6();
	}
}
