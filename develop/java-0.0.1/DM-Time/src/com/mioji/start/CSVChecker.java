package com.mioji.start;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.mioji.parse.SpotTimeText;

public class CSVChecker {

	public static void filterUnvalid() throws IOException {
		List<String> lines = FileUtils.readLines(new File("data/viewspot.csv"));
		List<String> validLines = new ArrayList<String>();
		for (String line : lines) {
			if (countNumber(line, "\"") != 6) {
				continue;
			}
			validLines.add(line);
			System.out.println(line);
		}
		FileUtils.writeLines(new File("data/valid_viewspot.csv"), validLines);
	}

	public static int countNumber(String str, String sub) {
		int index;
		int count = 0;
		while ((index = str.indexOf(sub)) != -1) {
			++count;
			str = str.substring(index + sub.length());
		}

		return count;
	}

	/**
	 * 组合目前已经抓取的几个网站的CSV数据集合.
	 * 
	 * @param srcPath1
	 * @param srcPath2
	 * @throws IOException
	 */
	public static void combineValidCSV(String srcPath1, String srcPath2)
			throws IOException {
		List<String> combineLines = new ArrayList<String>();
		List<String> lines1 = FileUtils.readLines(new File(srcPath1));
		List<String> lines2 = FileUtils.readLines(new File(srcPath2));
		combineLines.addAll(lines1);
		combineLines.addAll(lines2);
		FileUtils.writeLines(new File("combine.all"), combineLines);
	}

	/**
	 * 构造复杂模式数据集合.
	 * 
	 * @param tarPath
	 * @throws IOException
	 */
	public static void createComplex(String tarPath) throws IOException {
		List<String> orgLines = FileUtils.readLines(new File("combine.all"));
		List<String> tarLines = new ArrayList<String>();
		for (String line : orgLines) {
			SpotTimeText stt = SpotTimeText.createInstance(line);
			if (!stt.getTimeText().equals("NULL") && !stt.getTimeText().equals("null")) {
				tarLines.add(stt.getTimeText());
			}
//			System.out.println(line);
//			System.out.println(stt.getTimeText());
//			System.out.println();
		}
		Collections.sort(tarLines);
		FileUtils.writeLines(new File(tarPath), tarLines);
	}

	public static void main(String[] args) throws Throwable {
		// filterUnvalid();
		// combineValidCSV("data/qyer2.csv", "data/valid_viewspot.csv");

		createComplex("combine.notnull");
	}

}
