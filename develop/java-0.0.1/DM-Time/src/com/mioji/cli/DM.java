package com.mioji.cli;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.mioji.miner.MineRobot;
import com.mioji.miner.Pipeline2;

public class DM {
	public static void mine(String dataPath, String recogPath,
			String unrecogPath) {
		try {
			System.out.println("----------景点时间文本规则挖掘开始!----------");
			long start = System.currentTimeMillis();
			Pipeline2 pipeLine = new Pipeline2();
			List<String> lines = FileUtils.readLines(new File(dataPath));
			System.out.println("读入待解析文本完毕!");
			for (String line : lines) {
				pipeLine.parse(line);
			}
			System.out.println("解析时间文本完毕!");
			long end = System.currentTimeMillis();
			System.out.println("解析时间:" + (end - start) * 1.0 / 1000 + "秒!");
			FileUtils.writeLines(new File(recogPath), pipeLine.getRuleList());
			System.out.println("已将成功解析的时间文本规则写入" + recogPath + "!");

			System.out.println("识别率:\t" + pipeLine.getRecogRate());
			// 打印详细信息
			Map<String, Integer> patterMatchMap = pipeLine.getMatchMap();
			for (String pattern : patterMatchMap.keySet()) {
				System.out.println("匹配" + pattern + ":\t"
						+ patterMatchMap.get(pattern));
			}
			System.out.println("识别数量:\t" + pipeLine.getRecogCount());
			System.out.println("总共数量:\t" + pipeLine.getTotalCount());
			//
			for (String parserName : pipeLine.getRecogMap().keySet()) {
				System.out.println(parserName + "识别数量:\t"
						+ pipeLine.getRecogMap().get(parserName));
			}
			FileUtils.writeLines(new File(unrecogPath),
					pipeLine.getUnrecognizedList());
			System.out.println("已将不能解析的时间文本规则写入" + unrecogPath + "!");

			System.out.println("----------景点时间文本规则挖掘结束!----------");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		if (args.length < 3) {
			System.out.println("请输入参数:dataPath, recogPath和unrecogPath!");
			System.out.println("\tdataPath: 待解析文本的路径，每一行表示一个景点的时间描述!");
			System.out.println("\trecogPath: 存储成功解析时间规则的文件路径!");
			System.out.println("\tunrecogPath: 存储不能解析的景点文本的文件路径!");

			return;
		}

		String dataPath = args[0];
		String recogPath = args[1];
		String unrecogPath = args[2];
		MineRobot.mine(dataPath, recogPath, unrecogPath);
		// Miner.mine("data/qyer2.csv");
	}

}
