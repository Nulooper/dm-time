package com.mioji.parse;


/**
 * 处理景点开放时间描述仅有month和time的情况，包含以下三种情况：1)5月至9月,09:00-19:00 ;3月和4月, 09:00-18:00
 * ;10月至2月,09:00-17:00;2)2-12月,9:30-18:00;3)5月至9月中旬:8:30-18:00.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-8 下午8:37:49
 * 
 */
public final class MonthTimeParser implements Parsable {

	@Override
	public boolean isMatched(SpotTimeText stt) {
		return false;
	}

	@Override
	public String parse(SpotTimeText stt) {
		return null;
	}

}
