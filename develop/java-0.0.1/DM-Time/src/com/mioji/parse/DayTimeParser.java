package com.mioji.parse;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mioji.config.ConfigManager;
import com.mioji.config.ParserConfig;
import com.mioji.ds.CloseRule;
import com.mioji.ds.DayTimeRule;
import com.mioji.ds.MiojiPair;
import com.mioji.ds.OpenRule;
import com.mioji.ds.SpotRule;
import com.mioji.ds.TimeRule;

/**
 * 
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-9 上午11:34:21
 * 
 */
public class DayTimeParser implements Parsable {

	@Override
	public boolean isMatched(SpotTimeText stt) {
		ParserConfig pc = ConfigManager.getInstance().getParserConfig(
				DayTimeParser.class.getName());
		Pattern pattern = Pattern.compile(pc.patternList.get(0));
		String timeText = stt.getTimeText();
		Matcher matcher = pattern.matcher(timeText);

		return matcher.matches();
	}

	@Override
	public String parse(SpotTimeText stt) {
		String timeTextInfo = stt.getTimeText();

		List<DayTimeRule> dayTimeRuleList = new ArrayList<DayTimeRule>();

		List<String> dayTimeGroupList = TimeSubExtractor.extractByGroup(
				timeTextInfo, SubPattern.DAY_TIME_SUB_REGEX);
		for (String group : dayTimeGroupList) {
			dayTimeRuleList.add(this.getDayTimeRule(group));
		}

		OpenRule or = OpenRule.createInstance(TimeRule.createEmpty(),
				dayTimeRuleList, EmptyListCreator.getEmptyWTR(),
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule
				.createInstance(stt.getId(), or, CloseRule.createEmpty())
				.toString();
	}

	/**
	 * 根据输入的文本创建DayTimeRule对象.
	 * 
	 * @param dayTimeText
	 *            格式为:XX月XX日XX:XX
	 * @return
	 */
	private DayTimeRule getDayTimeRule(String dayTimeText) {
		// step1: 获取天区间.
		List<String> days = TimeSubExtractor.extractByGroup(dayTimeText,
				SubPattern.DAY_REGEX);
		MiojiPair dayPair = MiojiPair.createInstance(days.get(0), days.get(1));

		// step2: 获取时间区间.
		List<String> times = TimeSubExtractor.extractByGroup(dayTimeText,
				SubPattern.TIME_REGX);
		MiojiPair timePair = MiojiPair.createInstance(times.get(0),
				times.get(1));

		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		timePairList.add(timePair);

		return DayTimeRule.createInstance(dayPair, timePairList);
	}
}
