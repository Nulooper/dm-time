package com.mioji.parse;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mioji.config.ConfigManager;
import com.mioji.config.ParserConfig;
import com.mioji.ds.CloseRule;
import com.mioji.ds.MiojiPair;
import com.mioji.ds.OpenRule;
import com.mioji.ds.SpotRule;
import com.mioji.ds.TimeRule;
import com.mioji.ds.WeekTimeRule;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-8 下午8:36:15
 * 
 */
public final class WeekTimeParser implements Parsable {

	@Override
	public boolean isMatched(SpotTimeText stt) {
		ParserConfig pc = ConfigManager.getInstance().getParserConfig(
				WeekTimeParser.class.getName());
		Pattern pattern = Pattern.compile(pc.patternList.get(0));
		String timeText = stt.getTimeText();
		Matcher matcher = pattern.matcher(timeText);

		return matcher.matches();
	}

	@Override
	public String parse(SpotTimeText stt) {
		String timeTextInfo = stt.getTimeText();
		List<WeekTimeRule> weekTimeRuleList = new ArrayList<WeekTimeRule>();

		List<String> weekTimeGroupList = TimeSubExtractor.extractByGroup(
				timeTextInfo, SubPattern.WEEK_SUB_REGEX);
		for (String group : weekTimeGroupList) {
			weekTimeRuleList.add(this.getWeekTimeRule(group));
		}

		OpenRule or = OpenRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), weekTimeRuleList,
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(stt.getId(), or, CloseRule.createEmpty()).toString();
	}

	/**
	 * 
	 * @param weekTimeText
	 *            格式为: 周X-周XXX:XX-XX:XX
	 * @return
	 */
	private WeekTimeRule getWeekTimeRule(String weekTimeText) {
		// System.out.println("时间抽取");
		// step1. 检索周X.
		List<String> weekList = TimeSubExtractor.extractByGroup(weekTimeText,
				"周\\d");

		// step1-1. 后处理1. 针对这种情况: 周7,10:00-17:00. 解决办法：在weekList中增加一项"周7".
		if (weekList.size() == 1) {
			weekList.add(weekList.get(0));
		}

		// step2. 检索出时间XX:XX
		List<String> timeList = TimeSubExtractor.extractByGroup(weekTimeText,
				"\\d{1,2}[:.]\\d{1,2}");

		// step2-1. 后处理1.针对这种情况：周2至周48:00-17:00. 上述两个步骤会时间识别为48:00-17:00,
		// 正确地时间为8:00-17:00.
		int weekLastIndex = weekTimeText.lastIndexOf("周");
		int semicolnIndex = weekTimeText.indexOf(":");
		if (semicolnIndex - weekLastIndex == 3) {
			timeList.set(0, weekTimeText.substring(semicolnIndex - 1,
					semicolnIndex + 3));
		}

		// step2-2. 后处理2.针对这些情况1：10.20-12.30 时间格式不规范，应当规范为10:20-12:30; 情况2：8:00,
		// 应当规范为08:00.
		int timeSize = timeList.size();
		String time;
		for (int i = 0; i < timeSize; ++i) {
			// 处理情况1
			time = timeList.get(i);
			time = time.replaceAll("\\.", ":");

			// 处理情况2
			int index = time.indexOf(":");
			if (1 == index) {
				time = "0" + time;
			}

			timeList.set(i, time);
		}

		// step3. 构造一个weektime规则
		MiojiPair weekPair = MiojiPair.createInstance(weekList.get(0),
				weekList.get(1));
		// System.out.println(weekPair.toString());
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		for (int j = 0; j < timeSize; j = j + 2) {
			timePairList.add(MiojiPair.createInstance(timeList.get(j),
					timeList.get(j + 1)));
		}

		return WeekTimeRule.createInstance(weekPair, timePairList);
	}
}
