package com.mioji.parse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.mioji.config.ConfigManager;

/**
 * 根据配置文件创建parser.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-8 下午8:48:34
 * 
 */
public final class ParserFactory {

	/**
	 * 根据配置文件创建Parser.
	 * 
	 * @return
	 */
	public static List<Parsable> createParsres() {

		List<Parsable> parserList = new ArrayList<Parsable>();
		// wait to add code.
		List<String> parserClassNameList = ConfigManager.getInstance()
				.getParserClassNameList();
		try {
			for (String className : parserClassNameList) {
				parserList.add((Parsable) Class.forName(className)
						.newInstance());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return Collections.unmodifiableList(parserList);
	}

	public static void main(String[] args) {
		List<Parsable> parsers = ParserFactory.createParsres();
		for (Parsable parser : parsers) {
			System.out.println(parser.getClass().getName());
		}
	}
}
