package com.mioji.parse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mioji.config.ConfigManager;
import com.mioji.config.ParserConfig;
import com.mioji.ds.CloseRule;
import com.mioji.ds.MiojiPair;
import com.mioji.ds.OpenRule;
import com.mioji.ds.SpotRule;
import com.mioji.ds.TimeRule;
import com.mioji.utils.StringHelper;

/**
 * 处理景点开放时间文本中仅包含时间信息的情况: 1)00:00-24:00; 2)9:00-12:3015:00-19:00;
 * 3)00:00-24:00开放.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-8 下午8:48:22
 * 
 */
public final class OnlyTimeParser implements Parsable {

	@Override
	public boolean isMatched(SpotTimeText stt) {

		ParserConfig pc = ConfigManager.getInstance().getParserConfig(
				OnlyTimeParser.class.getName());
		Pattern pattern = Pattern.compile(pc.patternList.get(0));
		String timeText = StringHelper.toDBC(stt.getTimeText());
		Matcher matcher = pattern.matcher(timeText);

		return matcher.matches();
	}

	@Override
	public String parse(SpotTimeText stt) {
		String timeText = StringHelper.toDBC(stt.getTimeText());
		List<MiojiPair> timePairs = this.getTimePairs(timeText);
		TimeRule tr = TimeRule.createInstance(timePairs);
		OpenRule or = OpenRule.createInstance(tr,
				EmptyListCreator.getEmptyDTR(), EmptyListCreator.getEmptyWTR(),
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule
				.createInstance(stt.getId(), or, CloseRule.createEmpty())
				.toString();
	}

	private List<MiojiPair> getTimePairs(String timeInfo) {
		if (null == timeInfo || timeInfo.isEmpty()) {
			return Collections.emptyList();
		}

		Pattern timeSubPat = Pattern.compile("(\\d{1,2}[.:]\\d{1,2})");
		Matcher matcher = timeSubPat.matcher(timeInfo);
		List<MiojiPair> timePairs = new ArrayList<MiojiPair>();
		List<String> timeItems = new ArrayList<String>();
		while (matcher.find()) {
			timeItems.add(matcher.group());
		}

		int size = timeItems.size();
		if (0 == size || size % 2 != 0) {

			Logger.getGlobal().logp(Level.WARNING,
					OnlyTimeParser.class.getName(), "getTimePairs", "匹配不合法!",
					"匹配数量:size=[" + size + "]");

			return Collections.emptyList();
		}

		for (int i = 0; i < size; i = i + 2) {
			timePairs.add(MiojiPair.createInstance(timeItems.get(i),
					timeItems.get(i + 1)));
		}

		return Collections.unmodifiableList(timePairs);
	}

}
