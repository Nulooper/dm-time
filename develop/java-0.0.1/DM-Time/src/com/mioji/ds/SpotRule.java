package com.mioji.ds;

/**
 * 本系统的最终结果描述，描述了景点开放时间日期的规则.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年6月9日 下午2:22:55
 * 
 */
public class SpotRule {
	private String id;
	private OpenRule openRule;
	private CloseRule closeRule;

	public static SpotRule createDefault(String id) {
		return new SpotRule(id, OpenRule.createDefault(),
				CloseRule.createDefault());
	}

	public static SpotRule createInstance(String id, OpenRule openRule,
			CloseRule closeRule) {
		return new SpotRule(id, openRule, closeRule);
	}

	public SpotRule(String id, OpenRule openRule, CloseRule closeRule) {
		this.id = id;
		this.openRule = openRule;
		this.closeRule = closeRule;
	}

	public OpenRule getOpenRule() {
		return this.openRule;
	}

	public CloseRule getCloseRule() {
		return this.closeRule;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public static String getEmptyFormat(String id) {
		return id + "\t" + OpenRule.getEmptyFormat() + "|"
				+ CloseRule.getEmptyFormat();
	}

	public static String getUnknown(String id) {
		return id + "\t" + "UNKNOWN" + "&UNKNOWN" + "|" + "UNKNOWN"
				+ "&UNKNOWN";
	}

	public static String getAlwaysClose(String id) {
		return id + "\t" + "NO" + "&NO" + "|" + "ALWAYS" + "&ALWAYS";
	}

	public static String getAlwaysOpen(String id) {
		return id + "\t" + "ALWAYS" + "&ALWAYS" + "|" + "NO" + "&NO";
	}

	@Override
	public String toString() {
		return this.id + "\t" + this.openRule.toString() + "|"
				+ this.closeRule.toString();
	}

	public static void main(String[] args) {
		System.out.println(SpotRule.getAlwaysOpen("12345"));
	}

}
