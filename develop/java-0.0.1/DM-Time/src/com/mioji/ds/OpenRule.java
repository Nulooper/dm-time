package com.mioji.ds;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * 描述了景点的开放时间规则.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-8 下午7:22:38
 * 
 */
public class OpenRule {
	private TimeRule timeRule;
	private List<DayTimeRule> dayTimeRuleList;
	private List<WeekTimeRule> weekTimeRuleList;
	private List<MonthTimeRule> monthTimeRuleList;
	private List<QuarterTimeRule> quarterTimeRuleList;

	public static OpenRule createDefault() {
		List<DayTimeRule> dayTimeRuleList = new ArrayList<DayTimeRule>();
		dayTimeRuleList.add(DayTimeRule.createDefault());
		dayTimeRuleList.add(DayTimeRule.createDefault());
		List<WeekTimeRule> weekTimeRuleList = new ArrayList<WeekTimeRule>();
		weekTimeRuleList.add(WeekTimeRule.createDefault());
		weekTimeRuleList.add(WeekTimeRule.createDefault());
		List<MonthTimeRule> monthTimeRuleList = new ArrayList<MonthTimeRule>();
		monthTimeRuleList.add(MonthTimeRule.createDefault());
		monthTimeRuleList.add(MonthTimeRule.createDefault());
		List<QuarterTimeRule> quarterTimeRuleList = new ArrayList<QuarterTimeRule>();
		quarterTimeRuleList.add(QuarterTimeRule.createDefault());
		quarterTimeRuleList.add(QuarterTimeRule.createDefault());

		return new OpenRule(TimeRule.createDefault(), dayTimeRuleList,
				weekTimeRuleList, monthTimeRuleList, quarterTimeRuleList);
	}

	public static OpenRule createInstance(TimeRule timeRule,
			List<DayTimeRule> dayTimeRuleList,
			List<WeekTimeRule> weekTimeRuleList,
			List<MonthTimeRule> monthTimeRuleList,
			List<QuarterTimeRule> quarterTimeRuleList) {

		return new OpenRule(timeRule, dayTimeRuleList, weekTimeRuleList,
				monthTimeRuleList, quarterTimeRuleList);

	}

	public OpenRule(TimeRule timeRule, List<DayTimeRule> dayTimeRuleList,
			List<WeekTimeRule> weekTimeRuleList,
			List<MonthTimeRule> monthTimeRuleList,
			List<QuarterTimeRule> quarterTimeRuleList) {

		this.timeRule = timeRule;
		this.dayTimeRuleList = dayTimeRuleList;
		this.weekTimeRuleList = weekTimeRuleList;
		this.monthTimeRuleList = monthTimeRuleList;
		this.quarterTimeRuleList = quarterTimeRuleList;
	}

	public TimeRule getTimeRule() {
		return timeRule;
	}

	public List<DayTimeRule> getDayTimeRuleList() {
		return Collections.unmodifiableList(dayTimeRuleList);
	}

	public List<WeekTimeRule> getWeekTimeRuleList() {
		return Collections.unmodifiableList(weekTimeRuleList);
	}

	public List<MonthTimeRule> getMonthTimeRuleList() {
		return Collections.unmodifiableList(monthTimeRuleList);
	}

	public List<QuarterTimeRule> getQuarterTimeRuleList() {
		return Collections.unmodifiableList(quarterTimeRuleList);
	}

	public static String getEmptyFormat() {
		String times = "<time_rules:" + TimeRule.getEmptyFormat() + ">";
		String dayTimes = "<day_time_rules:[" + "]>";
		String weekTimes = "<week_time_rules:[" + "]>";
		String monthTimes = "<month_time_rules:[" + "]>";
		String quarterTimes = "<quarter_time_rules:[" + "]>";

		return "or:" + times + dayTimes + weekTimes + monthTimes + quarterTimes;
	}

	@Override
	public String toString() {
		if (!this.timeRule.isEmpty()) {
			return this.timeRule.toString() + "&" + "TIME_RULE";
		} else if (!this.dayTimeRuleList.isEmpty()) {
			return StringUtils.join(this.dayTimeRuleList.iterator(), ",") + "&"
					+ "DAY_TIME_RULE";
		} else if (!this.weekTimeRuleList.isEmpty()) {
			return StringUtils.join(this.weekTimeRuleList.iterator(), ",")
					+ "&" + "WEEK_TIME_RULE";
		} else if (!this.monthTimeRuleList.isEmpty()) {
			return StringUtils.join(this.monthTimeRuleList.iterator(), ",")
					+ "&" + "MONTH_TIME_RULE";
		} else {
			return StringUtils.join(this.quarterTimeRuleList.iterator(), ",")
					+ "&" + "QUARTER_TIMES";
		}
	}

	public static void main(String[] args) {
		System.out.println(OpenRule.getEmptyFormat());
	}
}
