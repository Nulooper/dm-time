package com.mioji.ds;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-8 下午8:27:03
 * 
 */
public class MonthTimeRule {
	private MiojiPair monthPair;
	private List<MiojiPair> timePairList;

	public static MonthTimeRule createDefault() {
		List<MiojiPair> list = new ArrayList<MiojiPair>();
		list.add(MiojiPair.createDefaultTimePair());

		return new MonthTimeRule(MiojiPair.createDefaultMonthPair(), list);
	}

	public static MonthTimeRule createInstance(MiojiPair monthPair,
			List<MiojiPair> timePairList) {
		return new MonthTimeRule(monthPair, timePairList);
	}

	public MonthTimeRule(MiojiPair monthPair, List<MiojiPair> timePairList) {
		this.monthPair = monthPair;
		this.timePairList = timePairList;
	}

	public MiojiPair getMonthPair() {
		return monthPair;
	}

	public List<MiojiPair> getTimePairList() {
		return Collections.unmodifiableList(this.timePairList);
	}

	public static String getEmptyFormat() {
		return "{}";
	}

	@Override
	public String toString() {
		String month = this.monthPair.toString();
		if (this.timePairList.isEmpty()) {
			return "{" + month + "}";
		}

		String times = StringUtils.join(this.timePairList.iterator(), ",");
		return "{" + month + ":" + times + "}";
	}

	public static void main(String[] args) {
		System.out.println(MonthTimeRule.createDefault());
	}

}
