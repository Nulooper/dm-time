package com.mioji.x;

import java.util.regex.Pattern;

/**
 * 此类定义时间打标签体系.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月12日 @上午11:51:40
 *
 */
public class TagSyntax {

	/*
	 * 季节标签体系.
	 */
	public static final class Season {
		public static final String TAG_SPRING = "s_1";// 春天
		public static final String TAG_SUMMER = "s_2";// 夏天
		public static final String TAG_AUTUMN = "s_3";// 秋天
		public static final String TAG_WINTER = "s_4";// 冬天

		public static final Pattern PAT_SPRING = Pattern.compile("");
		public static final Pattern PAT_SUMMER = Pattern.compile("");
		public static final Pattern PAT_AUTUMN = Pattern.compile("");
		public static final Pattern PAT_WINTER = Pattern.compile("");
	}

	/*
	 * 月度标签体系.
	 */
	public static final class Month {
		public static final String MONTH_JAN = "m_1";
		public static final String MONTH_FEB = "m_2";
		public static final String MONTH_MAR = "m_3";
		public static final String MONTH_APR = "m_4";
		public static final String MONTH_MAY = "m_5";
		public static final String MONTH_JUNE = "m_6";
		public static final String MONTH_JULY = "m_7";
		public static final String MONTH_AUG = "m_8";
		public static final String MONTH_SEPT = "m_9";
		public static final String MONTH_OCT = "m_10";
		public static final String MONTH_NOV = "m_11";
		public static final String MONTH_DEC = "m_12";
		// 上旬.
		public static final String EARLY_MONTH = "m_e";
		// 中旬.
		public static final String MID_MONTH = "m_m";
		// 下旬.
		public static final String LATE_MONTH = "m_l";
	}

	/*
	 * 周标签体系.对 周x 进行打标签.
	 */
	public static final class Week {
		public static final String WEEK_MON = "w_1";
		public static final String WEEK_TUE = "w_2";
		public static final String WEEK_WED = "w_3";
		public static final String WEEK_THU = "w_4";
		public static final String WEEK_FRI = "w_5";
		public static final String WEEK_SAT = "w_6";
		public static final String WEEK_SUN = "w_7";
	}

	public static final class Day {
		// 天标签体系, 即对一天的划分命名.
		public static final String DAY_MORNing = "d_m";// 上午
		public static final String DAY_NOON = "d_n";// 中午
		public static final String DAY_AFTERNOON = "d_a";// 下午
	}

	public static final class Date {
		// 日期标签体系, 是对具体日期, 例如 2014年12月5日, 12月5日 的打标签.
		public static final String DATE_YMD = "d_ymd";
		public static final String DATE_MD = "d_md";
	}

	public static final class Time {
		// 时间点标签体系, 具体是对xx:xx打标签.
		public static final String TIME_HM = "t_hm";
	}
}
