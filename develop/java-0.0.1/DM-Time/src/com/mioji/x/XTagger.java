package com.mioji.x;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 对给定的文本进行相应地打上标签.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月12日 @上午11:49:36
 *
 */
public class XTagger {
	
	public static List<String> tag(String inputText) {
		if (null == inputText || inputText.isEmpty()) {

			Logger.getGlobal().logp(Level.SEVERE, XTagger.class.getName(),
					"tag", "输入参数为null或者为空!");

			return Collections.emptyList();
		}
		
		return null;
	}
}
