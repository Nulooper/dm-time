package com.mioji.miner;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.CloseRule;
import com.mioji.ds.OpenRule;
import com.mioji.ds.SpotRule;
import com.mioji.ds.TimeRule;
import com.mioji.parse.EmptyListCreator;

public class WhiteDayOpen implements Minable {

	private static final String WHITE_DAY_DICT = "dict/white_day.dict";

	@Override
	public boolean check(String timeTextInfo) throws IOException {
		boolean isValid = false;
		List<String> patLines = FileUtils.readLines(new File(WHITE_DAY_DICT));
		for (String pat : patLines) {
			isValid = isValid || pat.equals(timeTextInfo);
		}
		return isValid;
	}

	@Override
	public String doParse(String id, String timeTextInfo) {
		OpenRule or = OpenRule.createInstance(TimeRule.createWhiteDay(),
				EmptyListCreator.getEmptyDTR(), EmptyListCreator.getEmptyWTR(),
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, CloseRule.createEmpty()).toString();
	}

	@Override
	public String getName() {
		return WhiteDayOpen.class.getName();
	}

}
