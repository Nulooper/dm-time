package com.mioji.miner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import com.mioji.ds.CloseRule;
import com.mioji.ds.MiojiPair;
import com.mioji.ds.MonthTimeRule;
import com.mioji.ds.OpenRule;
import com.mioji.ds.SpotRule;
import com.mioji.ds.TimeRule;
import com.mioji.parse.EmptyListCreator;
import com.mioji.parse.TimeSubExtractor;

public class OnlyMonth implements Minable {

	@Override
	public boolean check(String timeTextInfo) throws IOException {
		Matcher matcher = PatternManager.ONLY_MONTH_PAIR_PAT
				.matcher(timeTextInfo);

		return matcher.matches();
	}

	@Override
	public String doParse(String id, String timeTextInfo) {
		List<String> onlyMonthList = TimeSubExtractor.extractByGroup(
				timeTextInfo, "\\d{1,2}月");
		int size = onlyMonthList.size();
		List<MonthTimeRule> mtrList = new ArrayList<MonthTimeRule>();
		for (int i = 0; i < size; i = 1 + 2) {
			MiojiPair monthPair = MiojiPair.createInstance(
					onlyMonthList.get(i), onlyMonthList.get(i + 1));
			mtrList.add(MonthTimeRule.createInstance(monthPair,
					new ArrayList<MiojiPair>()));
		}

		OpenRule or = OpenRule.createInstance(TimeRule.createDefaultOpen(),
				EmptyListCreator.getEmptyDTR(), EmptyListCreator.getEmptyWTR(),
				mtrList, EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, CloseRule.createEmpty())
				.toString();
	}

	@Override
	public String getName() {
		return OnlyMonth.class.getName();
	}

}
