package com.mioji.miner;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class MinerFactory {
	public static List<Minable> createMiners() throws IOException {
		List<String> minerNameList = FileUtils.readLines(new File(
				"conf/miner.list"));
		List<Minable> minerList = new ArrayList<Minable>();
		for (String classname : minerNameList) {
			try {
				minerList.add((Minable) Class.forName(classname).newInstance());
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException e) {
				e.printStackTrace();
			}
		}

		return Collections.unmodifiableList(minerList);
	}

	public static void main(String[] args) throws IOException {
		List<Minable> miners = createMiners();
		for (Minable miner : miners) {
			System.out.println(miner.getName());
		}
	}
}
