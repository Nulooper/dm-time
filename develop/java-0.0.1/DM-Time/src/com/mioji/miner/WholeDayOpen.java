package com.mioji.miner;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.SpotRule;

public class WholeDayOpen implements Minable {
	private static final String WHOLE_DAY_DICT = "dict/whole_day.dict";

	@Override
	public boolean check(String timeTextInfo) throws IOException {
		boolean isValid = false;
		List<String> patLines = FileUtils.readLines(new File(WHOLE_DAY_DICT));
		for (String pat : patLines) {
			isValid = isValid || pat.equals(timeTextInfo);
		}
		return isValid;
	}

	@Override
	public String doParse(String id, String timeTextInfo) {
		return SpotRule.getAlwaysOpen(id);
	}

	@Override
	public String getName() {
		return WholeDayOpen.class.getName();
	}

}
