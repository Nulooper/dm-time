package com.mioji.miner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import com.mioji.ds.CloseRule;
import com.mioji.ds.MiojiPair;
import com.mioji.ds.OpenRule;
import com.mioji.ds.SpotRule;
import com.mioji.ds.TimeRule;
import com.mioji.ds.WeekTimeRule;
import com.mioji.parse.EmptyListCreator;
import com.mioji.parse.TimeSubExtractor;

public class TimeWithBlack implements Minable {

	@Override
	public boolean check(String timeTextInfo) throws IOException {
		Matcher matcher = PatternManager.TIME_BLACK_DAY_PAT
				.matcher(timeTextInfo);

		return matcher.matches();
	}

	@Override
	public String doParse(String id, String timeTextInfo) {
		List<String> timeList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"[0-9]{1,2}[.:][0-9]{1,2}");
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		int size = timeList.size();
		for (int i = 0; i < size; i = i + 2) {
			timePairList.add(MiojiPair.createInstance(timeList.get(i),
					timeList.get(i + 1)));
		}
		TimeRule rule = TimeRule.createInstance(timePairList);
		OpenRule or = OpenRule.createInstance(rule,
				EmptyListCreator.getEmptyDTR(), EmptyListCreator.getEmptyWTR(),
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		List<String> weekList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"周\\d");
		List<WeekTimeRule> wtrList = new ArrayList<WeekTimeRule>();
		for (String day : weekList) {
			List<MiojiPair> tmpList = new ArrayList<MiojiPair>();
			tmpList.add(MiojiPair.createDefaultTimePair());
			wtrList.add(WeekTimeRule.createInstance(MiojiPair.createInstance(
					day, day)));
		}
		CloseRule cr = CloseRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), wtrList,
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, cr).toString();
	}

	@Override
	public String getName() {
		return TimeWithBlack.class.getName();
	}

}
