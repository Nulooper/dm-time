package com.mioji.miner;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.SpotRule;

public class Unknown implements Minable {
	private static final String UNKNOWN_DICT = "dict/unknown.dict";

	@Override
	public boolean check(String timeTextInfo) throws IOException {
		boolean isValid = false;
		List<String> patLines = FileUtils.readLines(new File(UNKNOWN_DICT));
		for (String pat : patLines) {
			isValid = isValid || pat.equals(timeTextInfo);
		}
		return isValid;
	}

	@Override
	public String doParse(String id, String timeTextInfo) {
		return SpotRule.getUnknown(id);
	}

	@Override
	public String getName() {
		return Unknown.class.getName();
	}

}
