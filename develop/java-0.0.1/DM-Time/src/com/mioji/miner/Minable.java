package com.mioji.miner;

import java.io.IOException;

public interface Minable {

	public boolean check(String timeTextInfo) throws IOException;

	public String doParse(String id, String timeTextInfo);

	public String getName();
}
