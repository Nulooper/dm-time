package com.mioji.miner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import com.mioji.ds.CloseRule;
import com.mioji.ds.MiojiPair;
import com.mioji.ds.OpenRule;
import com.mioji.ds.SpotRule;
import com.mioji.ds.TimeRule;
import com.mioji.ds.WeekTimeRule;
import com.mioji.parse.EmptyListCreator;
import com.mioji.parse.TimeSubExtractor;

/**
 * 识别和解析模式:"11:30-14:30,17:30-21:30;周3休息"
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月10日 @上午9:53:34
 *
 */
public class NTimesWeekClose implements Minable {

	@Override
	public boolean check(String timeTextInfo) throws IOException {
		Matcher matcher = PatternManager.N_TIMES_WEEK_CLOSE_PAT
				.matcher(timeTextInfo);

		return matcher.matches();
	}

	@Override
	public String doParse(String id, String timeTextInfo) {
		List<String> timeList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"\\d{1,2}[:.]\\d{1,2}");
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		int size = timeList.size();
		for (int i = 0; i < size; i = i + 2) {
			timePairList.add(MiojiPair.createInstance(timeList.get(i),
					timeList.get(i + 1)));
		}
		TimeRule tr = TimeRule.createInstance(timePairList);
		OpenRule or = OpenRule.createInstance(tr,
				EmptyListCreator.getEmptyDTR(), EmptyListCreator.getEmptyWTR(),
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		List<String> weekList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"周\\d");
		WeekTimeRule wtr = WeekTimeRule.createInstance(MiojiPair
				.createInstance(weekList.get(0), weekList.get(0)));
		List<WeekTimeRule> wtrList = new ArrayList<WeekTimeRule>();
		wtrList.add(wtr);
		CloseRule cr = CloseRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), wtrList,
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, cr).toString();
	}

	@Override
	public String getName() {
		return NTimesWeekClose.class.getName();
	}

}
