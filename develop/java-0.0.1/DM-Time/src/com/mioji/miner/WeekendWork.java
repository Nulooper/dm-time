package com.mioji.miner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import com.mioji.ds.CloseRule;
import com.mioji.ds.MiojiPair;
import com.mioji.ds.OpenRule;
import com.mioji.ds.SpotRule;
import com.mioji.ds.TimeRule;
import com.mioji.ds.WeekTimeRule;
import com.mioji.parse.EmptyListCreator;
import com.mioji.parse.TimeSubExtractor;

public class WeekendWork implements Minable {

	@Override
	public boolean check(String timeTextInfo) throws IOException {
		Matcher matcher = PatternManager.WEEKEND_WORK_PAT.matcher(timeTextInfo);
		return matcher.matches();
	}

	@Override
	public String doParse(String id, String timeTextInfo) {
		List<String> weekList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"周\\d");
		MiojiPair weekPair1 = MiojiPair.createInstance(weekList.get(0),
				weekList.get(1));
		MiojiPair weekPair2 = MiojiPair.createInstance(weekList.get(2),
				weekList.get(2));
		MiojiPair weekPair3 = MiojiPair.createInstance(weekList.get(3),
				weekList.get(3));

		List<String> timeList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"\\d{1,2}[.:]\\d{1,2}");
		List<MiojiPair> timePairList1 = new ArrayList<MiojiPair>();
		timePairList1.add(MiojiPair.createInstance(timeList.get(0),
				timeList.get(1)));
		List<MiojiPair> timePairList2 = new ArrayList<MiojiPair>();
		timePairList2.add(MiojiPair.createInstance(timeList.get(2),
				timeList.get(3)));

		WeekTimeRule openWtr1 = WeekTimeRule.createInstance(weekPair1,
				timePairList1);
		WeekTimeRule openWtr2 = WeekTimeRule.createInstance(weekPair2,
				timePairList2);

		List<WeekTimeRule> openWtrList = new ArrayList<WeekTimeRule>();
		openWtrList.add(openWtr1);
		openWtrList.add(openWtr2);

		OpenRule or = OpenRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), openWtrList,
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		timePairList.add(MiojiPair.createDefaultOpenTimePair());
		WeekTimeRule closeWtr = WeekTimeRule.createInstance(weekPair3,
				timePairList);
		List<WeekTimeRule> closeWtrList = new ArrayList<WeekTimeRule>();
		closeWtrList.add(closeWtr);

		CloseRule cr = CloseRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), closeWtrList,
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, cr).toString();
	}

	@Override
	public String getName() {
		return WeekendWork.class.getName();
	}

}
