package com.mioji.miner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import com.mioji.ds.CloseRule;
import com.mioji.ds.MiojiPair;
import com.mioji.ds.OpenRule;
import com.mioji.ds.SpotRule;
import com.mioji.ds.TimeRule;
import com.mioji.parse.EmptyListCreator;
import com.mioji.parse.TimeSubExtractor;

public class OnlyTime implements Minable {

	@Override
	public boolean check(String timeTextInfo) throws IOException {
		Matcher matcher = PatternManager.ONLY_TIME_PAT.matcher(timeTextInfo);

		return matcher.matches();
	}

	@Override
	public String doParse(String id, String timeTextInfo) {
		List<String> timeList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"[0-9]{1,2}[.:][0-9]{1,2}");
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		int size = timeList.size();
		for (int i = 0; i < size; i = i + 2) {
			timePairList.add(MiojiPair.createInstance(timeList.get(i),
					timeList.get(i + 1)));
		}
		TimeRule rule = TimeRule.createInstance(timePairList);
		// System.out.println("timeTextInfo:" + timeTextInfo + "\t" +
		// "TimeRule:"
		// + rule.toString());
		// System.out.println("rule:" + rule.toString());
		OpenRule or = OpenRule.createInstance(rule,
				EmptyListCreator.getEmptyDTR(), EmptyListCreator.getEmptyWTR(),
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id,or, CloseRule.createEmpty()).toString();
	}

	@Override
	public String getName() {
		return OnlyTime.class.getName();
	}

}
