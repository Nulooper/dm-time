package com.mioji.miner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import com.mioji.ds.CloseRule;
import com.mioji.ds.MiojiPair;
import com.mioji.ds.MonthTimeRule;
import com.mioji.ds.OpenRule;
import com.mioji.ds.SpotRule;
import com.mioji.ds.TimeRule;
import com.mioji.parse.EmptyListCreator;
import com.mioji.parse.TimeSubExtractor;

public class MonthTime implements Minable {

	@Override
	public boolean check(String timeTextInfo) throws IOException {
		Matcher matcher = PatternManager.MONTH_TIME_PAT.matcher(timeTextInfo);

		return matcher.matches();
	}

	@Override
	public String doParse(String id, String timeTextInfo) {
		List<String> monthList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"\\d{1,2}月");
		MiojiPair monthPair = MiojiPair.createInstance(monthList.get(0),
				monthList.get(1));
		List<String> timeList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"\\d{1,2}[:.]\\d{1,2}");
		MiojiPair timePair = MiojiPair.createInstance(timeList.get(0),
				timeList.get(1));
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		timePairList.add(timePair);
		MonthTimeRule mtr = MonthTimeRule.createInstance(monthPair,
				timePairList);
		List<MonthTimeRule> mtrList = new ArrayList<MonthTimeRule>();
		mtrList.add(mtr);

		OpenRule or = OpenRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), EmptyListCreator.getEmptyWTR(),
				mtrList, EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, CloseRule.createEmpty()).toString();
	}

	@Override
	public String getName() {
		return MonthTime.class.getName();
	}

}
