package com.mioji.miner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import com.mioji.ds.CloseRule;
import com.mioji.ds.MiojiPair;
import com.mioji.ds.OpenRule;
import com.mioji.ds.SpotRule;
import com.mioji.ds.TimeRule;
import com.mioji.ds.WeekTimeRule;
import com.mioji.parse.EmptyListCreator;
import com.mioji.parse.TimeSubExtractor;

public class OnlyWeek implements Minable {

	@Override
	public boolean check(String timeTextInfo) throws IOException {
		Matcher matcher = PatternManager.ONLY_WEEK_PAIR_PAT
				.matcher(timeTextInfo);

		return matcher.matches();
	}

	@Override
	public String doParse(String id, String timeTextInfo) {
		List<String> weekList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"周\\d");
		MiojiPair weekPair = MiojiPair.createInstance(weekList.get(0),
				weekList.get(1));
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		timePairList.add(MiojiPair.createDefaultOpenTimePair());// 当时间模式为"周x-周x"时，默认为白天开放.
		WeekTimeRule wtr = WeekTimeRule.createInstance(weekPair, timePairList);
		List<WeekTimeRule> wtrList = new ArrayList<WeekTimeRule>();
		wtrList.add(wtr);
		OpenRule or = OpenRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), wtrList,
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, CloseRule.createEmpty())
				.toString();
	}

	@Override
	public String getName() {
		return OnlyWeek.class.getName();
	}

}
