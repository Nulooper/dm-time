package com.mioji.miner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import com.mioji.ds.CloseRule;
import com.mioji.ds.MiojiPair;
import com.mioji.ds.OpenRule;
import com.mioji.ds.SpotRule;
import com.mioji.ds.TimeRule;
import com.mioji.ds.WeekTimeRule;
import com.mioji.parse.EmptyListCreator;
import com.mioji.parse.TimeSubExtractor;

public class WeekNTimes implements Minable {

	@Override
	public boolean check(String timeTextInfo) throws IOException {
		Matcher matcher = PatternManager.WEEK_N_TIMES_PAT.matcher(timeTextInfo);

		return matcher.matches();
	}

	@Override
	public String doParse(String id, String timeTextInfo) {
		// System.out.println("timeTextInfo:" + timeTextInfo);
		List<String> weekList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"周\\d");
		// System.out.println("weekList:" + weekList.size());
		MiojiPair weekPair = MiojiPair.createInstance(weekList.get(0),
				weekList.get(1));
		List<String> timeList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"\\d{1,2}[:.]\\d{1,2}");
		// System.out.println("timeList:" + timeList.size());
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		int size = timeList.size();
		for (int i = 0; i < size; i = i + 2) {
			timePairList.add(MiojiPair.createInstance(timeList.get(i),
					timeList.get(i + 1)));
		}
		WeekTimeRule wtr = WeekTimeRule.createInstance(weekPair, timePairList);
		List<WeekTimeRule> wtrList = new ArrayList<WeekTimeRule>();
		wtrList.add(wtr);
		OpenRule or = OpenRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), wtrList,
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, CloseRule.createEmpty())
				.toString();
	}

	@Override
	public String getName() {
		return WeekNTimes.class.getName();
	}

}
