package com.mioji.miner;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.SpotRule;

public class WholeDayClose implements Minable {
	private static final String CLOSE_ALL_DICT = "dict/close.dict";

	@Override
	public boolean check(String timeTextInfo) throws IOException {
		boolean isValid = false;
		List<String> patLines = FileUtils.readLines(new File(CLOSE_ALL_DICT));
		for (String pat : patLines) {
			isValid = isValid || pat.equals(timeTextInfo);
		}
		return isValid;
	}

	@Override
	public String doParse(String id, String timeTextInfo) {
		return SpotRule.getAlwaysClose(id);
	}

	@Override
	public String getName() {
		return WholeDayClose.class.getName();
	}

	public static void main(String[] args) {
		System.out.println(WholeDayClose.class.getCanonicalName());
	}

}
