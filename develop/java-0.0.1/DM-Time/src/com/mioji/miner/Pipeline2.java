package com.mioji.miner;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.CloseRule;
import com.mioji.ds.DayTimeRule;
import com.mioji.ds.MiojiPair;
import com.mioji.ds.MonthTimeRule;
import com.mioji.ds.OpenRule;
import com.mioji.ds.SpotRule;
import com.mioji.ds.TimeRule;
import com.mioji.ds.WeekTimeRule;
import com.mioji.parse.EmptyListCreator;
import com.mioji.parse.OnlyTimeParser;
import com.mioji.parse.SpotTimeText;
import com.mioji.parse.TimeSubExtractor;
import com.mioji.parse.WeekTimeParser;

/**
 * 新方法实现的时间识别.
 *
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年6月28日 下午4:36:42
 *
 */
public class Pipeline2 {
	private static final String WHOLE_DAY_DICT = "dict/whole_day.dict";
	private static final String CLOSE_ALL_DICT = "dict/close.dict";
	private static final String WHITE_DAY_DICT = "dict/white_day.dict";
	// private static final String WEEKEND_DICT = "dict/weekend.dict";
	private static final String UNKNOWN_DICT = "dict/unknown.dict";

	private List<String> ruleList = new ArrayList<String>();
	private Map<String, Integer> recogMap = new HashMap<String, Integer>();

	public List<String> getRuleList() {
		return ruleList;
	}

	private List<String> recognizedList = new ArrayList<String>();

	public List<String> getRecognizedList() {
		return this.recognizedList;
	}

	public List<String> getUnrecognizedList() {
		Collections.sort(this.unrecognizedList);
		return unrecognizedList;
	}

	private List<String> unrecognizedList = new ArrayList<String>();

	private Map<String, Integer> matchMap = new HashMap<String, Integer>();

	/*
	 * 新添加变量:识别的模式类型对应的时间文本列表.
	 */
	private Map<String, List<String>> patternTimeTextsMap = new HashMap<String, List<String>>();

	public Map<String, Integer> getMatchMap() {
		return Collections.unmodifiableMap(this.matchMap);
	}

	private OnlyTimeParser onlyTimeParser = new OnlyTimeParser();
	private WeekTimeParser weekTimeParser = new WeekTimeParser();

	public void parse(String rawText) throws IOException {
		rawText = rawText.toLowerCase();
		SpotTimeText stt = SpotTimeText.createInstance(rawText);
		String timeTextInfo = stt.getTimeText();
		String id = stt.getId();

		if (checkWholeDay(timeTextInfo)) {
			// ruleList.add(doParseWholeDay(timeTextInfo));
			ruleList.add(SpotRule.getAlwaysOpen(id));
			recognizedList.add(timeTextInfo);
			addToMap("WHOLE_DAY");
			addToPatterTimeMap("WHOLE_DAY", timeTextInfo);
			// System.out.println(timeTextInfo);
			// System.out.println(SpotRule.getAlwaysOpen(id));
		} else if (checkCloseAll(timeTextInfo)) {
			// ruleList.add(doParserCloseAll(timeTextInfo));
			ruleList.add(SpotRule.getAlwaysClose(id));
			recognizedList.add(timeTextInfo);
			addToMap("CLOSE_ALL");
			addToPatterTimeMap("CLOSE_ALL", timeTextInfo);
			// System.out.println(timeTextInfo);
			// System.out.println(SpotRule.getAlwaysClose(id));
		} else if (checkWhiteDay(timeTextInfo)) {
			ruleList.add(doParseWhiteDay(id, timeTextInfo));
			recognizedList.add(timeTextInfo);
			addToMap("WHITE_DAY");
			addToPatterTimeMap("WHITE_DAY", timeTextInfo);
			// System.out.println(timeTextInfo);
			// System.out.println(doParseWhiteDay(id, timeTextInfo));
		} else if (checkUnknown(timeTextInfo)) {
			// ruleList.add(doParseUnknown(timeTextInfo));
			ruleList.add(SpotRule.getUnknown(id));
			recognizedList.add(timeTextInfo);
			addToMap("UNKNOWN");
			addToPatterTimeMap("UNKNOWN", timeTextInfo);
			// System.out.println(timeTextInfo);
			// System.out.println(SpotRule.getUnknown(id));
		} else if (checkOnlyTime(timeTextInfo)) {
			ruleList.add(doParserOnlyTime(id, timeTextInfo));
			recognizedList.add(timeTextInfo);
			addToMap("ONLY_TIME");
			addToPatterTimeMap("ONLY_TIME", timeTextInfo);
			// System.out.println("timeTextInfo:" + timeTextInfo);
			// System.out.println("解析结果:" + doParserOnlyTime(id, timeTextInfo));
		} else if (OnlyTimeCleaner.check2(timeTextInfo)) {
			// System.out.println(timeTextInfo);
			timeTextInfo = OnlyTimeCleaner.doClean2(timeTextInfo);
			ruleList.add(doParserOnlyTime(id, timeTextInfo));
			recognizedList.add(timeTextInfo);
			addToMap("ONLY_TIME");
			addToPatterTimeMap("ONLY_TIME", timeTextInfo);
			// System.out.println("timeTextInfo:" + timeTextInfo);
			// System.out.println(doParserOnlyTime(id, timeTextInfo));
		} else if (onlyTimeParser.isMatched(stt)) {
			ruleList.add(onlyTimeParser.parse(stt));
			recognizedList.add(timeTextInfo);
			addToMap("ONLY_TIME");
			addToPatterTimeMap("ONLY_TIME", timeTextInfo);
			// System.out.println("timeTextInfo:" + timeTextInfo);
			// System.out.println(onlyTimeParser.parse(stt));
		} else if (weekTimeParser.isMatched(WeekTimeCleaner.preClean(stt))) {

			SpotTimeText cleaned = WeekTimeCleaner.preClean(stt);
			ruleList.add(weekTimeParser.parse(cleaned));
			recognizedList.add(timeTextInfo);
			addToMap("WEEK_TIME");
			addToPatterTimeMap("WEEK_TIME", timeTextInfo);
			// System.out.println(timeTextInfo);
			// System.out.println(weekTimeParser.parse(cleaned));
		} else if (checkTimeWithBlack(timeTextInfo)) {
			// System.out.println(timeTextInfo);
			ruleList.add(doParseTimeWithBlack(id, timeTextInfo));
			recognizedList.add(timeTextInfo);
			addToMap("TIME_WITH_BLACK");
			addToPatterTimeMap("TIME_WITH_BLACK", timeTextInfo);
			// System.out.println(timeTextInfo);
			// System.out.println(doParseTimeWithBlack(id, timeTextInfo));
		} else if (checkOnlyMonth(timeTextInfo)) {
			ruleList.add(doParserOnlyMonth(id, timeTextInfo));
			recognizedList.add(timeTextInfo);
			addToMap("ONLY_MONTH");
			addToPatterTimeMap("ONLY_MONTH", timeTextInfo);
			// System.out.println(timeTextInfo);
			// System.out.println(doParserOnlyMonth(id, timeTextInfo));
		} else if (checkOnlyWeekPair(timeTextInfo)) {
			ruleList.add(doParseOnlyWeekPair(id, timeTextInfo));
			recognizedList.add(timeTextInfo);
			addToMap("ONLY_WEEK_PAIR");
			addToPatterTimeMap("ONLY_WEEK_PAIR", timeTextInfo);
			// System.out.println(timeTextInfo);
			// System.out.println(doParseOnlyWeekPair(id, timeTextInfo));
		} else if (checkWeekNTimes(timeTextInfo)) {
			ruleList.add(doParseWeekNTimes(id, timeTextInfo));
			recognizedList.add(timeTextInfo);
			addToMap("WEEK_N_TIMES");
			addToPatterTimeMap("WEEK_N_TIMES", timeTextInfo);
			// System.out.println(timeTextInfo);
			// System.out.println(doParseWeekNTimes(id, timeTextInfo));
		} else if (checkDetailedAllWeek(timeTextInfo)) {

			ruleList.add(doParserDetailedAllWeek(id, timeTextInfo));
			recognizedList.add(timeTextInfo);
			addToMap("DETAILED_WEEK");
			addToPatterTimeMap("DETAILED_WEEK", timeTextInfo);
			// System.out.println(timeTextInfo);
			// System.out.println(doParserDetailedAllWeek(id, timeTextInfo));
		} else if (checkWeekendWork(timeTextInfo)) {
			// System.out.println(timeTextInfo);
			ruleList.add(doParseWeekendWork(id, timeTextInfo));
			recognizedList.add(timeTextInfo);
			addToMap("WEEKEND_WORK");
			addToPatterTimeMap("WEEKEND_WORK", timeTextInfo);
			// System.out.println(timeTextInfo);
			// System.out.println(doParseWeekendWork(id, timeTextInfo));
		} else if (checkMonthTime(timeTextInfo)) {

			ruleList.add(doParseMonthTime(id, timeTextInfo));
			recognizedList.add(timeTextInfo);
			addToMap("MONTH_TIME");
			addToPatterTimeMap("MONTH_TIME", timeTextInfo);
			// System.out.println(timeTextInfo);
			// System.out.println(doParseMonthTime(id, timeTextInfo));
		} else if (checkNTimesWeekClose(timeTextInfo)) {
			// System.out.println(timeTextInfo);
			ruleList.add(doParseNTimeWeekClose(id, timeTextInfo));
			recognizedList.add(timeTextInfo);
			addToMap("N_TIMES_WEEK_CLOSE");
			addToPatterTimeMap("N_TIMES_WEEK_CLOSE", timeTextInfo);
			// System.out.println(timeTextInfo);
			// System.out.println(doParseNTimeWeekClose(id, timeTextInfo));
		} else {
			/**
			 * 不能识别的经典，默认使用全天.
			 */
			ruleList.add(SpotRule.getAlwaysOpen(id));
			unrecognizedList.add(timeTextInfo);
			addToMap("DEFAULT");
			addToPatterTimeMap("DEFAULT", timeTextInfo);
		}

	}

	private boolean checkWholeDay(String timeTextInfo) throws IOException {
		boolean isValid = false;
		List<String> patLines = FileUtils.readLines(new File(WHOLE_DAY_DICT));
		for (String pat : patLines) {
			isValid = isValid || pat.equals(timeTextInfo);
		}
		return isValid;
	}

	/**
	 * 处理时间信息仅包含"全天"的情况.即00:00-24:00
	 * 
	 * @param rawText
	 */
	@SuppressWarnings("unused")
	private String doParseWholeDay(String id, String timeTextInfo) {
		OpenRule or = OpenRule.createInstance(TimeRule.createWholeDay(),
				EmptyListCreator.getEmptyDTR(), EmptyListCreator.getEmptyWTR(),
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, CloseRule.createEmpty())
				.toString();
	}

	private boolean checkCloseAll(String timeTextInfo) throws IOException {
		boolean isValid = false;
		List<String> patLines = FileUtils.readLines(new File(CLOSE_ALL_DICT));
		for (String pat : patLines) {
			isValid = isValid || pat.equals(timeTextInfo);
		}
		return isValid;
	}

	@SuppressWarnings("unused")
	private String doParserCloseAll(String id, String timeTextInfo) {
		OpenRule or = OpenRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), EmptyListCreator.getEmptyWTR(),
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		List<DayTimeRule> dtrList = new ArrayList<DayTimeRule>();
		dtrList.add(DayTimeRule.createDefaultDay());
		CloseRule cr = CloseRule.createInstance(TimeRule.createEmpty(),
				dtrList, EmptyListCreator.getEmptyWTR(),
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, cr).toString();
	}

	private boolean checkWhiteDay(String timeTextInfo) throws IOException {
		boolean isValid = false;
		List<String> patLines = FileUtils.readLines(new File(WHITE_DAY_DICT));
		for (String pat : patLines) {
			isValid = isValid || pat.equals(timeTextInfo);
		}
		return isValid;
	}

	/**
	 * 处理白天的情况.即06:00-18:00
	 * 
	 * @param rawText
	 */
	private String doParseWhiteDay(String id, String timeTextInfo) {
		OpenRule or = OpenRule.createInstance(TimeRule.createWhiteDay(),
				EmptyListCreator.getEmptyDTR(), EmptyListCreator.getEmptyWTR(),
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, CloseRule.createEmpty())
				.toString();
	}

	private boolean checkUnknown(String timeTextInfo) throws IOException {
		boolean isValid = false;
		List<String> patLines = FileUtils.readLines(new File(UNKNOWN_DICT));
		for (String pat : patLines) {
			isValid = isValid || pat.equals(timeTextInfo);
		}
		return isValid;
	}

	@SuppressWarnings("unused")
	private String doParseUnknown(String id, String timeTextInfo) {
		return SpotRule.getEmptyFormat(id);
	}

	private boolean checkOnlyTime(String timeTextInfo) {
		Matcher matcher = PatternManager.ONLY_TIME_PAT.matcher(timeTextInfo);

		return matcher.matches();
	}

	private String doParserOnlyTime(String id, String timeTextInfo) {

		List<String> timeList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"[0-9]{1,2}[.:][0-9]{1,2}");
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		int size = timeList.size();
		for (int i = 0; i < size; i = i + 2) {
			timePairList.add(MiojiPair.createInstance(timeList.get(i),
					timeList.get(i + 1)));
		}
		TimeRule rule = TimeRule.createInstance(timePairList);
		// System.out.println("timeTextInfo:" + timeTextInfo + "\t" +
		// "TimeRule:"
		// + rule.toString());
		// System.out.println("rule:" + rule.toString());
		OpenRule or = OpenRule.createInstance(rule,
				EmptyListCreator.getEmptyDTR(), EmptyListCreator.getEmptyWTR(),
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, CloseRule.createEmpty())
				.toString();
	}

	private boolean checkTimeWithBlack(String timeTextInfo) {
		Matcher matcher = PatternManager.TIME_BLACK_DAY_PAT
				.matcher(timeTextInfo);

		return matcher.matches();
	}

	private String doParseTimeWithBlack(String id, String timeTextInfo) {
		List<String> timeList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"[0-9]{1,2}[.:][0-9]{1,2}");
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		int size = timeList.size();
		for (int i = 0; i < size; i = i + 2) {
			timePairList.add(MiojiPair.createInstance(timeList.get(i),
					timeList.get(i + 1)));
		}
		TimeRule rule = TimeRule.createInstance(timePairList);
		OpenRule or = OpenRule.createInstance(rule,
				EmptyListCreator.getEmptyDTR(), EmptyListCreator.getEmptyWTR(),
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		List<String> weekList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"周\\d");
		List<WeekTimeRule> wtrList = new ArrayList<WeekTimeRule>();
		for (String day : weekList) {
			List<MiojiPair> tmpList = new ArrayList<MiojiPair>();
			tmpList.add(MiojiPair.createDefaultTimePair());
			wtrList.add(WeekTimeRule.createInstance(MiojiPair.createInstance(
					day, day)));
		}
		CloseRule cr = CloseRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), wtrList,
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, cr).toString();
	}

	private boolean checkOnlyMonth(String timeTextInfo) {
		Matcher matcher = PatternManager.ONLY_MONTH_PAIR_PAT
				.matcher(timeTextInfo);
		return matcher.matches();
	}

	private String doParserOnlyMonth(String id, String timeTextInfo) {
		List<String> onlyMonthList = TimeSubExtractor.extractByGroup(
				timeTextInfo, "\\d{1,2}月");
		int size = onlyMonthList.size();
		List<MonthTimeRule> mtrList = new ArrayList<MonthTimeRule>();
		for (int i = 0; i < size; i = 1 + 2) {
			MiojiPair monthPair = MiojiPair.createInstance(
					onlyMonthList.get(i), onlyMonthList.get(i + 1));
			mtrList.add(MonthTimeRule.createInstance(monthPair,
					new ArrayList<MiojiPair>()));
		}

		OpenRule or = OpenRule.createInstance(TimeRule.createDefaultOpen(),
				EmptyListCreator.getEmptyDTR(), EmptyListCreator.getEmptyWTR(),
				mtrList, EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, CloseRule.createEmpty())
				.toString();
	}

	private boolean checkOnlyWeekPair(String timeTextInfo) {
		Matcher matcher = PatternManager.ONLY_WEEK_PAIR_PAT
				.matcher(timeTextInfo);

		return matcher.matches();
	}

	private String doParseOnlyWeekPair(String id, String timeTextInfo) {
		List<String> weekList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"周\\d");
		MiojiPair weekPair = MiojiPair.createInstance(weekList.get(0),
				weekList.get(1));
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		timePairList.add(MiojiPair.createDefaultOpenTimePair());// 当时间模式为"周x-周x"时，默认为白天开放.
		WeekTimeRule wtr = WeekTimeRule.createInstance(weekPair, timePairList);
		List<WeekTimeRule> wtrList = new ArrayList<WeekTimeRule>();
		wtrList.add(wtr);
		OpenRule or = OpenRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), wtrList,
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, CloseRule.createEmpty())
				.toString();
	}

	private boolean checkWeekNTimes(String timeTextInfo) {
		Matcher matcher = PatternManager.WEEK_N_TIMES_PAT.matcher(timeTextInfo);

		return matcher.matches();
	}

	private String doParseWeekNTimes(String id, String timeTextInfo) {
		// System.out.println("timeTextInfo:" + timeTextInfo);
		List<String> weekList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"周\\d");
		// System.out.println("weekList:" + weekList.size());
		MiojiPair weekPair = MiojiPair.createInstance(weekList.get(0),
				weekList.get(1));
		List<String> timeList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"\\d{1,2}[:.]\\d{1,2}");
		// System.out.println("timeList:" + timeList.size());
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		int size = timeList.size();
		for (int i = 0; i < size; i = i + 2) {
			timePairList.add(MiojiPair.createInstance(timeList.get(i),
					timeList.get(i + 1)));
		}
		WeekTimeRule wtr = WeekTimeRule.createInstance(weekPair, timePairList);
		List<WeekTimeRule> wtrList = new ArrayList<WeekTimeRule>();
		wtrList.add(wtr);
		OpenRule or = OpenRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), wtrList,
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, CloseRule.createEmpty())
				.toString();
	}

	private boolean checkDetailedAllWeek(String timeTextInfo) {
		Matcher matcher = PatternManager.DETAILED_ALL_WEEKDAY_PAT
				.matcher(timeTextInfo);

		return matcher.matches();
	}

	private String doParserDetailedAllWeek(String id, String timeTextInfo) {
		List<String> weekList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"周\\d");
		MiojiPair weekPair1 = MiojiPair.createInstance(weekList.get(0),
				weekList.get(1));
		MiojiPair weekPair2 = MiojiPair.createInstance(weekList.get(2),
				weekList.get(2));
		MiojiPair weekPair3 = MiojiPair.createInstance(weekList.get(3),
				weekList.get(3));

		List<String> timeList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"\\d{1,2}[.:]\\d{1,2}");
		List<MiojiPair> timePairList1 = new ArrayList<MiojiPair>();
		timePairList1.add(MiojiPair.createInstance(timeList.get(0),
				timeList.get(1)));
		List<MiojiPair> timePairList2 = new ArrayList<MiojiPair>();
		timePairList2.add(MiojiPair.createInstance(timeList.get(2),
				timeList.get(3)));
		List<MiojiPair> timePairList3 = new ArrayList<MiojiPair>();
		timePairList3.add(MiojiPair.createInstance(timeList.get(4),
				timeList.get(5)));

		WeekTimeRule wtr1 = WeekTimeRule.createInstance(weekPair1,
				timePairList1);
		WeekTimeRule wtr2 = WeekTimeRule.createInstance(weekPair2,
				timePairList2);
		WeekTimeRule wtr3 = WeekTimeRule.createInstance(weekPair3,
				timePairList3);

		List<WeekTimeRule> wtrList = new ArrayList<WeekTimeRule>();
		wtrList.add(wtr1);
		wtrList.add(wtr2);
		wtrList.add(wtr3);

		OpenRule or = OpenRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), wtrList,
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, CloseRule.createEmpty())
				.toString();
	}

	private boolean checkWeekendWork(String timeTextInfo) {
		Matcher matcher = PatternManager.WEEKEND_WORK_PAT.matcher(timeTextInfo);
		return matcher.matches();
	}

	private String doParseWeekendWork(String id, String timeTextInfo) {
		List<String> weekList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"周\\d");
		MiojiPair weekPair1 = MiojiPair.createInstance(weekList.get(0),
				weekList.get(1));
		MiojiPair weekPair2 = MiojiPair.createInstance(weekList.get(2),
				weekList.get(2));
		MiojiPair weekPair3 = MiojiPair.createInstance(weekList.get(3),
				weekList.get(3));

		List<String> timeList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"\\d{1,2}[.:]\\d{1,2}");
		List<MiojiPair> timePairList1 = new ArrayList<MiojiPair>();
		timePairList1.add(MiojiPair.createInstance(timeList.get(0),
				timeList.get(1)));
		List<MiojiPair> timePairList2 = new ArrayList<MiojiPair>();
		timePairList2.add(MiojiPair.createInstance(timeList.get(2),
				timeList.get(3)));

		WeekTimeRule openWtr1 = WeekTimeRule.createInstance(weekPair1,
				timePairList1);
		WeekTimeRule openWtr2 = WeekTimeRule.createInstance(weekPair2,
				timePairList2);

		List<WeekTimeRule> openWtrList = new ArrayList<WeekTimeRule>();
		openWtrList.add(openWtr1);
		openWtrList.add(openWtr2);

		OpenRule or = OpenRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), openWtrList,
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		timePairList.add(MiojiPair.createDefaultOpenTimePair());
		WeekTimeRule closeWtr = WeekTimeRule.createInstance(weekPair3,
				timePairList);
		List<WeekTimeRule> closeWtrList = new ArrayList<WeekTimeRule>();
		closeWtrList.add(closeWtr);

		CloseRule cr = CloseRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), closeWtrList,
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, cr).toString();
	}

	private boolean checkMonthTime(String timeTextInfo) {
		Matcher matcher = PatternManager.MONTH_TIME_PAT.matcher(timeTextInfo);

		return matcher.matches();
	}

	private String doParseMonthTime(String id, String timeTextInfo) {
		List<String> monthList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"\\d{1,2}月");
		MiojiPair monthPair = MiojiPair.createInstance(monthList.get(0),
				monthList.get(1));
		List<String> timeList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"\\d{1,2}[:.]\\d{1,2}");
		MiojiPair timePair = MiojiPair.createInstance(timeList.get(0),
				timeList.get(1));
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		timePairList.add(timePair);
		MonthTimeRule mtr = MonthTimeRule.createInstance(monthPair,
				timePairList);
		List<MonthTimeRule> mtrList = new ArrayList<MonthTimeRule>();
		mtrList.add(mtr);

		OpenRule or = OpenRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), EmptyListCreator.getEmptyWTR(),
				mtrList, EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, CloseRule.createEmpty())
				.toString();
	}

	private boolean checkNTimesWeekClose(String timeTextInfo) {
		Matcher matcher = PatternManager.N_TIMES_WEEK_CLOSE_PAT
				.matcher(timeTextInfo);

		return matcher.matches();
	}

	private String doParseNTimeWeekClose(String id, String timeTextInfo) {
		List<String> timeList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"\\d{1,2}[:.]\\d{1,2}");
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		int size = timeList.size();
		for (int i = 0; i < size; i = i + 2) {
			timePairList.add(MiojiPair.createInstance(timeList.get(i),
					timeList.get(i + 1)));
		}
		TimeRule tr = TimeRule.createInstance(timePairList);
		OpenRule or = OpenRule.createInstance(tr,
				EmptyListCreator.getEmptyDTR(), EmptyListCreator.getEmptyWTR(),
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		List<String> weekList = TimeSubExtractor.extractByGroup(timeTextInfo,
				"周\\d");
		// System.out.println(weekList.size() + ":\t" + weekList.get(0));
		WeekTimeRule wtr = WeekTimeRule.createInstance(MiojiPair
				.createInstance(weekList.get(0), weekList.get(0)));
		List<WeekTimeRule> wtrList = new ArrayList<WeekTimeRule>();
		wtrList.add(wtr);
		CloseRule cr = CloseRule.createInstance(TimeRule.createEmpty(),
				EmptyListCreator.getEmptyDTR(), wtrList,
				EmptyListCreator.getEmptyMTR(), EmptyListCreator.getEmptyQTR());

		return SpotRule.createInstance(id, or, cr).toString();
	}

	public int getRecogCount() {
		return this.recognizedList.size();// + regexParser.getMatchedCount();
	}

	public int getUnRecogCount() {
		return this.unrecognizedList.size();
	}

	public double getRecogRate() {
		return (this.recognizedList.size()) * 1.0
				/ (this.unrecognizedList.size() + this.recognizedList.size());
	}

	public int getTotalCount() {
		return this.recognizedList.size() + this.unrecognizedList.size();
	}

	private void addToMap(String parserName) {
		if (this.recogMap.containsKey(parserName)) {
			this.recogMap.put(parserName, this.recogMap.get(parserName) + 1);
		} else {
			this.recogMap.put(parserName, 1);
		}
	}

	/**
	 * 新添加函数.
	 * 
	 * @param parserName
	 * @param timeText
	 */
	private void addToPatterTimeMap(String parserName, String timeText) {
		if (null == parserName || parserName.isEmpty() || null == timeText
				|| timeText.isEmpty()) {
			return;
		}

		if (this.patternTimeTextsMap.containsKey(parserName)) {
			this.patternTimeTextsMap.get(parserName).add(timeText);
		} else {
			List<String> timeTextList = new ArrayList<String>();
			this.patternTimeTextsMap.put(parserName, timeTextList);
		}
	}

	public Map<String, Integer> getRecogMap() {
		return Collections.unmodifiableMap(this.recogMap);
	}

	public Map<String, List<String>> getPatternTimeTextsMap() {
		return Collections.unmodifiableMap(this.patternTimeTextsMap);
	}

	public static void main(String[] args) {

	}
}
