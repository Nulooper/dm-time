package com.mioji.ir;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

/**
 * 对解析程序进行测试.
 *
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年7月2日 下午5:51:45
 *
 */
public class TestIR {

	public static final List<String> TEST_DAYS = new ArrayList<String>();
	static {
		for (int i = 1; i <= 7; ++i) {
			TEST_DAYS.add(20140713 + i + "");
			TEST_DAYS.add(20140113 + i + "");
		}
	}

	public static void createTestRule() throws IOException {
		// 合并时间规则.
		List<String> ruleLines = FileUtils
				.readLines(new File("data/recog.log"));
		List<String> ruleTexts = new ArrayList<String>();
		int size = ruleLines.size();
		for (int i = 0; i < size; i = i + 2) {
			ruleTexts.add(ruleLines.get(i) + "&&" + ruleLines.get(i + 1));
		}

		FileUtils.writeLines(new File("test/test_rules"), ruleTexts);
	}

	public static void createAlwaysTestRules() throws IOException {
		List<String> ruleLines = FileUtils
				.readLines(new File("test/test_rules"));
		List<String> alwaysLines = new ArrayList<String>();
		for (String ruleLine : ruleLines) {
			if (ruleLine.contains("ALWAYS")) {
				alwaysLines.add(ruleLine);
			}
		}

		FileUtils.writeLines(new File("test/always.rule"), alwaysLines);
	}

	public static void createUnknownTestRules() throws IOException {
		List<String> ruleLines = FileUtils
				.readLines(new File("test/test_rules"));
		List<String> alwaysLines = new ArrayList<String>();
		for (String ruleLine : ruleLines) {
			if (ruleLine.contains("UNKNOWN")) {
				alwaysLines.add(ruleLine);
			}
		}

		FileUtils.writeLines(new File("test/unknown.rule"), alwaysLines);
	}

	public static void createNoTestRules() throws IOException {
		List<String> ruleLines = FileUtils
				.readLines(new File("test/test_rules"));
		List<String> alwaysLines = new ArrayList<String>();
		for (String ruleLine : ruleLines) {
			if (ruleLine.contains("NO")) {
				alwaysLines.add(ruleLine);
			}
		}

		FileUtils.writeLines(new File("test/no.rule"), alwaysLines);
	}

	public static void createWeekTimeTestRules() throws IOException {
		List<String> ruleLines = FileUtils
				.readLines(new File("test/test_rules"));
		List<String> alwaysLines = new ArrayList<String>();
		for (String ruleLine : ruleLines) {
			if (ruleLine.contains("WEEK_TIME_RULE")) {
				alwaysLines.add(ruleLine);
			}
		}

		FileUtils.writeLines(new File("test/weektime.rule"), alwaysLines);
	}

	public static void createDayTimeTestRules() throws IOException {
		List<String> ruleLines = FileUtils
				.readLines(new File("test/test_rules"));
		List<String> alwaysLines = new ArrayList<String>();
		for (String ruleLine : ruleLines) {
			if (ruleLine.contains("DAY_TIME_RULE")) {
				alwaysLines.add(ruleLine);
			}
		}

		FileUtils.writeLines(new File("test/daytime.rule"), alwaysLines);
	}

	public static void createTimeTestRules() throws IOException {
		List<String> ruleLines = FileUtils
				.readLines(new File("test/test_rules"));
		List<String> alwaysLines = new ArrayList<String>();
		for (String ruleLine : ruleLines) {
			if (ruleLine.contains("}\tTIME_RULE")) {
				System.out.println(ruleLine);
				alwaysLines.add(ruleLine);
			}
		}

		FileUtils.writeLines(new File("test/time.rule"), alwaysLines);
	}

	public static void createMonthTimeTestRules() throws IOException {
		List<String> ruleLines = FileUtils
				.readLines(new File("test/test_rules"));
		List<String> alwaysLines = new ArrayList<String>();
		for (String ruleLine : ruleLines) {
			if (ruleLine.contains("\tMONTH_TIME_RULE")) {
				alwaysLines.add(ruleLine);
			}
		}

		FileUtils.writeLines(new File("test/monthtime.rule"), alwaysLines);
	}

	public static void createQuarterTimeTestRules() throws IOException {
		List<String> ruleLines = FileUtils
				.readLines(new File("test/test_rules"));
		List<String> alwaysLines = new ArrayList<String>();
		for (String ruleLine : ruleLines) {
			if (ruleLine.contains("\tQUARTER_TIMES")) {
				alwaysLines.add(ruleLine);
			}
		}

		FileUtils.writeLines(new File("test/quartertime.rule"), alwaysLines);
	}

	/**
	 * 分类创建测试规则.
	 * 
	 * @throws IOException
	 */
	public static void createTestRules() throws IOException {
		createAlwaysTestRules();
		createUnknownTestRules();
		createNoTestRules();
		createWeekTimeTestRules();
		createDayTimeTestRules();
		createTimeTestRules();
		createMonthTimeTestRules();
		createQuarterTimeTestRules();
	}

	/**
	 * 测试UNKNOWN规则的解析.
	 * 
	 * @throws IOException
	 */
	public static void testUnknown() throws IOException {
		System.out.println("开始测试UNKNOWN类型时间规则!");
		List<String> unknowRules = FileUtils.readLines(new File(
				"test/unknown.rule"));
		List<String> testResults = new ArrayList<String>();
		for (String rule : unknowRules) {
			StringBuilder sb = new StringBuilder();
			sb.append(rule).append("\n");
			for (String day : TEST_DAYS) {
				sb.append("\t").append(day).append(":")
						.append(IR.search(rule, day)).append("\n");
			}
			sb.append("\n");
			testResults.add(sb.toString());
		}

		FileUtils.writeLines(new File("test/unknown.results"), testResults);
		System.out.println("测试UNKNOWN类型时间规则完成!");

	}

	/**
	 * 测试UNKNOWN规则的解析.
	 * 
	 * @throws IOException
	 */
	public static void testWeekTime() throws IOException {
		System.out.println("开始测试WeekTime类型时间规则!");
		List<String> unknowRules = FileUtils.readLines(new File(
				"test/weektime.rule"));
		List<String> testResults = new ArrayList<String>();
		for (String rule : unknowRules) {
			StringBuilder sb = new StringBuilder();
			sb.append(rule).append("\n");
			for (String day : TEST_DAYS) {
				sb.append("\t").append(day).append(":")
						.append(IR.search(rule, day)).append("\n");
			}
			sb.append("\n");
			testResults.add(sb.toString());
		}

		FileUtils.writeLines(new File("test/weektime.results"), testResults);
		System.out.println("测试weektime类型时间规则完成!");

	}

	public static void testOnlyTime() throws IOException {
		System.out.println("开始测试OnlyTime类型时间规则!");
		List<String> unknowRules = FileUtils.readLines(new File(
				"test/time.rule"));
		List<String> testResults = new ArrayList<String>();
		for (String rule : unknowRules) {
			StringBuilder sb = new StringBuilder();
			sb.append(rule).append("\n");
			for (String day : TEST_DAYS) {
				sb.append("\t").append(day).append(":")
						.append(IR.search(rule, day)).append("\n");
			}
			sb.append("\n");
			testResults.add(sb.toString());
		}

		FileUtils.writeLines(new File("test/time.results"), testResults);
		System.out.println("测试time类型时间规则完成!");

	}

	public static void testMonthTime() throws IOException {
		System.out.println("开始测试MonthTime类型时间规则!");
		List<String> unknowRules = FileUtils.readLines(new File(
				"test/monthtime.rule"));
		List<String> testResults = new ArrayList<String>();
		for (String rule : unknowRules) {
			StringBuilder sb = new StringBuilder();
			sb.append(rule).append("\n");
			for (String day : TEST_DAYS) {
				sb.append("\t").append(day).append(":")
						.append(IR.search(rule, day)).append("\n");
			}
			sb.append("\n");
			testResults.add(sb.toString());
		}

		FileUtils.writeLines(new File("test/monthtime.results"), testResults);
		System.out.println("测试monthtime类型时间规则完成!");

	}

	public static void main(String[] args) throws IOException {
		createTestRule();
		 createTestRules();
		// testUnknown();
		// testWeekTime();
		// testOnlyTime();
		testMonthTime();
	}
}
