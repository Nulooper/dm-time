package com.mioji.generate;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.mioji.preprocess.StringHelper;

/**
 * 生成器. 包括生成dict.define, pattern.define, tag.define, variable.define
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @date 2014年7月30日 下午2:45:56
 */
public class Generator {

	/*
	 * 景点一直开放词典.
	 */
	private static final String ALWAYS_OPEN = "always_open.dict";

	/*
	 * 景点一直关闭词典.
	 */
	private static final String ALWAYS_CLOSE = "always_close.dict";

	/*
	 * 景点补充说明关闭词典.
	 */
	private static final String ADDITIONAL_CLOSE = "additional_close.dict";

	/*
	 * 特殊日期词典.
	 */
	private static final String HOLIDAY_DATE = "holiday_date.dict";

	/*
	 * 季节日期词典.
	 */
	private static final String SEASON_DATE = "season_date.dict";

	/**
	 * 生成“景点一直开放”标签定义词典, 输出到文件always_open.define.
	 * 
	 * @throws IOException
	 */
	public static void generateAlwaysOpenDict() throws IOException {
		System.out.println("开始生成!");

		String timeType = "always_open";
		String defaultTime = "08:00至18:00";
		List<String> lines = FileUtils.readLines(new File(ALWAYS_OPEN));
		lines = new ArrayList<String>(new HashSet<String>(lines));
		Collections.sort(lines);
		List<String> defineLines = new ArrayList<String>();
		String defaultAttrValues = timeType + "|" + defaultTime;
		String defaultAttrNames = "TAG:always_open" + "\t" + "type" + "|"
				+ "default";
		defineLines.add(defaultAttrNames);
		for (String line : lines) {
			line = StringHelper.toSBC(line);
			if (line.contains("，") || line.contains("／")) {
				continue;
			}
			line = line + "\t" + defaultAttrValues;
			defineLines.add(line);
		}
		FileUtils.writeLines(new File("always_open.define"), defineLines);

		System.out.println("生成结束!");
	}

	/**
	 * 生成“景点一直开放”标签定义词典, 输出到文件always_open.define.
	 * 
	 * @throws IOException
	 */
	public static void generateAlwaysCloseDict() throws IOException {
		System.out.println("开始生成!");

		String timeType = "always_close";
		List<String> lines = FileUtils.readLines(new File(ALWAYS_CLOSE));
		lines = new ArrayList<String>(new HashSet<String>(lines));
		Collections.sort(lines);
		List<String> defineLines = new ArrayList<String>();
		String defaultAttrNames = "TAG:always_close" + "\t" + "type";
		defineLines.add(defaultAttrNames);
		for (String line : lines) {
			line = StringHelper.toSBC(line);
			if (line.contains("，") || line.contains("／")) {
				continue;
			}
			line = line + "\t" + timeType;
			defineLines.add(line);
		}
		FileUtils.writeLines(new File("always_close.define"), defineLines);

		System.out.println("生成结束!");
	}

	/**
	 * 生成“景点补充说明关闭”标签定义词典, 输出到文件additional_close.define.
	 * 
	 * @throws IOException
	 */
	public static void generateAdditionalCloseDict() throws IOException {
		System.out.println("开始生成!");

		String timeType = "additional_close";
		List<String> lines = FileUtils.readLines(new File(ADDITIONAL_CLOSE));
		lines = new ArrayList<String>(new HashSet<String>(lines));
		Collections.sort(lines);
		List<String> defineLines = new ArrayList<String>();
		String defaultAttrNames = "TAG:additional_close" + "\t" + "type";
		defineLines.add(defaultAttrNames);
		for (String line : lines) {
			line = StringHelper.toSBC(line);
			if (line.contains("，") || line.contains("／")) {
				continue;
			}
			line = line + "\t" + timeType;
			defineLines.add(line);
		}
		FileUtils.writeLines(new File("additional_close.define"), defineLines);

		System.out.println("生成结束!");
	}

	/**
	 * 生成“特殊日期”标签定义词典, 输出到文件special_date.define.
	 * 
	 * @throws IOException
	 */
	public static void generateSpecialDateDict() throws IOException {
		System.out.println("开始生成!");

		String timeType = "holiday";
		List<String> lines = FileUtils.readLines(new File(HOLIDAY_DATE));
		lines = new ArrayList<String>(new HashSet<String>(lines));
		Collections.sort(lines);
		List<String> defineLines = new ArrayList<String>();
		String defaultAttrNames = "TAG:holiday_date" + "\t" + "type";
		defineLines.add(defaultAttrNames);
		for (String line : lines) {
			line = StringHelper.toSBC(line);
			if (line.contains("，") || line.contains("／")) {
				continue;
			}
			line = line + "\t" + timeType;
			defineLines.add(line);
		}
		FileUtils.writeLines(new File("holiday_date.define"), defineLines);

		System.out.println("生成结束!");
	}

	/**
	 * 生成“季节日期”标签定义词典, 输出到文件season_date.define.
	 * 
	 * @throws IOException
	 */
	public static void generateSeasonDateDict() throws IOException {
		System.out.println("开始生成!");

		String timeType = "season";
		List<String> lines = FileUtils.readLines(new File(SEASON_DATE));
		lines = new ArrayList<String>(new HashSet<String>(lines));
		Collections.sort(lines);
		List<String> defineLines = new ArrayList<String>();
		String defaultAttrNames = "TAG:season_date" + "\t" + "type";
		defineLines.add(defaultAttrNames);
		for (String line : lines) {
			line = StringHelper.toSBC(line);
			if (line.contains("，") || line.contains("／")) {
				continue;
			}

			if (line.contains("春")) {
				timeType = "spring";
			} else if (line.contains("夏")) {
				timeType = "summer";
			} else if (line.contains("秋")) {
				timeType = "autumn";
			} else {
				timeType = "winter";
			}

			line = line + "\t" + timeType;
			defineLines.add(line);
		}
		FileUtils.writeLines(new File("season_date.define"), defineLines);

		System.out.println("生成结束!");
	}

	public static void main(String[] args) throws IOException {
		// Generator.generateAlwaysOpenDict();
		// generateAlwaysCloseDict();
		// generateAdditionalCloseDict();
		// generateSpecialDateDict();
		generateSeasonDateDict();
	}
}
