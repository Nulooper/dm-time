package com.mioji.preprocess;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;

/**
 * 在使用C++打标系统进行打标之前需要的预处理.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @date 2014年7月30日 上午11:41:59
 * 
 */
public class Preprocessor {

	public Preprocessor() {
		// wait to code here.
	}

	public static Preprocessor createInstance() {
		return new Preprocessor();
	}

	public List<String> preprocess(String srcFile) throws IOException {
		if (null == srcFile || srcFile.isEmpty()) {
			return Collections.emptyList();
		}

		List<String> lines = FileUtils.readLines(new File(srcFile));

		return preprocess(lines);
	}

	public List<String> preprocess(List<String> inputLines) {
		if (null == inputLines || inputLines.isEmpty()) {
			return Collections.emptyList();
		}

		List<String> validLines = new ArrayList<String>();
		for (String line : inputLines) {
			line = cleanNomeanSigns(line);
			line = cleanIntervalSigns(line);
			validLines.add(line);
		}

		return Collections.unmodifiableList(validLines);
	}

	/*
	 * 输入文本可能包含的无意义或者错误的符号集合.
	 */
	private static final List<String> NO_MEAN_SIGNS = new ArrayList<String>();
	static {
		NO_MEAN_SIGNS.add("");
		NO_MEAN_SIGNS.add("");
		NO_MEAN_SIGNS.add("");

		// wait to add.
	}

	@SuppressWarnings("unused")
	private String cleanNomeanSigns(String line) {
		if (null == line || line.isEmpty()) {
			return new String();
		}

		for (String sign : NO_MEAN_SIGNS) {

			// wait to code here.
		}

		return line;
	}

	/*
	 * 输入文本可能包含的区间符号的集合.
	 */
	private static final List<String> ORG_INTERVAL_SIGNS = new ArrayList<String>();
	static {
		ORG_INTERVAL_SIGNS.add("-");
		ORG_INTERVAL_SIGNS.add("~");

		// wait to add.
	}

	/**
	 * 将行文本中所有的区间符号(-~)等等替换为"至".
	 * 
	 * @param line
	 * @return
	 */
	private String cleanIntervalSigns(String line) {
		if (null == line || line.isEmpty()) {
			return new String();
		}

		for (String sign : ORG_INTERVAL_SIGNS) {
			line = line.replaceAll(sign, "至");
		}

		return line;
	}

	public static void main(String[] args) {
		
	}

}
