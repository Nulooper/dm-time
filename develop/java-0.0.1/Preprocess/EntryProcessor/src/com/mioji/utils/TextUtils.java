package com.mioji.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.mioji.preprocess.TimeSubExtractor;

public class TextUtils {

	@SuppressWarnings("unused")
	private static final String TIME_REGEX = "[0-9]{1,2}[.:][0-9]{1,2}";

	private static final Map<String, String> replaceMap = new HashMap<String, String>();
	static {
		replaceMap.put("周1:", "周1冒号");
		replaceMap.put("周2:", "周2冒号");
		replaceMap.put("周3:", "周3冒号");
		replaceMap.put("周4:", "周4冒号");
		replaceMap.put("周5:", "周5冒号");
		replaceMap.put("周6:", "周6冒号");
		replaceMap.put("周7:", "周7冒号");
		replaceMap.put("月:", "月冒号");
		replaceMap.put("日:", "日冒号");
		replaceMap.put("天:", "天冒号");
		replaceMap.put("季:", "季冒号");
		replaceMap.put("天:", "天冒号");
	}

	/**
	 * 使用split 间隔给定列表元素, 返回相应的字符串. 如果split为null则默认为空格.
	 * 
	 * @param list
	 * @param split
	 * @return
	 */
	public static String join(List<String> list, String split) {
		if (null == list || list.isEmpty()) {
			return "";
		}

		if (null == split || split.isEmpty()) {
			split = " ";
		}

		StringBuilder sb = new StringBuilder();
		int size = list.size();
		int i = 0;
		for (; i < size - 1; ++i) {
			sb.append(list.get(i)).append(split);
		}
		sb.append(list.get(i));

		return sb.toString();
	}

	/**
	 * 统计字符在字符串str中出现的次数.
	 * 
	 * 
	 * @param str
	 * @param subStr
	 * @return
	 */
	public static int count(String str, char s) {
		if (null == str || str.isEmpty()) {
			return 0;
		}

		char[] cArray = str.toCharArray();
		int sum = 0;
		for (char c : cArray) {
			if (c == s) {
				++sum;
			}
		}

		return sum;
	}

	/**
	 * 将字符串中的冒号(除了时间中的冒号之外)，替换为特殊中文字符.
	 * 方法:抽取给定字符串中的时间字符串，然后对剩下的字符串中的冒号符号使用中文"冒号"代替.
	 * 
	 * @param line
	 * @return
	 */
	public static String replaceWith(String line) {
		if (null == line || line.isEmpty()) {
			return "";
		}

		List<String> timeSubStrList = TimeSubExtractor.extractByGroup(line,
				"周[0-7]:");
		if (!timeSubStrList.isEmpty()) {
			System.out.println(line);
			System.out.println(join(timeSubStrList, ","));
		}
		return null;
	}

	public static void main(String[] args) throws IOException {
		List<String> lines = FileUtils.readLines(new File(
				"data/week_open_close.txt"));
		List<String> fuzzyLines = new ArrayList<String>();
		for (String line : lines) {
			for (String key : replaceMap.keySet()) {
				line = line.replaceAll(key, replaceMap.get(key));
			}
			fuzzyLines.add(line);
		}
		FileUtils.writeLines(new File("fuzzy.txt"), fuzzyLines);
	}
}
