package com.mioji.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import com.mioji.preprocess.TimeSubExtractor;

public class Tmp {

	public static final List<String> ALWAYS_OPEN = new ArrayList<String>();
	public static final List<String> ALWAYS_CLOSE = new ArrayList<String>();
	static {
		try {
			List<String> lines1 = FileUtils.readLines(new File(
					"pattern_always_open.dict"));
			List<String> lines2 = FileUtils.readLines(new File(
					"pattern_always_close.dict"));
			ALWAYS_OPEN.addAll(lines1);
			ALWAYS_CLOSE.addAll(lines2);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static final Comparator<String> DESC = new Comparator<String>() {

		@Override
		public int compare(String o1, String o2) {
			return o2.length() - o1.length();
		}

	};

	public static void extractWeekCases() throws IOException {
		List<String> lines = FileUtils.readLines(new File(
				"data/cleaned_open_close.txt"));
		List<String> weekLines = new ArrayList<String>();
		List<String> monthLines = new ArrayList<String>();
		List<String> alwaysOpenLines = new ArrayList<String>();
		List<String> alwaysCloseLines = new ArrayList<String>();
		List<String> seasonLines = new ArrayList<String>();

		List<String> apmLines = new ArrayList<String>();

		List<String> leftLines = new ArrayList<String>();
		for (String line : lines) {
			if (line.contains("周")) {
				weekLines.add(line);
			} else if (line.contains("月")) {
				monthLines.add(line);
			} else if (Collections.frequency(ALWAYS_OPEN, line) != 0) {
				alwaysOpenLines.add(line);
			} else if (Collections.frequency(ALWAYS_CLOSE, line) != 0) {
				alwaysCloseLines.add(line);
			} else if (line.contains("季")) {
				seasonLines.add(line);
			} else if (line.contains("p.m.") || line.contains("pm.")
					|| line.contains("pm") || line.contains("a.m.")
					|| line.contains("am.") || line.contains("am")) {
				apmLines.add(line);
			}

			else {
				leftLines.add(line);
			}
		}
		Collections.sort(weekLines, DESC);
		Collections.sort(monthLines, DESC);
		Collections.sort(alwaysOpenLines, DESC);
		Collections.sort(alwaysCloseLines, DESC);
		Collections.sort(seasonLines, DESC);

		Collections.sort(apmLines, DESC);

		Collections.sort(leftLines, DESC);

		FileUtils.writeLines(new File("data/week_open_close.txt"), weekLines);
		FileUtils.writeLines(new File("data/month_open_close.txt"), monthLines);
		FileUtils.writeLines(new File("data/always_open_open_close.txt"),
				alwaysOpenLines);
		FileUtils.writeLines(new File("data/always_close_open_close.txt"),
				alwaysCloseLines);
		FileUtils.writeLines(new File("data/season_open_close.txt"),
				seasonLines);

		FileUtils.writeLines(new File("data/apm_open_close.txt"), apmLines);

		FileUtils.writeLines(new File("data/left_open_close.txt"), leftLines);
	}

	public static void filterCasesSet() throws IOException {
		String ao = "data/always_open_open_close.txt";
		String ac = "data/always_close_open_close.txt";
		String mo = "data/month_open_close.txt";
		String so = "data/season_open_close.txt";
		String wo = "data/week_open_close.txt";
		String lo = "data/left_open_close.txt";

		writeSetToFileByLine(ao, "data/ao.txt");
		writeSetToFileByLine(ac, "data/ac.txt");
		writeSetToFileByLine(mo, "data/mo.txt");
		writeSetToFileByLine(so, "data/so.txt");
		writeSetToFileByLine(wo, "data/wo.txt");
		writeSetToFileByLine(lo, "data/lo.txt");
	}

	public static void writeSetToFileByLine(String srcPath, String tarPath)
			throws IOException {
		if (null == srcPath || null == tarPath || srcPath.isEmpty()
				|| tarPath.isEmpty()) {
			return;
		}
		Set<String> lineSet = new HashSet<String>(FileUtils.readLines(new File(
				srcPath)));
		List<String> lineList = new ArrayList<String>(lineSet);
		Collections.sort(lineList);
		FileUtils.writeLines(new File(tarPath), lineList);
	}

	public static void main(String[] args) throws IOException {
		extractWeekCases();
		filterCasesSet();
		// createEntryHitDefine("PAT.define");
		// setVariableID("entry.define");
	}

	/**
	 * 生成HIT声明表达式.
	 * 
	 * @param srcPath
	 * @throws IOException
	 */
	public static void createEntryHitDefine(String srcPath) throws IOException {

		System.out.println("INPUT:[" + srcPath + "]");

		List<String> lines = FileUtils.readLines(new File(srcPath));
		List<Integer> pidList = new ArrayList<Integer>();
		StringBuilder sb = new StringBuilder();
		sb.append("ENT:(");
		int size = lines.size();
		int i = 0;
		for (; i < size - 1; ++i) {
			String line = lines.get(i);
			if (line.startsWith("GID:")) {
				line = line.substring(line.indexOf(":") + 1);
				pidList.add(Integer.parseInt(line));
				sb.append("HIT(").append(line).append(") ").append("or ");
				sb.append("HIT(").append(line).append(") ").append("or ");
				sb.append("HIT(").append(line).append(") ").append("or ");
				sb.append("HIT(").append(line).append(") ").append("or ");
			}
		}
		String line = lines.get(i);
		if (line.startsWith("GID:")) {
			line = line.substring(line.indexOf(":") + 1);
			sb.append("HIT(").append(line).append(") ").append("or ");
			sb.append("HIT(").append(line).append(") ").append("or ");
			sb.append("HIT(").append(line).append(") ").append("or ");
			sb.append("HIT(").append(line).append(") ");
		} else {
			sb.replace(sb.length() - 3, sb.length(), "");
			sb.append(")");
		}

		System.out.println(sb.toString());

		// 打印pid
		// Collections.sort(pidList);
		// for (int id : pidList) {
		// System.out.println(id);
		// }
		FileUtils.write(new File("entry_hit.define"), sb.toString());

		List<String> pids = TimeSubExtractor.extractByGroup(sb.toString(),
				"\\d{5}");
		System.out.println("pid size:[" + pids.size() + "]");
		if (pids.size() % 4 == 0) {
			System.out.println("生成HIT成功! 验证通过!");
		} else {
			System.out.println("生成HIT失败! 验证失败!");
		}

		System.out.println("OUTPUT:[" + "entry_hit.define" + "]");
	}

	/**
	 * 重新设置entry中变量名，设置对应的递增id.
	 * 
	 * @throws IOException
	 */
	public static void setVariableID(String entryPath) throws IOException {
		if (null == entryPath || entryPath.isEmpty()) {
			return;
		}

		System.out.println("input entry: [" + entryPath + "]");

		List<String> lines = FileUtils.readLines(new File(entryPath));
		List<String> checkedLines = new ArrayList<String>();
		String idSubStr = "(S::VAR_NOR::";
		for (String line : lines) {
			if (!line.contains("time_span")) {
				checkedLines.add(line);
				continue;
			}

			// 获取time_span id.
			String testCase = line;
			int index = testCase.indexOf(idSubStr);
			testCase = testCase.substring(index + idSubStr.length());
			index = testCase.indexOf("::");
			String timeSpanId = testCase.substring(0, index);

			int startIndex = line.indexOf("time_span");
			String left = line.substring(0, startIndex);
			int length = line.length();
			for (int i = startIndex; i < length; ++i) {
				if ("=".equals(line.subSequence(i, i + 1))) {
					// System.out.println(line.substring(startIndex, i) + "->"
					// + "time_span" + (k++));
					// System.out.println("org:\t" + line);

					line = left + "time_span" + "_" + timeSpanId
							+ line.substring(i);

					// System.out.println("tar:\t" + line);

					checkedLines.add(line);
					break;
				}
			}
		}

		FileUtils.writeLines(new File("tar_entry.define"), checkedLines);
		System.out.println("output entry: [" + "tar_entry.define" + "]");
	}
}
