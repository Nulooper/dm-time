package com.mioji.test;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

public class Starter {

	/**
	 * 读取打标程序打标后的entry文件.
	 * 
	 * @param srcFile
	 *            entry文件.
	 * @return
	 * @throws IOException
	 */
	public static List<String> readEntry(String srcFile) throws IOException {
		if (null == srcFile || srcFile.isEmpty()) {
			return Collections.emptyList();
		}
		List<String> entryList = FileUtils.readLines(new File(srcFile));

		return Collections.unmodifiableList(entryList);
	}

	public static void sortByDict(String srcFile, String tarFile)
			throws IOException {
		List<String> lines = FileUtils.readLines(new File(srcFile));
		Collections.sort(lines);
		FileUtils.writeLines(new File(tarFile), lines);
	}

	public static String normalizeTimeSubStr1(String inputTime) {
		if (null == inputTime || inputTime.isEmpty()) {
			return "";
		}
		System.out.println("ORG: inputTime[" + inputTime + "]");
		Pattern subTimePattern = Pattern.compile("周\\d\\d[:]\\d\\d");
		Matcher matcher = subTimePattern.matcher(inputTime);

		Pattern digitPattern = Pattern.compile("\\d");
		int matchCount = 0;
		while (matcher.find()) {
			int start = matcher.start() + matchCount;
			int end = matcher.end() + matchCount;
			String subStr = inputTime.substring(start, end);
			Matcher digitMatcher = digitPattern.matcher(subStr);
			while (digitMatcher.find()) {
				int digitStart = digitMatcher.start();
				int digitEnd = digitMatcher.end();
				String digitStr = subStr.substring(digitStart, digitEnd);
				subStr = subStr.replaceFirst(digitStr, digitStr + "0");
				++matchCount;
				break;
			}
			inputTime = inputTime.replaceFirst(inputTime.substring(start, end),
					subStr);
		}
		System.out.println("END: inputTime[" + inputTime + "]");

		return inputTime;
	}

	/**
	 * 将“图书馆:周1至周51​​0:00至15:00”规范化为形式"图书馆:周1至周5冒号1​​0:00至15:00"
	 * 
	 * @param inputTime
	 * @return
	 */
	public static String normalizeTimeSubStr2(String inputTime) {
		if (null == inputTime || inputTime.isEmpty()) {
			return "";
		}
		// System.out.println("ORG: inputTime[" + inputTime + "]");
		Pattern subTimePattern = Pattern.compile("周\\d{3}:\\d{2}");
		Matcher matcher = subTimePattern.matcher(inputTime);

		Pattern digitPattern = Pattern.compile("\\d");
		int matchCount = 0;
		while (matcher.find()) {
			int start = matcher.start() + 2 * matchCount;
			int end = matcher.end() + 2 * matchCount;
			String subStr = inputTime.substring(start, end);
			// System.out.println("subStr[" + subStr + "]");
			Matcher digitMatcher = digitPattern.matcher(subStr);
			while (digitMatcher.find()) {
				int digitStart = digitMatcher.start();
				int digitEnd = digitMatcher.end();
				String digitStr = subStr.substring(digitStart, digitEnd);
				subStr = subStr.replaceFirst(digitStr, digitStr + "冒号");
				// System.out.println("subStr[" + subStr + "]");
				++matchCount;
				break;
			}
			inputTime = inputTime.replaceFirst(inputTime.substring(start, end),
					subStr);
		}
		// System.out.println("END: inputTime[" + inputTime + "]");

		return inputTime;
	}

	/**
	 * 将形如10.25至16.35规范化为10:25至16:35
	 * 
	 * @param inputTime
	 * @return
	 */
	public static String normalizeTimeSubStr3(String inputTime) {
		if (null == inputTime || inputTime.isEmpty()) {
			return "";
		}

		Pattern subTimePattern = Pattern
				.compile("\\d{1,2}[.]\\d{2}至\\d{1,2}[.]\\d{2}");
		Matcher matcher = subTimePattern.matcher(inputTime);

		while (matcher.find()) {
			int start = matcher.start();
			int end = matcher.end();
			String subStr = inputTime.substring(start, end);

			String tarStr = subStr.replaceAll("\\.", ":");
			System.out
					.println("subStr[" + subStr + "], tarStr[" + tarStr + "]");
			inputTime = inputTime.substring(0, start) + tarStr
					+ inputTime.substring(end);
			System.out.println("inputTime[" + inputTime + "]");
		}

		return inputTime;
	}

	/**
	 * 将形如"09:30至17:3012月24至26日"规范化为"09:30至17:3012月24日至12月26日"
	 * 
	 * @param inputTime
	 * @return
	 */
	public static String normalizeTimeSubStr4(String inputTime) {
		if (null == inputTime || inputTime.isEmpty()) {
			return "";
		}
		// System.out.println("ORG: inputTime[" + inputTime + "]");
		Pattern subTimePattern = Pattern.compile("\\d{2}:\\d{2}\\d{1,2}月");
		Matcher matcher = subTimePattern.matcher(inputTime);

		int matchCount = 0;
		while (matcher.find()) {
			int start = matcher.start() + 2 * matchCount;
			int end = matcher.end() + 2 * matchCount;
			String subStr = inputTime.substring(start, end);
			subStr = subStr.substring(0, 5) + "逗号" + subStr.substring(5);
			++matchCount;
			inputTime = inputTime.replaceFirst(inputTime.substring(start, end),
					subStr);
		}

		return inputTime;

	}

	public static void main(String[] args) throws IOException {
		// sortByDict("data/week_open_close.txt",
		// "data/week_dict_open_close.txt");
		String rawTimeText = "每日冒号10:00至15:308月18日冒号9:00至20:00";
		System.out.println(normalizeTimeSubStr4(rawTimeText));
	}
}
