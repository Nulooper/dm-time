/////////////////////////////////////////////////////////////
/*Filename   : IR.h                                        */
/*Create Date: 2014-07-04                                  */
/*Author     : Wang Zhiwei 王志伟                           */
/*Version	 : 0.0.1                                       */

/*changed	 : 2014-08-16 by Wang Zhiwei 王志伟             */
/*reversion  : 0.0.2                                       */
/////////////////////////////////////////////////////////////

#ifndef _IR_H_
#define _IR_H_

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

using namespace std;

const static std::string RETURN_CLOSE = "";//如果关闭，则返回空串.
const static std::string RETURN_UNKNOWN = "08:00-18:00";
const static std::string RETURN_OPEN = "00:00-24:00";

const static std::string RULE_DELIM = "|";

const static std::string TYPE_WEEK_TIME = "WEEK_TIME_RULE";
const static std::string TYPE_ONLY_TIME = "TIME_RULE";
const static std::string TYPE_MONTH_TIME = "MONTH_TIME_RULE";

const int mon_day[]={31,28,31,30,31,30,31,31,30,31,30,31};

class IR
{
public:
    //static bool search(const std::string& open_rule, const std::string& close_rule, const std::string& in_date, std::string& out_times);
    static bool search(const std::string& rule, const std::string& in_date, std::string& out_times);
    
public:
    
    static void vector2String(const std::vector<std::string>& time_pair_vec, const std::string& delim, std::string& out_str);
    static void split(const std::string& in_str, const std::string& delim, std::vector<std::string>& out_vec);
    static std::string int2String(int in_number);
    static int string2int(const std::string& in_str);
    static int getWDay(const time_t& t, int zone = 8);
    static time_t toTime(const std::string& s, int zone = 8);
    static int compareDayStr(const std::string& a,const std::string& b);
    
    static bool checkDayOfWeek(const std::string& in_day_of_week, const std::string& close_type, const std::string& open_week_time_rule,
                               const std::string& close_week_time_rule, std::string& out_times);
    static std::string getDayOfWeek(const std::string& in_date);
    static bool getDayOfWeekTimesMap(const std::string& week_time_rule, std::map<std::string, std::vector<std::string> >& out_map);
    static void spitWeekTimeRule(const std::string& week_time_rule, std::vector<std::string>& out_vec);
    
    static bool checkTime(const std::string& day_of_week, const std::string& close_type, const std::string& open_text, const std::string& close_text,
                          std::string& out_times);
    static void splitTimeRule(const std::string& time_rule, std::vector<std::string>& out_vec);
    
    static bool checkMonthTime(int month_of_given_day, const std::string& close_type, const std::string& open_text, const std::string& close_text,
                               std::string& out_times);
    static int getMonthOfDay(const std::string& in_date);
    static void getMonthTimeMap(const std::string& month_time_rule, std::map<int, std::string>& out_map);
    
};

bool IR::search(const std::string& rule, const std::string& in_date, std::string& out_times)
{
	//
	//std::cerr << "rule:[" << rule << "] in_date:[" << in_date << "] out_times:[" << out_times << "]" << std::endl;
	std::vector<std::string> rule_items;
    split(rule, RULE_DELIM, rule_items);
	if(rule_items.empty())
	{
        //cerr << "macth empty" << endl;
		out_times = RETURN_UNKNOWN;
		return false;
	}
    
	std::string open_rule = rule_items[0];
	std::vector<std::string> open_items;
    split(open_rule, "&", open_items);
	std::string open_text = open_items[0];
	std::string open_type = open_items[1];
    
    std::string close_rule = rule_items[1];
    std::vector<std::string> close_items;
    split(close_rule, "&", close_items);
    std::string close_text = close_items[0];
    std::string close_type = close_items[1];
    
    //std::cerr << "open_text[" << open_text << "] open_type[" << open_type << "] close_text["
    //<< close_text << "] close_type["<< close_type << "]" << std::endl;
    
    if("ALWAYS" == open_rule && "NO" == close_rule)
    {
    	out_times = RETURN_OPEN;
    	//std::cerr << "match ALWAYS OPEN" << std::endl;
    	return true;
    }
    else if("NO" == open_rule && "ALWAYS" == close_rule)
    {
    	out_times = RETURN_CLOSE;
    	//std::cerr << "match ALWAYS CLOSE" << std::endl;
    	return true;
    }
    else if("UNKNOWN" == open_rule && "UNKNOWN" == close_rule)
    {
        //std::cerr << "macth UNKNOWN" << std::endl;
    	out_times = RETURN_UNKNOWN;
    	return true;
    }
    else
    {
    	std::string day_of_week = getDayOfWeek(in_date);
    	//cerr<<" "<<day_of_week<<" ";
    	int month_of_day = getMonthOfDay(in_date);
    	if(TYPE_WEEK_TIME == open_type)
    	{
            //cerr << "macth week_time" << endl;
            //std::cerr << "in_date:[" << in_date << "] day_of_week:[" << day_of_week << "] month_of_day:[" << month_of_day << "]" << std::endl;
    		return checkDayOfWeek(day_of_week, close_type, open_text, close_text, out_times);
    	}
    	else if(TYPE_ONLY_TIME == open_type)
    	{
            //cerr << "macth only_time" << endl;
    		return checkTime(day_of_week, close_type, open_text, close_text, out_times);
    	}
    	else if(TYPE_MONTH_TIME == open_type)
    	{
            //cerr << "match month_time" << endl;
    		return checkMonthTime(month_of_day, close_type, open_text, close_text, out_times);
    	}
    	else
    	{
            //cerr << "match nothing" << endl;
    		out_times = RETURN_UNKNOWN;
    		return false;
    	}
    }
    //cerr << "macth nothing !" << endl;
    out_times = RETURN_UNKNOWN;
    
    return false;
}

void IR::vector2String(const std::vector<std::string>& time_pair_vec, const std::string& split, std::string& out_str)
{
	std::vector<std::string>::size_type size = time_pair_vec.size();
    
    if(0 == size)
    {
    	out_str = "";
    	return;
    }
    
    if(1 == size){
        out_str = time_pair_vec[0];
        return;
    }
    
    out_str = "";
    for(std::vector<std::string>::size_type i = 0; i < size - 1; ++i)
    {
        out_str += time_pair_vec[i] + split;
    }
    
    out_str += time_pair_vec[size - 1];
}

void IR::split(const std::string& in_str, const std::string& delim, std::vector<std::string>& out_vec)
{
	out_vec.clear();
    
    std::string::size_type pos = 0;
    std::string::size_type len = in_str.length();
    std::string::size_type delim_len = delim.length();
    if (0 == len || 0 == delim_len)
    {
    	return;
    }
    while (pos < len)
    {
        std::string::size_type find_pos = in_str.find(delim, pos);
        if (find_pos == -1)
        {
            out_vec.push_back(in_str.substr(pos, len - pos));
            break;
        }
        out_vec.push_back(in_str.substr(pos, find_pos - pos));
        pos = find_pos + delim_len;
    }
}

std::string IR::int2String(int in_number)
{
    std::stringstream ss;
	ss << in_number;
    std::string number_str;
	ss >> number_str;
    
	return number_str;
}

int IR::string2int(const std::string& in_str)
{
    std::stringstream ss;
	ss << in_str;
	int number;
	ss >> number;
    
	return number;
}

time_t IR::toTime(const std::string& s, int zone)
{
	const char* s_c = s.c_str();
	char* pstr;
	long year, month, day, hour, min, sec;
	year = strtol(s_c, &pstr, 10);
	month = (year / 100) % 100;
	day = (year % 100);
	year = year / 10000;
	hour = min = sec = 0;
	if(*pstr)
	{
		hour = strtol(++pstr, &pstr, 10);
		if(*pstr)
		{
			min = strtol(++pstr, &pstr, 10);
			if (*pstr)
			{
				sec = strtol(++pstr, &pstr, 10);
			}
		}
	}
	//printf("%d %d %d\n", hour,min,sec);

    int leap_year = (year - 1969) / 4;	//year-1-1968
    int i = (year - 2001) / 100;		//year-1-2000
    leap_year -= ((i / 4) * 3 + i % 4);
    int day_offset = 0;
    for (i = 0; i < month - 1; i++)
    	day_offset += mon_day[i];
    bool isLeap = ((year % 4 == 0 && year % 100 != 0)||(year % 400 == 0));
    if (isLeap && month>2)
    	day_offset += 1;
    day_offset += (day-1);
    day_offset += ((year - 1970) * 365 + leap_year);
    int hour_offset = hour - zone;
    time_t ret = day_offset * 86400 + hour_offset * 3600 + min * 60 + sec;

    return ret;
}

/**
* 比较两个给定日期. 相等返回0,大于返回正值表示天数,小于返回负值表示天数. 
*
* a: 0814(月日)
* b: 0814(月日)
*/
int IR::compareDayStr(const std::string& a, const std::string& b)
{
	if (a == b)
		return 0;

	time_t a_t = toTime("2014" + a + "_00:00", 0);
	time_t b_t = toTime("2014" + b + "_00:00", 0);

	return (b_t - a_t) / 86400;
}

int IR::getWDay(const time_t& t, int zone)
{
	struct tm TM;
	time_t nt = t + zone * 3600;
	gmtime_r(&nt, &TM);
	int ret = (TM.tm_wday + 6) % 7 + 1;
	return ret;
}



///////////////////////////////////
//week time rule.

bool IR::checkDayOfWeek(const std::string& in_day_of_week, const std::string& close_type, const std::string& open_week_time_rule,
                        const std::string& close_week_time_rule, std::string& out_times)
{
	//std::cerr << "in_day_of_week:[" << in_day_of_week << "]" << std::endl;
	//wait to implement.
	if(TYPE_WEEK_TIME == close_type)
	{
		std::map<std::string, std::vector<std::string> > close_map;
		getDayOfWeekTimesMap(close_week_time_rule, close_map);
		if(close_map.find(in_day_of_week) != close_map.end())
		{
            out_times = RETURN_CLOSE;
            return true;
		}
        
	}
	std::map<std::string, std::vector<std::string> > open_map;
	getDayOfWeekTimesMap(open_week_time_rule, open_map);
	if(open_map.find(in_day_of_week) == open_map.end())
	{
		out_times = RETURN_CLOSE;
		return true;
	}
    
	vector2String(open_map[in_day_of_week], ",", out_times);
	return true;
}

std::string IR::getDayOfWeek(const std::string& in_date)
{
	
	time_t tt = IR::toTime(in_date+"_00:00");
    int wd = IR::getWDay(tt);

	return "周" + int2String(wd);
	//return "周1"; 
}


bool IR::getDayOfWeekTimesMap(const std::string& week_time_rule, std::map<std::string, std::vector<std::string> >& out_map)
{
	//wait to implement.
	std::vector<std::string> week_time_vec;
	spitWeekTimeRule(week_time_rule, week_time_vec);
    
	out_map.clear();
	for(std::vector<std::string>::iterator it = week_time_vec.begin(); it != week_time_vec.end(); ++it)
	{
		std::string week_time = *it;
                //std::cerr << "week_time size:[" << week_time.size() << "]["<< week_time << "]" << std::endl;
		int index1 = week_time.find("-");
		int index2 = week_time.find(":");
		std::string start_day = week_time.substr(0, index1);
		std::string end_day = week_time.substr(index1 + 1, index2 - index1 - 1);
        
		//std::cerr << "start_day[" << start_day << "] end_day[" << end_day << "]" << std::endl;
        
		std::string time_info = week_time.substr(index2 +1 , week_time.size() - index2 - 1);
		//std::cerr << "time_info[" << time_info << "]" << std::endl;
		std::vector<std::string> time_pair_vec;
		split(time_info, ",", time_pair_vec);
		if(start_day == end_day)
		{
			out_map[start_day] = time_pair_vec;
			//cerr << "start_day[" << start_day << "]" << endl;
		}
		else
		{
			int start = string2int(start_day.substr(3, 1));
			int end = string2int(end_day.substr(3, 1));
                        //std::cerr << start_day.substr(3) << "-" << end_day.substr(3) << std::endl;
                       //std::cerr << "start[" << start << "] end[" << end << "]" << std::endl;
			if(start < end)
			{
				for(int i = start; i <= end; ++i)
				{
					out_map["周" + int2String(i)] = time_pair_vec;
				}
			}
			else
			{
				for(int j = start; j <= 7; ++j)
				{
					out_map["周" + int2String(j)] = time_pair_vec;
				}
				for(int m = 1; m <= end; ++m)
				{
					out_map["周" + int2String(m)] = time_pair_vec;
				}
			}
		}
	}
	return true;
}

void IR::spitWeekTimeRule(const std::string& week_time_rule, std::vector<std::string>& out_vec)
{
       // std::cerr << "in spitWeekTimeRule week_time_rule[" << week_time_rule << "]" << std::endl;
	std::vector<std::string> time_vec;
	split(week_time_rule, "{", time_vec);
    
	out_vec.clear();
    
	for(std::vector<std::string>::iterator iter = time_vec.begin(); iter != time_vec.end(); ++iter)
	{
        std::string time = *iter;
		if(time.empty())
		{
			continue;
		}
		out_vec.push_back(time.substr(0, time.find("}")));
	}
}

///////////////////////////////////
//time rule.

bool IR::checkTime(const std::string& day_of_week, const std::string& close_type, const std::string& open_text, const std::string& close_text,
                   std::string& out_times)
{
	std::vector<std::string> time_vec;
	splitTimeRule(open_text, time_vec);
	if(TYPE_WEEK_TIME == close_type)
	{
		std::map<std::string, std::vector<std::string> > close_map;
		getDayOfWeekTimesMap(close_text, close_map);
		if(close_map.find(day_of_week) != close_map.end())
		{
			out_times = RETURN_CLOSE;
			return true;
		}
		vector2String(time_vec, ",", out_times);
		return true;
	}
	else if ("NO" == close_type)
	{
		vector2String(time_vec, ",", out_times);
		return true;
	}
	else
	{
		out_times = RETURN_UNKNOWN;
		return true;
	}
}

void IR::splitTimeRule(const std::string& time_rule, std::vector<std::string>& out_vec)
{
	out_vec.clear();
    
	std::string cleaned_time_rule = time_rule.substr(1, time_rule.find("}") - 1);
	split(cleaned_time_rule, ",", out_vec);
}

/////////////////////////////////
//month time rule.

bool IR::checkMonthTime(int month_of_given_day, const std::string& close_type, const std::string& open_text, const std::string& close_text,
                        std::string& out_times)
{
	std::map<int, std::string> open_map;
	getMonthTimeMap(open_text, open_map);
    
	if("NO" == close_type)
	{
		if(open_map.find(month_of_given_day) != open_map.end())
		{
			out_times = open_map[month_of_given_day];
			return true;
		}
		out_times = RETURN_CLOSE;
		return true;
	}
    
	if(open_map.find(month_of_given_day) != open_map.end())
	{
		out_times = open_map[month_of_given_day];
		return true;
	}
	out_times = RETURN_CLOSE;
	return true;
}

int IR::getMonthOfDay(const std::string& in_date)
{
	std::string month = in_date.substr(4, 2);
    
	return string2int(month);
}

void IR::getMonthTimeMap(const std::string& month_time_rule, std::map<int, std::string>& out_map)
{
	int index1 = month_time_rule.find("月");
	int index2 = month_time_rule.find("-");
	int index3 = month_time_rule.find(":");
	std::string start_month = month_time_rule.substr(1, index1 - 1);
	std::string end_month = month_time_rule.substr(index2 + 1, month_time_rule.rfind("月") - index2 - 1);
	int start = string2int(start_month);
	int end = string2int(end_month);
	std::string time_pair = month_time_rule.substr(index3 + 1, month_time_rule.rfind("}") - index3 - 1);
    
	out_map.clear();
    
	if(start <= end)
	{
		for(int i = start; i <= end; ++i)
		{
			out_map[i] = time_pair;
		}
	}
	else
	{
		for(int j = start; j <= 12; ++j)
		{
			out_map[j] = time_pair;
		}
		for(int m = 1; m <= end; ++m)
		{
			out_map[m] = time_pair;
		}
	}
}

#endif

