package com.mioji.parse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 执行一系列时间识别. 根据配置文件，按照优先级，依次读取parser，最终返回预定义格式的时间规则.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-8 下午8:48:46
 * 
 */
public final class Pipeline {
	private int matchedCount = 0;
	private int totalCount = 0;
	private Map<String, Integer> patternMatchMap = new HashMap<String, Integer>();
	private List<String> unrecognizedList = new ArrayList<String>();

	private List<Parsable> parsers;

	public Pipeline() {
		this.parsers = ParserFactory.createParsres();
	}

	public String doParse(String rawText) {
		if (null == rawText || rawText.isEmpty()) {
			Logger.getGlobal().logp(Level.WARNING, Pipeline.class.getName(),
					"doParse", "输入解析文本不合法！解析失败！返回空字符川！");
			return "";
		}

		SpotTimeText stt = SpotTimeText.createInstance(rawText);
		Iterator<Parsable> iter = parsers.iterator();
		Parsable parser = null;
		++totalCount;
		while (iter.hasNext()) {
			parser = iter.next();

			if (parser.isMatched(stt)) {
				/*
				 * System.out.println(stt.getTimeText() + "\t匹配: " +
				 * parser.getClass().getName());
				 */
				++matchedCount;
				this.updateMatchMap(parser);
				String ruleInfo = parser.parse(stt);
				if (parser.getClass().getName()
						.equals("com.mioji.parse.WeekTimeParser")) {
					System.out.println(stt.getTimeText());
					System.out.println(parser.getClass().getName());
					System.out.println(ruleInfo);
					System.out.println();
				}
				// System.out.println(ruleInfo);
				// System.out.println();

				return ruleInfo;
			}

		}
		unrecognizedList.add(stt.getTimeText());

		return "";
	}

	private void updateMatchMap(Parsable parser) {
		String classname = parser.getClass().getName();
		if (this.patternMatchMap.containsKey(classname)) {
			this.patternMatchMap.put(classname,
					this.patternMatchMap.get(classname) + 1);
		} else {
			this.patternMatchMap.put(classname, 1);
		}
	}

	public int getMatchedCount() {
		return this.matchedCount;
	}

	public int getTotalCount() {
		return this.totalCount;
	}

	public double getMacthedRate() {
		return this.matchedCount * 1.0 / this.totalCount;
	}

	public Map<String, Integer> getPatternMatchMap() {
		return Collections.unmodifiableMap(this.patternMatchMap);
	}

	public List<String> getUnrecognizedList() {
		Collections.sort(this.unrecognizedList);
		return Collections.unmodifiableList(this.unrecognizedList);
	}

	public static void main(String[] agrs) {
		Logger.getGlobal().setLevel(Level.INFO);

		Pipeline p = new Pipeline();
		System.out.println(p
				.doParse("\"123\",\"12$\",\"周1至周510:00-17:00,周610:00-14:00\""));
		System.out.println(p.getMacthedRate());
	}
}
