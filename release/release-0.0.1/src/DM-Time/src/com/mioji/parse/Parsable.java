package com.mioji.parse;

/**
 * 定义了，所有parser的接口.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @date 2014/06/08.
 * 
 */
public interface Parsable {
	/**
	 * step1: 检测输入是否匹配当前parser可以解析的模式，如果匹配就进行解析，如果不匹配将当前文本传给下一个parser.
	 * 
	 * @param stt
	 * @return
	 */
	public boolean isMatched(SpotTimeText stt);

	/**
	 * step2: 仅当第一满足之后才执行. 解析给定的景点开关时间信息本文，返回时间规则对象.
	 * 
	 * @param stt
	 * @return
	 */
	public String parse(SpotTimeText stt);
}
