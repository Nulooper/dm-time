package com.mioji.parse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.mioji.ds.DayTimeRule;
import com.mioji.ds.MonthTimeRule;
import com.mioji.ds.QuarterTimeRule;
import com.mioji.ds.WeekTimeRule;

/**
 * 创建空列表.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年6月13日 上午10:07:06
 * 
 */
public class EmptyListCreator {
	public static List<DayTimeRule> getEmptyDTR() {
		List<DayTimeRule> dtrList = new ArrayList<DayTimeRule>();
		return Collections.unmodifiableList(dtrList);
	}

	public static List<WeekTimeRule> getEmptyWTR() {
		List<WeekTimeRule> dtrList = new ArrayList<WeekTimeRule>();
		return Collections.unmodifiableList(dtrList);
	}

	public static List<MonthTimeRule> getEmptyMTR() {
		List<MonthTimeRule> dtrList = new ArrayList<MonthTimeRule>();
		return Collections.unmodifiableList(dtrList);
	}

	public static List<QuarterTimeRule> getEmptyQTR() {
		List<QuarterTimeRule> dtrList = new ArrayList<QuarterTimeRule>();
		return Collections.unmodifiableList(dtrList);
	}
}
