package com.mioji.parse;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.mioji.ds.MiojiPair;
import com.mioji.utils.StringHelper;

/**
 * 定义了景点的时间信息文本，包含三个字段景点id，价格，关于开关时间描述的文本.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-8 下午8:35:50
 * 
 */
public class SpotTimeText {

	private static List<MiojiPair> replacePairList = new ArrayList<MiojiPair>();
	static {
		replacePairList.add(MiojiPair.createInstance("1点", "01:00"));
		replacePairList.add(MiojiPair.createInstance("2点", "02:00"));
		replacePairList.add(MiojiPair.createInstance("3点", "03:00"));
		replacePairList.add(MiojiPair.createInstance("4点", "04:00"));
		replacePairList.add(MiojiPair.createInstance("5点", "05:00"));
		replacePairList.add(MiojiPair.createInstance("6点", "06:00"));
		replacePairList.add(MiojiPair.createInstance("7点", "07:00"));
		replacePairList.add(MiojiPair.createInstance("8点", "08:00"));
		replacePairList.add(MiojiPair.createInstance("9点", "09:00"));
		replacePairList.add(MiojiPair.createInstance("10点", "10:00"));
		replacePairList.add(MiojiPair.createInstance("11点", "11:00"));
		replacePairList.add(MiojiPair.createInstance("12点", "12:00"));
		replacePairList.add(MiojiPair.createInstance("13点", "13:00"));
		replacePairList.add(MiojiPair.createInstance("14点", "14:00"));
		replacePairList.add(MiojiPair.createInstance("15点", "15:00"));
		replacePairList.add(MiojiPair.createInstance("16点", "16:00"));
		replacePairList.add(MiojiPair.createInstance("17点", "17:00"));
		replacePairList.add(MiojiPair.createInstance("18点", "18:00"));
		replacePairList.add(MiojiPair.createInstance("19点", "19:00"));
		replacePairList.add(MiojiPair.createInstance("20点", "20:00"));
		replacePairList.add(MiojiPair.createInstance("21点", "21:00"));
		replacePairList.add(MiojiPair.createInstance("22点", "22:00"));
		replacePairList.add(MiojiPair.createInstance("23点", "23:00"));
		replacePairList.add(MiojiPair.createInstance("24点", "24:00"));

		replacePairList.add(MiojiPair.createInstance("星期天", "周7"));
		replacePairList.add(MiojiPair.createInstance("星期日", "周7"));
		replacePairList.add(MiojiPair.createInstance("礼拜天", "周7"));
		replacePairList.add(MiojiPair.createInstance("礼拜日", "周7"));

		replacePairList.add(MiojiPair.createInstance("十二月", "12月"));
		replacePairList.add(MiojiPair.createInstance("十一月", "11月"));
		replacePairList.add(MiojiPair.createInstance("十月", "10月"));
		replacePairList.add(MiojiPair.createInstance("九月", "9月"));
		replacePairList.add(MiojiPair.createInstance("八月", "8月"));
		replacePairList.add(MiojiPair.createInstance("七月", "7月"));
		replacePairList.add(MiojiPair.createInstance("六月", "6月"));
		replacePairList.add(MiojiPair.createInstance("五月", "5月"));
		replacePairList.add(MiojiPair.createInstance("四月", "4月"));
		replacePairList.add(MiojiPair.createInstance("三月", "3月"));
		replacePairList.add(MiojiPair.createInstance("二月", "2月"));
		replacePairList.add(MiojiPair.createInstance("一月", "1月"));

		replacePairList.add(MiojiPair.createInstance("月中旬", "月15日"));
		replacePairList.add(MiojiPair.createInstance("月中", "月15日"));
		replacePairList.add(MiojiPair.createInstance("月份", "月"));
		replacePairList.add(MiojiPair.createInstance("月初", "1日"));

		replacePairList.add(MiojiPair.createInstance("星期一", "周1"));
		replacePairList.add(MiojiPair.createInstance("星期二", "周2"));
		replacePairList.add(MiojiPair.createInstance("星期三", "周3"));
		replacePairList.add(MiojiPair.createInstance("星期四", "周4"));
		replacePairList.add(MiojiPair.createInstance("星期五", "周5"));
		replacePairList.add(MiojiPair.createInstance("星期六", "周6"));

		replacePairList.add(MiojiPair.createInstance("礼拜一", "周1"));
		replacePairList.add(MiojiPair.createInstance("礼拜二", "周2"));
		replacePairList.add(MiojiPair.createInstance("礼拜三", "周3"));
		replacePairList.add(MiojiPair.createInstance("礼拜四", "周4"));
		replacePairList.add(MiojiPair.createInstance("礼拜五", "周5"));
		replacePairList.add(MiojiPair.createInstance("礼拜六", "周6"));

		replacePairList.add(MiojiPair.createInstance("周一", "周1"));
		replacePairList.add(MiojiPair.createInstance("周二", "周2"));
		replacePairList.add(MiojiPair.createInstance("周三", "周3"));
		replacePairList.add(MiojiPair.createInstance("周四", "周4"));
		replacePairList.add(MiojiPair.createInstance("周五", "周5"));
		replacePairList.add(MiojiPair.createInstance("周六", "周6"));
		replacePairList.add(MiojiPair.createInstance("周日", "周7"));
		replacePairList.add(MiojiPair.createInstance("周末", "周7"));

		replacePairList.add(MiojiPair.createInstance("每周", "周"));

		replacePairList.add(MiojiPair.createInstance("週一", "周1"));

		replacePairList.add(MiojiPair.createInstance("，", ","));
		replacePairList.add(MiojiPair.createInstance("--", "-"));
		replacePairList.add(MiojiPair.createInstance("。", ""));
		replacePairList.add(MiojiPair.createInstance("：", ":"));
		replacePairList.add(MiojiPair.createInstance("；", ";"));
		replacePairList.add(MiojiPair.createInstance(" ", ""));

		replacePairList.add(MiojiPair.createInstance("&ndash;", "-"));
		replacePairList.add(MiojiPair.createInstance("&nbsp;", ""));
		replacePairList.add(MiojiPair.createInstance("&amp;", ""));
		replacePairList.add(MiojiPair.createInstance("(&mdash;){1,2}", "-"));
		replacePairList.add(MiojiPair.createInstance("～", "-"));
		replacePairList.add(MiojiPair.createInstance("~", "-"));
		replacePairList.add(MiojiPair.createInstance("——", "-"));
		replacePairList.add(MiojiPair.createInstance("–", "-"));
		replacePairList.add(MiojiPair.createInstance("--", "-"));

		replacePairList.add(MiojiPair.createInstance("&ldquo;", "\""));
		replacePairList.add(MiojiPair.createInstance("p.m.", "pm"));
		replacePairList.add(MiojiPair.createInstance("p.m", "pm"));
		replacePairList.add(MiojiPair.createInstance("a.m.", "am"));
		replacePairList.add(MiojiPair.createInstance("a.m", "am"));

		replacePairList.add(MiojiPair.createInstance("日落", "18:00"));
		replacePairList.add(MiojiPair.createInstance("黄昏", "18:00"));
		replacePairList.add(MiojiPair.createInstance("黎明", "04:00"));
		replacePairList.add(MiojiPair.createInstance("正午", "12:00"));
		replacePairList.add(MiojiPair.createInstance("中午", "12:00"));
		replacePairList.add(MiojiPair.createInstance("午夜", "24:00"));
		replacePairList.add(MiojiPair.createInstance("深夜", "24:00"));
		replacePairList.add(MiojiPair.createInstance("日出", "06:00"));
		replacePairList.add(MiojiPair.createInstance("凌晨", "24:00"));
		replacePairList.add(MiojiPair.createInstance("傍晚", "18:00"));
		replacePairList.add(MiojiPair.createInstance("半夜", "24:00"));

		replacePairList.add(MiojiPair.createInstance("二十四点", "24:00"));
		replacePairList.add(MiojiPair.createInstance("二十三点", "23:00"));
		replacePairList.add(MiojiPair.createInstance("二十二点", "22:00"));
		replacePairList.add(MiojiPair.createInstance("二十一点", "21:00"));
		replacePairList.add(MiojiPair.createInstance("二十点", "20:00"));
		replacePairList.add(MiojiPair.createInstance("十九点", "19:00"));
		replacePairList.add(MiojiPair.createInstance("十八点", "18:00"));
		replacePairList.add(MiojiPair.createInstance("十七点", "17:00"));
		replacePairList.add(MiojiPair.createInstance("十六点", "16:00"));
		replacePairList.add(MiojiPair.createInstance("十五点", "15:00"));
		replacePairList.add(MiojiPair.createInstance("十四点", "14:00"));
		replacePairList.add(MiojiPair.createInstance("十三点", "13:00"));
		replacePairList.add(MiojiPair.createInstance("十二点", "12:00"));
		replacePairList.add(MiojiPair.createInstance("十一点", "11:00"));
		replacePairList.add(MiojiPair.createInstance("十点", "10:00"));
		replacePairList.add(MiojiPair.createInstance("九点", "09:00"));
		replacePairList.add(MiojiPair.createInstance("八点", "08:00"));
		replacePairList.add(MiojiPair.createInstance("七点", "07:00"));
		replacePairList.add(MiojiPair.createInstance("六点", "06:00"));
		replacePairList.add(MiojiPair.createInstance("五点", "05:00"));
		replacePairList.add(MiojiPair.createInstance("四点", "04:00"));
		replacePairList.add(MiojiPair.createInstance("三点", "03:00"));
		replacePairList.add(MiojiPair.createInstance("两点", "02:00"));
		replacePairList.add(MiojiPair.createInstance("一点", "01:00"));
	}

	private String id;
	private String price;
	private String timeText;

	/**
	 * 
	 * @param rawText
	 *            原始的粗糙的CSV格式文本:id, price, timeText.
	 * @return
	 */
	public static SpotTimeText createInstance(String rawText) {
		return new SpotTimeText(rawText);
	}

	public static SpotTimeText createInstance(String id, String price,
			String timeText) {
		return new SpotTimeText(id, price, timeText);
	}

	public SpotTimeText(String id, String price, String timeText) {
		this.id = id;
		this.price = price;
		this.timeText = timeText;
	}

	public SpotTimeText(String rawText) {
		rawText = rawText.toLowerCase();
		this.init(rawText);
	}

	/**
	 * 完成CSV格式的解析，使用解析结果初始化字段.
	 * 
	 * @param rawText
	 *            原始的粗糙的CSV格式文本:id, price, timeText.
	 */
	private void init(String rawText) {
		// System.out.println("输入的原始字符串: rawText=[" + rawText + "]");
		// wait to implement.
		// step1: 切分csv格式，对id, price, timeText进行初始化.
		this.splitRawText(rawText);
		// step2: 全角转半角.
		this.timeText = StringHelper.toDBC(timeText);
		// System.out.println("删除无用字符前: timeText=[" + this.timeText + "]");
		// step3: trim
		timeText = StringUtils.trim(timeText);
		// step4: 替换掉停用字符.
		this.replaceStops();
		// System.out.println("删除无用字符后: timeText=[" + this.timeText + "]");
		// System.out.println(timeText);
		// step5: 根据正则表达式进一步清洗.
		this.timeText = cleanTime1(timeText);

		this.timeText = cleanTime2(timeText);
	}

	private String cleanTime1(String inputText) {
		Pattern timePat = Pattern
				.compile("(\\d{1,2}[:.]\\d{1,2}|\\d{1,2})[ap]m");
		Matcher matcher = timePat.matcher(inputText);
		while (matcher.find()) {
			int start = matcher.start();
			int end = matcher.end();

			String left = inputText.substring(0, start);

			String time = inputText.substring(start, end);
			time = time.replaceAll("\\.", ":");
			if (time.contains("pm")) {
				time = time.replace("pm", "");
				if (!time.contains(":")) {
					time = Integer.parseInt(time) + 12 + ":00";
				} else {
					int index = time.indexOf(":");
					time = (Integer.parseInt(time.substring(0, index)) + 12)
							+ time.substring(index);
				}
			} else {
				time = time.replace("am", "");
				if (!time.contains(":")) {
					if (time.length() == 1) {
						time = "0" + time + ":00";
					} else {
						time = time + ":00";
					}
				}
			}
			String right = inputText.substring(end);

			// System.out.println(left + time + right);
			inputText = left + time + right;
			matcher = timePat.matcher(inputText);
		}
		return inputText;
	}

	/**
	 * 清洗格式15:00-次日2:00，直接将次日去掉.
	 * 
	 * @param timeTextInfo
	 * @return
	 */
	private String cleanTime2(String timeTextInfo) {
		Pattern timePat = Pattern
				.compile("\\d{1,2}[:.]\\d{1,2}[-–—到至达和及](次日|翌|第二天|隔日)\\d{1,2}[:.]\\d{1,2}");
		Matcher matcher = timePat.matcher(timeTextInfo);
		StringBuilder sb = new StringBuilder();
		if (matcher.matches()) {
			List<String> timeList = TimeSubExtractor.extractByGroup(
					timeTextInfo, "\\d{1,2}[:.]\\d{1,2}");
			String endTime = timeList.get(1);
			sb.append(timeList.get(0)).append("-");
			int index = endTime.indexOf(":");
			if (1 == index) {
				sb.append("0" + endTime);
			} else {
				sb.append(endTime);
			}
		} else {
			return timeTextInfo;
		}

		return sb.toString();
	}

	/**
	 * "下午17:00",上午10:00,晚上20:00"转为标准格式
	 * 
	 * @param timeTextInfo
	 * @return
	 */
	@SuppressWarnings("unused")
	private String cleanTime3(String timeTextInfo) {
		Matcher morningMat = Pattern.compile("上午\\d{1,2}[.:]\\d{1,2}").matcher(
				timeTextInfo);
		Matcher afternoonMat = Pattern.compile("下午\\d{1,2}[.:]\\d{1,2}")
				.matcher(timeTextInfo);
		Matcher nightPatMat = Pattern.compile("晚上\\d{1,2}[.:]\\d{1,2}")
				.matcher(timeTextInfo);

		// 处理"上午"
		while (morningMat.find()) {

		}

		return null;
	}

	/**
	 * 将类似"3月-10月"格式，进行标准化为"03月-10月"格式.
	 * 
	 * @param timeTextInfo
	 * @return
	 */
	@SuppressWarnings("unused")
	private String cleanTime4(String timeTextInfo) {
		Pattern monthPairPat = Pattern.compile("\\d{1,2}[-]\\d{1,2}月");
		Matcher matcher = monthPairPat.matcher(timeTextInfo);
		while (matcher.matches()) {

		}

		return null;
	}

	private void splitRawText(String rawText) {
		// System.out.println("正在解析:" + rawText);

		// 获取id.
		int index = rawText.indexOf(",");
		this.id = rawText.substring(1, index - 1);
		rawText = rawText.substring(index + 2);

		// 获取price.
		index = rawText.indexOf("\"");
		this.price = rawText.substring(0, index);
		if (index + 3 >= rawText.length()) {
			this.timeText = "NULL";
			return;
		}
		rawText = rawText.substring(index + 3);

		if (rawText.isEmpty()) {
			this.timeText = "NULL";
		} else {
			// 获取timeText.
			index = rawText.lastIndexOf("\"");
			this.timeText = rawText.substring(0, index);
		}
	}

	/**
	 * step4: 替换无效停用字符.
	 * 
	 * @param input
	 * @return
	 */
	private void replaceStops() {
		for (MiojiPair pair : replacePairList) {
			this.timeText = this.timeText.replaceAll(pair.start, pair.end);
		}

	}

	public String getId() {
		return this.id;
	}

	public String getPrice() {
		return this.price;
	}

	public String getTimeText() {
		return this.timeText;
	}

	@Override
	public String toString() {
		return "InputText [id=" + id + ", price=" + price + ", timeText="
				+ timeText + "]";
	}

	public static void main(String[] args) throws IOException {
		/*
		 * List<String> lines = FileUtils.readLines(new File("data/qyer2.csv"));
		 * Collections.sort(lines); List<String> cleanedLines = new
		 * ArrayList<String>(); SpotTimeText stt = null; for (String line :
		 * lines) { stt = new SpotTimeText(line); // cleanedLines.add("清洗前:\t" +
		 * line); cleanedLines.add(stt.getTimeText()); }
		 * Collections.sort(cleanedLines); FileUtils.writeLines(new
		 * File("data/cleaned_qyer2"), cleanedLines);
		 */

		// 将替换规则写入配置文件.
		List<String> replaceDictList = new ArrayList<String>();
		for (MiojiPair pair : replacePairList) {
			replaceDictList.add(pair.start + "\t" + pair.end);
		}

		FileUtils.writeLines(new File("dict/preprocess.dict"), replaceDictList);
	}

}
