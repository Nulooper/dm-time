package com.mioji.parse;

import com.mioji.ds.SpotRule;

/**
 * 处理景点开关时间信息为空的情况 .
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-8 下午8:37:04
 * 
 */
public final class DefaultParser implements Parsable {

	@Override
	public boolean isMatched(SpotTimeText stt) {
		if ("NULL".equals(stt.getTimeText())) {
			return true;
		}

		return false;
	}

	@Override
	public String parse(SpotTimeText stt) {
		return SpotRule.getEmptyFormat();
	}

}
