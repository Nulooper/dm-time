package com.mioji.parse;

/**
 * 
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年6月13日 上午11:30:31
 * 
 */
public class SubPattern {

	public static final String TIME_SUB_REGEX = "([0-9]{1,2}[.:][0-9]{1,2})";

	public static final String DAY_TIME_SUB_REGEX = "([0-9]{1,2}月[0-9]{1,2}日[-~至到][0-9]{1,2}月[0-9]{1,2}日[0-9]{1,2}[.:][0-9]{1,2}[-~至到][0-9]{1,2}[.:][0-9]{1,2})";

	public static final String WEEK_SUB_REGEX = "((周[0-9][-~至到和及]周[0-9]|周[0-9])[,.;:]?([0-9]{1,2}[.:][0-9]{1,2}[-~至到和及][0-9]{1,2}[.:][0-9]{1,2}[,;&]?)+[.]?)";

	public static final String MONTH_SUB_REGEX = "";

	public static final String QUARTER_SUB_REGEX = "";

	public static final String YEAR_SUB_REGEX = "";

	// 获取子串: XX月XX日.
	public static final String DAY_REGEX = "([0-9]{1,2}月[0-9]{1,2}日)";

	// 获取时间: xx:xx
	public static final String TIME_REGX = "([0-9]{1,2}[.:][0-9]{1,2})";
}
