package com.mioji.parse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年6月13日 上午11:24:43
 * 
 */
public class TimeSubExtractor {

	/**
	 * 根据regex从输入文本中抽取匹配的全部子串.
	 * 
	 * @param inputText
	 * @param regex
	 * @return
	 */
	public static List<String> extractByGroup(String inputText, String regex) {
		if (null == inputText || inputText.isEmpty() || null == regex
				|| regex.isEmpty()) {

			Logger.getGlobal().logp(Level.WARNING,
					TimeSubExtractor.class.getName(), "extract",
					"输入文本或者正则表达式不合法！");
			return Collections.emptyList();
		}
		List<String> subStrs = new ArrayList<String>();
		Pattern subPat = Pattern.compile(regex);
		Matcher matcher = subPat.matcher(inputText);

		while (matcher.find()) {
			subStrs.add(matcher.group());
			//System.out.println(matcher.group());
		}

		return subStrs;
	}

	public static void main(String[] args) {
		/*String text = "09:00~12:00&13:00~16:00";
		List<String> subs = TimeSubExtractor.extractByGroup(text,
				SubPattern.TIME_SUB_REGEX);
		for (String sub : subs) {
			System.out.println(sub);
		}*/
		
		String text = "周2-周510:00-17:00,周6-周711:00-17:00";
		List<String> subs = TimeSubExtractor.extractByGroup(text, SubPattern.WEEK_SUB_REGEX);
		for(String sub : subs){
			System.out.println(sub);
		}
	}

	/**
	 * 根据输入的正则表达式，迭代group id进行子串抽取.
	 * 
	 * @param inputText
	 * @param regex
	 * @return
	 */
	public static List<String> extractByGroupId(String inputText, String regex) {
		if (null == inputText || inputText.isEmpty() || null == regex
				|| regex.isEmpty()) {
			Logger.getGlobal().log(Level.WARNING,
					TimeSubExtractor.class.getName(), "输入参数不合法，抽取子串失败！返回空列表！");

			return Collections.emptyList();
		}

		List<String> subStrs = new ArrayList<String>();
		Pattern subPat = Pattern.compile(regex);
		Matcher matcher = subPat.matcher(inputText);

		while (matcher.find()) {
			int count = matcher.groupCount();
			for (int i = 1; i < count; ++i) {
				subStrs.add(matcher.group(i));
			}
		}

		return subStrs;
	}
}
