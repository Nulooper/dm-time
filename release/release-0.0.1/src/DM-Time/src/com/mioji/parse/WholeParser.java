package com.mioji.parse;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mioji.config.ConfigManager;
import com.mioji.config.ParserConfig;
import com.mioji.ds.CloseRule;
import com.mioji.ds.DayTimeRule;
import com.mioji.ds.MonthTimeRule;
import com.mioji.ds.OpenRule;
import com.mioji.ds.QuarterTimeRule;
import com.mioji.ds.SpotRule;
import com.mioji.ds.TimeRule;
import com.mioji.ds.WeekTimeRule;

/**
 * 处理时间信息为"全天"，"常年"，"全年"等情况.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-8 下午8:49:13
 * 
 */
public final class WholeParser implements Parsable {

	@Override
	public boolean isMatched(SpotTimeText stt) {
		ParserConfig pc = ConfigManager.getInstance().getParserConfig(
				WholeParser.class.getName());
		Pattern pattern = Pattern.compile(pc.patternList.get(0));

		Matcher matcher = pattern.matcher(stt.getTimeText());

		return matcher.matches();
	}

	@Override
	public String parse(SpotTimeText stt) {
		MonthTimeRule mtr = MonthTimeRule.createDefault();
		List<MonthTimeRule> mtrList = new ArrayList<MonthTimeRule>();
		mtrList.add(mtr);

		List<DayTimeRule> dtrList = new ArrayList<DayTimeRule>();
		List<WeekTimeRule> wtrList = new ArrayList<WeekTimeRule>();
		List<QuarterTimeRule> qtrList = new ArrayList<QuarterTimeRule>();

		OpenRule or = OpenRule.createInstance(TimeRule.createEmpty(), dtrList,
				wtrList, mtrList, qtrList);

		return SpotRule.createInstance(or, CloseRule.createEmpty()).toString();
	}

	public static void main(String[] args) {
		System.out.println(WholeParser.class.getName());
	}

}
