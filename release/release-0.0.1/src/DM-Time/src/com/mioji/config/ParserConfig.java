package com.mioji.config;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年6月9日 下午12:02:35
 * 
 */
public class ParserConfig {
	public String classname;
	public int level;
	public List<String> patternList;

	public ParserConfig(String classname, int level, List<String> patternList) {
		this.classname = classname;
		this.level = level;
		this.patternList = patternList;
	}

	public static ParserConfig createDefault() {
		List<String> patternList = new ArrayList<String>();
		patternList.add("");// wait to add.
		return new ParserConfig("com.mioji.parse.DefaultParser", 1, patternList);
	}

	@Override
	public String toString() {
		return "ParserConfig [classname=" + classname + ", level=" + level
				+ ", patternList=" + patternList + "]";
	}

}
