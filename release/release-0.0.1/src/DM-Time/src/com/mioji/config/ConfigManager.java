package com.mioji.config;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * 配置管理器, 遵循单例模式.负责程序的配置信息，主要包括模式的开关，添加;日志的开关.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-8 下午8:32:43
 * 
 */
public class ConfigManager {

	private static final String CONFIG_PATH = "conf/config.xml";

	private static ConfigManager instance = new ConfigManager();

	private Map<String, ParserConfig> parserConfigMap = new HashMap<String, ParserConfig>();

	private ConfigManager() {
		this.init();
	}

	@SuppressWarnings("unchecked")
	private void init() {
		try {
			SAXReader reader = new SAXReader();
			Document document = reader.read(new File(CONFIG_PATH));
			Element root = document.getRootElement();
			for (Iterator<Element> iter = root.elementIterator(); iter
					.hasNext();) {
				Element parser = iter.next();
				String classname = parser.attributeValue("classname");
				String level = parser.attributeValue("level");
				List<String> patternList = new ArrayList<String>();
				for (Iterator<Element> pIter = parser.elementIterator(); pIter
						.hasNext();) {
					Element patternParser = pIter.next();
					String pattern = patternParser.getText();
					patternList.add(pattern);
				}
				this.parserConfigMap.put(classname, new ParserConfig(classname,
						Integer.parseInt(level), patternList));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static ConfigManager getInstance() {
		return instance;
	}

	public List<String> getParserClassNameList() {
		return Collections.unmodifiableList(new ArrayList<String>(
				this.parserConfigMap.keySet()));
	}

	/**
	 * 根据完整的类名信息获取对应的Parser配置对象.
	 * 
	 * @param classname
	 *            Parser的完整類名.
	 * @return
	 */
	public ParserConfig getParserConfig(String classname) {
		if (null == classname || classname.isEmpty()) {
			Logger.getGlobal().logp(Level.WARNING,
					ConfigManager.class.getName(), "getParserConfig",
					"输入类名参数不合法！直接返回默认的ParserConfig对象！");
			return ParserConfig.createDefault();
		}

		if (!this.parserConfigMap.containsKey(classname)) {
			Logger.getGlobal().logp(Level.INFO, ConfigManager.class.getName(),
					"getParserConfig", "输入的类名不存在！获取失败！直接返回默认的ParserConfig对象！");
			return ParserConfig.createDefault();
		}

		return this.parserConfigMap.get(classname);
	}

	public static void main(String[] args) {
		System.out.println(ConfigManager.getInstance().getParserConfig(
				"com.mioji.parse.DayTimeParser"));
	}

}
