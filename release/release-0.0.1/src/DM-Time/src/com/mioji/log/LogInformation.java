package com.mioji.log;

/**
 * 打印日志信息，包括识别率，错误率，不能识别的模型写入指定日志文件.
 *
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年6月26日 下午12:05:33
 *
 */
public class LogInformation {
	
	public static void main(String[] args){
		System.out.println("log information !");
	}

}
