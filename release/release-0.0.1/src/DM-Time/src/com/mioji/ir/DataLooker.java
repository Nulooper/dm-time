package com.mioji.ir;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;

/**
 * 测试类，测试常见的语法和库.
 *
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年7月2日 下午12:38:04
 *
 */
public class DataLooker {

	public static void testTime() {
		DateTime dateTime = new DateTime(2014, 7, 2, 13, 3);
		dateTime.plusHours(8);
		System.out
				.println(dateTime.plusDays(90).toString("E yyyy/mm/dd HH:mm"));
		System.out.println(dateTime.getDayOfWeek());
	}

	public static void testRecog() throws IOException {
		List<String> lines = FileUtils.readLines(new File("data/recog.log"));
		for (String line : lines) {
			String[] items = line.split("\t");
			if (2 == items.length) {
				System.out.println(line);
			}
		}
	}

	public static void main(String[] args) throws IOException {
		// // testTime();
		//
		// DateTime dt = DateTime.now(DateTimeZone.UTC).plusHours(8);
		// System.out.println(dt.getDayOfMonth() + "\t" + dt.getDayOfWeek() +
		// "\t"
		// + dt.getDayOfYear() + "\t" + dt.getMonthOfYear());
		// System.out.println(dt);
		//
		// System.out.println(getDayOfWeek("20140702"));
		testRecog();
	}

	/**
	 * 
	 * @param date
	 *            格式为xxxxxxxx(0-3表示年,4-5表示月,6-7表示日)
	 * @return
	 */
	public static int getDayOfWeek(String date) {
		int year = Integer.parseInt(date.substring(0, 4));
		int month = Integer.parseInt(date.substring(4, 6));
		int day = Integer.parseInt(date.substring(6, 8));

		DateTime dt = new DateTime(year, month, day, 0, 0);

		return dt.getDayOfWeek();
	}
}
