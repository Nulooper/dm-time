package com.mioji.ir;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;

/**
 * 时间规则检索, 给定时间规则和指定日期(具体某一天, 格式为xxxx年xx月xx日), 返回预定义的时间(时间格式为xx:xx精确到分钟)格式文本.
 *
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年6月28日 下午3:41:18
 *
 */
public class IR {

	/*
	 * 预定义的返回时间文本1: NULL表示在输入的时间那一天, 该景点不开放.
	 */
	private static final String RETURN_CLOSE = "CLOSE";
	/*
	 * 预定义的返回时间文本2: UNKNOWN表示在输入的时间那一天, 该景点的开放信息未知.
	 */
	private static final String RETURN_UNKNOWN = "UNKNOWN";

	/*
	 * 预定的返回时间文本3: 表示在输入时间的那一天, 该景点全天开放, 以24小时格式返回.
	 */
	private static final String RETURN_OPEN = "00:00-24:00";

	/*
	 * 预定义的输入时间规则中open_rules和close_rules之间的间隔符号: "&&".
	 */
	private static final String SPLIT_RULE = "&&";

	/*
	 * 预定义时间规则(包括open_rules和close_rules)的类型.
	 */
	private static final String TYPE_WEEK_TIME = "WEEK_TIME_RULE";
	private static final String TYPE_ONLY_TIME = "TIME_RULE";
	private static final String TYPE_MONTH_TIME = "MONTH_TIME_RULE";

	/**
	 * 根据输入的时间规则文本，检测输入的具体时间(格式为xxxx年xx月xx日)是否开放，如果开放返回开放时间字符串，否则返回空串.
	 * 返回的开放时间字符串为: "xx:xx-xx:xx,xx:xx-xx:xx,..." 返回的空串为: "NULL".
	 * 
	 * @param rule
	 *            景点时间规则,景点时间规则包括open_rules和close_rules两部分，这两个规则之间使用"&&"连接.
	 * @param date
	 *            需要检测的日期格式为xxxx年xx月xx日
	 * @return 返回预定义的开放时间字符串或者空串.
	 */
	public static String search(String rule, String date) {
		if (null == rule || rule.isEmpty() || null == date || date.isEmpty()) {
			Logger.getGlobal().logp(Level.SEVERE, IR.class.getName(),
					"isOpened", "输入参数不合法！返回UNKNOWN!");

			return RETURN_UNKNOWN;
		}
		String[] ruleItems = rule.split(SPLIT_RULE);
		// 需要检测输入的时间规则文本格式是否合法.
		if (null == ruleItems || 0 == ruleItems.length) {
			Logger.getGlobal().logp(Level.WARNING, IR.class.getName(),
					"search", "输入时间规则文本不合法!返回UNKNOWN");

			return RETURN_UNKNOWN;
		}

		String openRule = ruleItems[0].substring(ruleItems[0].indexOf(":") + 1);
		System.out.println(openRule);
		String[] openItems = openRule.split("\t");
		System.out.println(openItems.length);
		String openText = openItems[0];
		String openType = openItems[1];

		String closeRule = ruleItems[1]
				.substring(ruleItems[1].indexOf(":") + 1);
		String[] closeItems = closeRule.split("\t");
		String closeText = closeItems[0];
		String closeType = closeItems[1];

		System.out.println("openType:" + openType + " closeType:" + closeType
				+ " " + "openText:" + openText + " closeText" + closeText);

		if ("ALWAYS".equals(openRule) && "NO".equals(closeRule)) {
			return RETURN_OPEN;
		} else if ("NO".equals(openRule) && "ALWAYS".equals(closeRule)) {
			return RETURN_CLOSE;
		} else if ("UNKNOWN".equals(openRule) && "UNKNOWN".equals(closeRule)) {
			return RETURN_UNKNOWN;
		} else {
			String dayOfWeek = getDayOfWeek(date);
			// step1: 分析开放规则, 返回时间格式文本.
			int monthOfDay = getMonthOfDay(date);
			System.out.println();
			if (TYPE_WEEK_TIME.equals(openType)) {
				return checkDayOfWeek(dayOfWeek, closeType, openText, closeText);
			} else if (TYPE_ONLY_TIME.equals(openType)) {
				return checkTime(dayOfWeek, closeType, openText, closeText);
			} else if (TYPE_MONTH_TIME.equals(openType)) {
				return checkMonthTime(monthOfDay, closeType, openText,
						closeText);
			}
		}

		return RETURN_UNKNOWN;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	// 解析WeekTimeRule.
	/**
	 * 计算给定的日期是周几，即一周的第几天.
	 * 
	 * @param date
	 *            xxxxxxxx, 其中0-3表示年, 4-5表示月, 6-7表示日
	 * @return
	 */
	public static String getDayOfWeek(String date) {
		if (null == date || date.isEmpty() || date.length() != 8) {
			Logger.getGlobal().logp(Level.SEVERE, IR.class.getName(),
					"getDayOfWeek", "输入日期格式不合法, 错误严重! 程序直接退出!", "date:" + date);
			System.out.println(date);
			System.exit(1);
		}

		int year = Integer.parseInt(date.substring(0, 4));
		int month = Integer.parseInt(date.substring(4, 6));
		int day = Integer.parseInt(date.substring(6, 8));

		DateTime dt = new DateTime(year, month, day, 0, 0);// 0,0分别对应hour,
															// minute,
															// 由于只需要计算输入的日期是一周的第几天，不需要时间，这里为了初始化DateTime，指定默认的hour和time.

		return "周" + dt.getDayOfWeek();
	}

	/**
	 * 检测给定的dayOfWeek，如果属于close_rules, 则返回RETURN_CLOSE; 否则以预定义的时间格式字符串返回开放时间.
	 * 
	 * @param dayOfWeek
	 * @return
	 */
	public static String checkDayOfWeek(String dayOfWeek, String closeType,
			String openWeekTimeRule, String closeWeekTimeRule) {
		// System.out.println(dayOfWeek + "\t" + closeType + "\t"
		// + openWeekTimeRule + "\t" + closeWeekTimeRule);
		if (TYPE_WEEK_TIME.equals(closeType)) {
			Map<String, List<String>> closeMap = getDayOfWeekTimesMap(closeWeekTimeRule);
			if (closeMap.containsKey(dayOfWeek)) {
				return RETURN_CLOSE;
			}
		}
		Map<String, List<String>> openMap = getDayOfWeekTimesMap(openWeekTimeRule);
		if (!openMap.containsKey(dayOfWeek)) {
			return RETURN_CLOSE;
		}

		return list2String(openMap.get(dayOfWeek), ",");
	}

	/**
	 * 根据给定的WeekTimeRule(周时间规则), 抽取一个key为day of week, value为xx:xx-xx:xx列表的map.
	 * 
	 * @param weekTimeRule
	 *            格式为:{周1-周5:07:00-21:00},{周6-周6:09:00-21:00},{周7-周7:09:00-14:00
	 *            },
	 *            或者{周1-周5:07:00-21:00},{周6-周6:09:00-21:00,22:00-23:50},{周7-周7
	 *            :09:00-14:00,15:00-18:00} 注意:每一个周区间(周x-周x)可能对应多个时间区间.
	 * @return
	 */
	public static Map<String, List<String>> getDayOfWeekTimesMap(
			String weekTimeRule) {
		if (null == weekTimeRule || weekTimeRule.isEmpty()) {
			Logger.getGlobal().logp(Level.SEVERE, IR.class.getName(),
					"getOpenTimesOfDay", "输入参数错误！错误严重！程序直接退出!");
			System.exit(1);
		}

		System.out.println("weekTimeRule:\t" + weekTimeRule);

		List<String> weekTimeList = splitWeekTimeRule(weekTimeRule);
		// key: 周x Value: xx:xx格式时间列表.
		Map<String, List<String>> dayOfWeekTimesMap = new HashMap<String, List<String>>();
		for (String weekTime : weekTimeList) {
			System.out.println("weekTime:\t" + weekTime);
			// 例如: 周4-周5
			String startDay = weekTime.substring(0, 2);
			String endDay = weekTime.substring(3, 5);

			System.out.println("startDay:" + startDay + ",\t" + "endDay:"
					+ endDay);

			String timeInfo = weekTime.substring(6, weekTime.length());
			System.out.println("timeInfo:\t" + timeInfo);
			String[] timePairs = timeInfo.split(",");
			List<String> timePairList = Arrays.asList(timePairs);
			if (startDay.equals(endDay)) {
				dayOfWeekTimesMap.put(startDay, timePairList);
			} else {
				int start = Integer.parseInt(startDay.substring(1));
				int end = Integer.parseInt(endDay.substring(1));
				if (start < end) {
					for (int i = start; i <= end; ++i) {
						dayOfWeekTimesMap.put("周" + i, timePairList);
					}
				} else {
					for (int j = start; j <= 7; ++j) {
						dayOfWeekTimesMap.put("周" + j, timePairList);
					}
					for (int m = 1; m <= end; ++m) {
						dayOfWeekTimesMap.put("周" + m, timePairList);
					}
				}
			}
		}

		return dayOfWeekTimesMap;
	}

	/**
	 * 将列表中元素以split隔开的字符串形式返回.
	 * 
	 * @param timePairList
	 * @param split
	 * @return
	 */
	public static String list2String(List<String> timePairList, String split) {
		if (null == timePairList || timePairList.isEmpty() || null == split) {
			Logger.getGlobal().logp(Level.SEVERE, IR.class.getName(),
					"list2String", "输入参数错误! 程序直接退出!");
			System.exit(1);
		}

		StringBuilder sb = new StringBuilder();
		int size = timePairList.size();
		if (1 == size) {
			return timePairList.get(0);
		} else {
			for (int i = 0; i < size - 1; ++i) {
				sb.append(timePairList.get(i)).append(split);
			}
			sb.append(timePairList.get(size - 1));
		}

		return sb.toString();
	}

	/**
	 * 将格式为{周1-周5:07:00-21:00},{周6-周6:09:00-21:00,22:00-23:50},{周7-周7:09:00-14:
	 * 00,15:00-18:00}的WeekTimeRule分割为周1-周5:07:00-21:00的列表.
	 * 
	 * @param weekTimeRule
	 * @return
	 */
	public static List<String> splitWeekTimeRule(String weekTimeRule) {
		if (null == weekTimeRule || weekTimeRule.isEmpty()) {
			Logger.getGlobal().logp(Level.SEVERE, IR.class.getName(),
					"splitWeekTimeRule", "输入weekTimeRule参数不合法!程序直接退出!");
			System.out.println("weekTimeRule:" + weekTimeRule);
			System.exit(1);
		}

		String[] times = weekTimeRule.split("\\{");
		List<String> timePairList = new ArrayList<String>();

		for (String time : times) {
			if (time.isEmpty()) {
				continue;
			}
			System.out.println(time);
			timePairList.add(time.substring(0, time.indexOf("}")));
		}

		return Collections.unmodifiableList(timePairList);
	}

	// /////////////////////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////////////////////
	// 解析TIME_RULE
	public static String checkTime(String dayOfWeek, String closeType,
			String openText, String closeText) {
		List<String> times = splitTimeRule(openText);
		if (TYPE_WEEK_TIME.equals(closeType)) {
			Map<String, List<String>> closeMap = getDayOfWeekTimesMap(closeText);
			if (closeMap.containsKey(dayOfWeek)) {
				return RETURN_CLOSE;
			}
			return list2String(times, ",");
		} else if ("NO".equals(closeType)) {
			return list2String(times, ",");
		}

		return RETURN_UNKNOWN;
	}

	/**
	 * 将时间规则open_rules:{10:00-17:00,18:00-00:00} 切分为xx:xx-xx:xx的列表.
	 * 
	 * @param TimeRule
	 * @return
	 */
	public static List<String> splitTimeRule(String timeRule) {
		if (null == timeRule || timeRule.isEmpty()) {
			Logger.getGlobal().logp(Level.SEVERE, IR.class.getName(),
					"splitTimeRule", "输入参数不合法!程序直接退出!");
			System.exit(1);
		}
		String cleanedTimeRule = timeRule.substring(1, timeRule.length() - 1);
		String[] timePairs = cleanedTimeRule.split(",");

		return Arrays.asList(timePairs);
	}

	// /////////////////////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////////////////////
	// 解析MONTH_TIME_RULE
	public static String checkMonthTime(int monthOfGivenDay, String closeType,
			String openText, String closeText) {
		Map<Integer, String> openMap = getMonthTimeMap(openText);
		if ("NO".equals(closeType)) {
			if (openMap.containsKey(monthOfGivenDay)) {
				return openMap.get(monthOfGivenDay);
			}
			return RETURN_CLOSE;
		}
		if (openMap.containsKey(monthOfGivenDay)) {
			return openMap.get(monthOfGivenDay);
		}
		return RETURN_CLOSE;
	}

	public static int getMonthOfDay(String date) {
		String month = date.substring(4, 6);

		return Integer.parseInt(month);
	}

	/**
	 * 
	 * @param monthTimeRule
	 *            格式为:{4月-10月:10:30-18:00}
	 * @return
	 */
	public static Map<Integer, String> getMonthTimeMap(String monthTimeRule) {
		String startMonth = monthTimeRule.substring(1,
				monthTimeRule.indexOf("月"));
		String endMonth = monthTimeRule.substring(
				monthTimeRule.indexOf("-") + 1, monthTimeRule.lastIndexOf("月"));
		int start = Integer.parseInt(startMonth);
		int end = Integer.parseInt(endMonth);
		String timePair = monthTimeRule.substring(
				monthTimeRule.indexOf(":") + 1, monthTimeRule.lastIndexOf("}"));
		Map<Integer, String> monthTimeMap = new HashMap<Integer, String>();
		if (start <= end) {
			for (int i = start; i <= end; ++i) {
				monthTimeMap.put(i, timePair);
			}
		} else {
			for (int j = 0; j <= 12; ++j) {
				monthTimeMap.put(j, timePair);
			}
			for (int m = 1; m <= end; ++m) {
				monthTimeMap.put(m, timePair);
			}
		}

		return Collections.unmodifiableMap(monthTimeMap);
	}

	public static void main(String[] args) throws IOException {
		Logger.getGlobal().setLevel(Level.ALL);

		String rule = "{10:00-17:00,18:00-00:00}";
		List<String> ruleItems = splitTimeRule(rule);
		for (String item : ruleItems) {
			System.out.println(item);
		}

		// testAllRecog();

	}

	private static void testAllRecog() throws IOException {
		List<String> recogLines = FileUtils
				.readLines(new File("data/recog.log"));
		int size = recogLines.size();
		for (int i = 0; i < size; i = i + 2) {
			System.out.println("开始处理第" + i + "景点信息!");
			String ruleText = recogLines.get(i) + "&&" + recogLines.get(i + 1);
			System.out.println(ruleText);
			System.out.println(search(ruleText, "20140702"));
			System.out.println("第" + i + "景点处理完成!");
		}
	}
}
