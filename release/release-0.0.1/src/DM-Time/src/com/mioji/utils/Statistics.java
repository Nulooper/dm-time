package com.mioji.utils;

/**
 * 定义了本系统的基本性能统计信息.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-9 上午11:11:19
 * 
 */
public class Statistics {
	// 输入的景点信息文本总数.
	private int totalCaseCount;
	// 正确识别的景点信息文本总数.
	private int recognizedCount;
	// 正确识别的比率.
	private double rate;

	public Statistics(int totalCaseCount, int recognizedCount) {
		this.totalCaseCount = totalCaseCount;
		this.recognizedCount = recognizedCount;
		this.rate = this.recognizedCount * 1.0 / this.totalCaseCount;
	}

	public int getTotalCaseCount() {
		return totalCaseCount;
	}

	public int getRecognizedCount() {
		return recognizedCount;
	}

	public double getRate() {
		return rate;
	}

	@Override
	public String toString() {
		return "景点时间规则识别模块统计信息 [输入总数: " + totalCaseCount + ", 识别总数: "
				+ recognizedCount + ", 识别比率: " + rate + "]";
	}
}
