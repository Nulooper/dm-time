package com.mioji.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Wang Zhiwei
 * 
 */
public class Constants {

	public static Map<String, String> replaceMap = new HashMap<String, String>();
	static {

		replaceMap.put("营业时间:\\s*", "");

		replaceMap.put("每日", "周1-周6");
		replaceMap.put("每天", "周1-周6");
		replaceMap.put("周末", "周6-周7");
		replaceMap.put("休息日", "周6-周7");
		replaceMap.put("工作日", "周1-周5");
		replaceMap.put("星期天", "周7");
		replaceMap.put("星期日", "周7");
		replaceMap.put("礼拜天", "周7");
		replaceMap.put("礼拜日", "周7");

		replaceMap.put("周一", "周1");
		replaceMap.put("周二", "周2");
		replaceMap.put("周三", "周3");
		replaceMap.put("周四", "周4");
		replaceMap.put("周五", "周5");
		replaceMap.put("周六", "周6");
		replaceMap.put("周日", "周7");

		replaceMap.put("，", ",");
		replaceMap.put("。", "");
		replaceMap.put("：", ":");
		replaceMap.put("；", ";");

		replaceMap.put("&ndash;", "-");
		replaceMap.put("&nbsp;", "");
		replaceMap.put("&amp;", "");
		replaceMap.put("(&mdash;){1,2}", "-");
		replaceMap.put("～", "-");
		replaceMap.put("&ldquo;", "\"");

		// /////////////////////////////////////////////////////
		// 天替换

		replaceMap.put("\b[mM]on[.]?\b", "周1");
		replaceMap.put("\b[tT]ues[.]?\b", "周2");
		replaceMap.put("\b[wW]ed[.]?\b", "周3");
		replaceMap.put("\b[tT]hur[.]?\b", "周4");
		replaceMap.put("\b[fF]ri[.]?\b", "周5");
		replaceMap.put("\b[sS]at[.]?\b", "周6");
		replaceMap.put("\b[sS]un[.]?\b", "周7");

		replaceMap.put("\b[Jj]an[.]?\b", "1月");
		replaceMap.put("\b[Ff]eb[.]?\b", "2月");
		replaceMap.put("\b[Mm]ar[.]?\b", "3月");
		replaceMap.put("\b[Aa]pr[.]?\b", "4月");
		replaceMap.put("\b[Mm]ay[.]?\b", "5月");
		replaceMap.put("\b[Jj]un[.]?\b", "6月");
		replaceMap.put("\b[Jj]ul[.]?\b", "7月");
		replaceMap.put("\b[Aa]ug[.]?\b", "8月");
		replaceMap.put("\b[Ss]ep[.]?\b", "9月");
		replaceMap.put("\b[Oo]ct[.]?\b", "10月");
		replaceMap.put("\b[Nn]ov[.]?\b", "11月");
		replaceMap.put("\b[Dd]ec[.]?\b", "12月");

		replaceMap.put("一月", "1月");
		replaceMap.put("二月", "2月");
		replaceMap.put("三月", "3月");
		replaceMap.put("四月", "4月");
		replaceMap.put("五月", "5月");
		replaceMap.put("六月", "6月");
		replaceMap.put("七月", "7月");
		replaceMap.put("八月", "8月");
		replaceMap.put("九月", "9月");
		replaceMap.put("十月", "10月");
		replaceMap.put("十一月", "11月");
		replaceMap.put("十二月", "12月");

		replaceMap.put("月份", "月");

		// ////////////////////////////////////////////////////
		// 时间替换
		replaceMap.put("日落", "17:00");
		replaceMap.put("黄昏", "17:00");
		replaceMap.put("半夜", "24:00");
		replaceMap.put("午夜", "24:00");
		replaceMap.put("全天", "00:00-24:00");
		replaceMap.put("白天", "07:00-17:00");
		replaceMap.put("全年全天", "00:00-24:00");
		replaceMap.put("全天开放", "00:00-24:00");
		replaceMap.put("24小时", "00:00-24:00");
		replaceMap.put("正午", "12:00");

		replaceMap.put("[0]?1\\s*[pP][mM]", "13:00");
		replaceMap.put("[0]?2\\s*[pP][mM]", "14:00");
		replaceMap.put("[0]?3\\s*[pP][mM]", "15:00");
		replaceMap.put("[0]?4\\s*[pP][mM]", "16:00");
		replaceMap.put("[0]?5\\s*[pP][mM]", "17:00");
		replaceMap.put("[0]?6\\s*[pP][mM]", "18:00");
		replaceMap.put("[0]?7\\s*[pP][mM]", "19:00");
		replaceMap.put("[0]?8\\s*[pP][mM]", "20:00");
		replaceMap.put("[0]?9\\s*[pP][mM]", "21:00");
		replaceMap.put("10\\s*[pP][mM]", "22:00");
		replaceMap.put("11\\s*[pP][mM]", "23:00");
		replaceMap.put("12\\s*[pP][mM]", "24:00");
		replaceMap.put("[0]?1\\s*[Aa][mM]", "01:00");
		replaceMap.put("[0]?2\\s*[Aa][mM]", "02:00");
		replaceMap.put("[0]?3\\s*[Aa][mM]", "03:00");
		replaceMap.put("[0]?4\\s*[Aa][mM]", "04:00");
		replaceMap.put("[0]?5\\s*[Aa][mM]", "05:00");
		replaceMap.put("[0]?6\\s*[Aa][mM]", "06:00");
		replaceMap.put("[0]?7\\s*[Aa][mM]", "07:00");
		replaceMap.put("[0]?8\\s*[Aa][mM]", "08:00");
		replaceMap.put("[0]?9\\s*[Aa][mM]", "09:00");
		replaceMap.put("10\\s*[Aa][mM]", "10:00");
		replaceMap.put("11\\s*[Aa][mM]", "11:00");
		replaceMap.put("12\\s*[Aa][mM]", "12:00");
	}

	public static void main(String[] args) {
		System.out.println(Constants.replaceMap.size());
		for (String value : Constants.replaceMap.values()) {
			System.out.println(value);
		}
	}

}
