package com.mioji.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年6月9日 下午4:34:47
 * 
 */
public class StringHelper {
	/**
	 * 半角转全角
	 * 
	 * @param input
	 *            String.
	 * @return 全角字符串.
	 */
	public static String toSBC(String input) {
		if (null == input || input.isEmpty()) {
			return "";
		}

		char c[] = input.toCharArray();
		for (int i = 0; i < c.length; i++) {
			if (c[i] == ' ') {
				c[i] = '\u3000';
			} else if (c[i] < '\177') {
				c[i] = (char) (c[i] + 65248);

			}
		}
		return new String(c);
	}

	/**
	 * 全角转半角
	 * 
	 * @param input
	 *            String.
	 * @return 半角字符串
	 */
	public static String toDBC(String input) {

		if (null == input || input.isEmpty()) {
			return "";
		}

		char c[] = input.toCharArray();
		for (int i = 0; i < c.length; i++) {
			if (c[i] == '\u3000') {
				c[i] = ' ';
			} else if (c[i] > '\uFF00' && c[i] < '\uFF5F') {
				c[i] = (char) (c[i] - 65248);

			}
		}

		return new String(c);
	}

	public static void main(String[] agrs) throws IOException {
		List<String> lines = FileUtils.readLines(new File("replcae"));
		List<String> fullLines = new ArrayList<String>();
		for (String line : lines) {
			fullLines.add(StringHelper.toSBC(line));
		}

		FileUtils.writeLines(new File("full.txt"), fullLines);

	}
}
