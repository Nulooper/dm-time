package com.mioji.ds;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-8 下午8:28:36
 * 
 */
public class WeekTimeRule {
	private MiojiPair weekPair;
	private List<MiojiPair> timePairList;

	public static WeekTimeRule createDefault() {
		List<MiojiPair> list = new ArrayList<MiojiPair>();
		list.add(MiojiPair.createDefaultOpenTimePair());

		return new WeekTimeRule(MiojiPair.createDefaultWeekPair(), list);
	}

	public static WeekTimeRule createInstance(MiojiPair weekPair,
			List<MiojiPair> timePairList) {
		return new WeekTimeRule(weekPair, timePairList);
	}

	public static WeekTimeRule createInstance(MiojiPair weekPair) {
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();

		return new WeekTimeRule(weekPair, timePairList);
	}

	public WeekTimeRule(MiojiPair weekPair, List<MiojiPair> timePairList) {
		this.weekPair = weekPair;
		this.timePairList = timePairList;
	}

	public MiojiPair getWeekPair() {
		return weekPair;
	}

	public List<MiojiPair> getTimePairList() {
		return Collections.unmodifiableList(this.timePairList);
	}

	public static String getEmptyFormat() {
		return "{" + "}";
	}

	@Override
	public String toString() {
		String weekPair = this.weekPair.toString();
		if (timePairList.isEmpty()) {
			return "{" + weekPair + ":"
					+ MiojiPair.createDefaultOpenTimePair().toString() + "}";
		}

		String timePairs = StringUtils.join(this.timePairList.iterator(), ",");

		return "{" + weekPair + ":" + timePairs + "}";
	}

	public static void main(String[] args) {
		System.out.println(WeekTimeRule.getEmptyFormat());
	}

}
