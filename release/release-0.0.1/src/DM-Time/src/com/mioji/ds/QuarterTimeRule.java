package com.mioji.ds;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-8 下午8:30:32
 * 
 */
public class QuarterTimeRule {
	private MiojiPair quarterPair;
	private List<MiojiPair> timePairList;

	public static QuarterTimeRule createDefault() {
		List<MiojiPair> list = new ArrayList<MiojiPair>();
		list.add(MiojiPair.createDefaultTimePair());

		return new QuarterTimeRule(MiojiPair.createDefaultQuarterPair(), list);
	}

	public static QuarterTimeRule createInstance(MiojiPair quarterPair,
			List<MiojiPair> timePairList) {
		return new QuarterTimeRule(quarterPair, timePairList);
	}

	public QuarterTimeRule(MiojiPair quarterPair, List<MiojiPair> timePairList) {
		this.quarterPair = quarterPair;
		this.timePairList = timePairList;
	}

	public MiojiPair getQuarterPair() {
		return quarterPair;
	}

	public List<MiojiPair> getTimePairList() {
		return Collections.unmodifiableList(this.timePairList);
	}

	public static String getEmptyFormat() {
		return "{}";
	}

	@Override
	public String toString() {
		String quarter = this.quarterPair.toString();
		String times = StringUtils.join(this.timePairList.iterator(), ",");

		return "{" + quarter + ":" + times + "}";
	}

	public static void main(String[] args) {
		System.out.println(QuarterTimeRule.createDefault());
	}

}
