package com.mioji.ds;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * 描述了景点的开放时间规则.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-8 下午7:22:38
 * 
 */
public class CloseRule {
	private TimeRule timeRule;
	private List<DayTimeRule> dayTimeRuleList;
	private List<WeekTimeRule> weekTimeRuleList;
	private List<MonthTimeRule> monthTimeRuleList;
	private List<QuarterTimeRule> quarterTimeRuleList;

	public static CloseRule createDefault() {
		List<DayTimeRule> dayTimeRuleList = new ArrayList<DayTimeRule>();
		dayTimeRuleList.add(DayTimeRule.createDefault());
		dayTimeRuleList.add(DayTimeRule.createDefault());
		List<WeekTimeRule> weekTimeRuleList = new ArrayList<WeekTimeRule>();
		weekTimeRuleList.add(WeekTimeRule.createDefault());
		weekTimeRuleList.add(WeekTimeRule.createDefault());
		List<MonthTimeRule> monthTimeRuleList = new ArrayList<MonthTimeRule>();
		monthTimeRuleList.add(MonthTimeRule.createDefault());
		monthTimeRuleList.add(MonthTimeRule.createDefault());
		List<QuarterTimeRule> quarterTimeRuleList = new ArrayList<QuarterTimeRule>();
		quarterTimeRuleList.add(QuarterTimeRule.createDefault());
		quarterTimeRuleList.add(QuarterTimeRule.createDefault());

		return new CloseRule(TimeRule.createDefault(), dayTimeRuleList,
				weekTimeRuleList, monthTimeRuleList, quarterTimeRuleList);
	}

	public static CloseRule createEmpty() {
		List<DayTimeRule> dayTimeRuleList = new ArrayList<DayTimeRule>();
		List<WeekTimeRule> weekTimeRuleList = new ArrayList<WeekTimeRule>();
		List<MonthTimeRule> monthTimeRuleList = new ArrayList<MonthTimeRule>();
		List<QuarterTimeRule> quarterTimeRuleList = new ArrayList<QuarterTimeRule>();

		return new CloseRule(TimeRule.createEmpty(), dayTimeRuleList,
				weekTimeRuleList, monthTimeRuleList, quarterTimeRuleList);
	}

	public static CloseRule createInstance(TimeRule timeRule,
			List<DayTimeRule> dayTimeRuleList,
			List<WeekTimeRule> weekTimeRuleList,
			List<MonthTimeRule> monthTimeRuleList,
			List<QuarterTimeRule> quarterTimeRuleList) {

		return new CloseRule(timeRule, dayTimeRuleList, weekTimeRuleList,
				monthTimeRuleList, quarterTimeRuleList);

	}

	public CloseRule(TimeRule timeRule, List<DayTimeRule> dayTimeRuleList,
			List<WeekTimeRule> weekTimeRuleList,
			List<MonthTimeRule> monthTimeRuleList,
			List<QuarterTimeRule> quarterTimeRuleList) {

		this.timeRule = timeRule;
		this.dayTimeRuleList = dayTimeRuleList;
		this.weekTimeRuleList = weekTimeRuleList;
		this.monthTimeRuleList = monthTimeRuleList;
		this.quarterTimeRuleList = quarterTimeRuleList;
	}

	public TimeRule getTimeRule() {
		return timeRule;
	}

	public List<DayTimeRule> getDayTimeRuleList() {
		return Collections.unmodifiableList(dayTimeRuleList);
	}

	public List<WeekTimeRule> getWeekTimeRuleList() {
		return Collections.unmodifiableList(weekTimeRuleList);
	}

	public List<MonthTimeRule> getMonthTimeRuleList() {
		return Collections.unmodifiableList(monthTimeRuleList);
	}

	public List<QuarterTimeRule> getQuarterTimeRuleList() {
		return Collections.unmodifiableList(quarterTimeRuleList);
	}

	public static String getEmptyFormat() {
		String times = "<time_rules:" + TimeRule.getEmptyFormat() + ">";
		String dayTimes = "<day_time_ruls:[" + "]>";
		String weekTimes = "<week_time_rules:[" + "]>";
		String monthTimes = "<month_time_rules:[" + "]>";
		String quarterTimes = "<quarter_time_rules:[" + "]>";

		return "close_rules:" + times + dayTimes + weekTimes + monthTimes
				+ quarterTimes;
	}

	@Override
	public String toString() {
		if (!this.timeRule.isEmpty()) {
			return "close_rules:"
					+ StringUtils.join(this.dayTimeRuleList.iterator(), ",")
					+ "\t" + "TIME_RULE";
		} else if (!this.dayTimeRuleList.isEmpty()) {
			return "close_rules:"
					+ StringUtils.join(this.dayTimeRuleList.iterator(), ",")
					+ "\t" + "DAY_TIME_RULE";
		} else if (!this.weekTimeRuleList.isEmpty()) {
			return "close_rules:"
					+ StringUtils.join(this.weekTimeRuleList.iterator(), ",")
					+ "\t" + "WEEK_TIME_RULE";
		} else if (!this.monthTimeRuleList.isEmpty()) {
			return "close_rules:"
					+ StringUtils.join(this.monthTimeRuleList.iterator(), ",")
					+ "\t" + "MONTH_TIME_RULE";
		} else if (!this.quarterTimeRuleList.isEmpty()) {
			return "close_rules:"
					+ StringUtils
							.join(this.quarterTimeRuleList.iterator(), ",")
					+ "\t" + "QUARTER_TIMES";
		} else {
			return "close_rules:" + "NO" + "\tNO";
		}
	}

	public static void main(String[] args) {
		System.out.println(CloseRule.getEmptyFormat());
	}
}