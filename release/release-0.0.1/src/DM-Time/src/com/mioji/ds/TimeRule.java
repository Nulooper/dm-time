package com.mioji.ds;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年6月9日 下午1:45:50
 * 
 */
public class TimeRule {
	private List<MiojiPair> timePairList;

	public static TimeRule createDefault() {
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		timePairList.add(MiojiPair.createDefaultTimePair());

		return new TimeRule(timePairList);
	}

	public static TimeRule createWholeDay() {
		return TimeRule.createDefault();
	}

	public static TimeRule createWhiteDay() {
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		timePairList.add(MiojiPair.createWhiteDayPair());

		return new TimeRule(timePairList);
	}

	public static TimeRule createEmpty() {
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();

		return new TimeRule(timePairList);
	}

	public static TimeRule createDefaultOpen() {
		List<MiojiPair> timePairList = new ArrayList<MiojiPair>();
		timePairList.add(MiojiPair.createDefaultOpenTimePair());

		return new TimeRule(timePairList);
	}

	public static TimeRule createInstance(List<MiojiPair> timePairList) {
		return new TimeRule(timePairList);

	}

	public TimeRule(List<MiojiPair> timePairList) {
		this.timePairList = timePairList;
	}

	public List<MiojiPair> getTimePairList() {
		return timePairList;
	}

	public static String getEmptyFormat() {
		return "[]";
	}

	public boolean isEmpty() {
		return this.timePairList.isEmpty();
	}

	@Override
	public String toString() {
		return "{" + StringUtils.join(this.timePairList.iterator(), ",") + "}";

	}

	public static void main(String[] args) {
		System.out.println(TimeRule.createDefault());
	}

}
