package com.mioji.ds;

/**
 * 本系统的最终结果描述，描述了景点开放时间日期的规则.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年6月9日 下午2:22:55
 * 
 */
public class SpotRule {
	private OpenRule openRule;
	private CloseRule closeRule;

	public static SpotRule createDefault() {
		return new SpotRule(OpenRule.createDefault(), CloseRule.createDefault());
	}

	public static SpotRule createInstance(OpenRule openRule, CloseRule closeRule) {
		return new SpotRule(openRule, closeRule);
	}

	public SpotRule(OpenRule openRule, CloseRule closeRule) {
		this.openRule = openRule;
		this.closeRule = closeRule;
	}

	public OpenRule getOpenRule() {
		return this.openRule;
	}

	public CloseRule getCloseRule() {
		return this.closeRule;
	}

	public static String getEmptyFormat() {
		return OpenRule.getEmptyFormat() + "\n" + CloseRule.getEmptyFormat();
	}

	public static String getUnknown() {
		return "open_rules:UNKNOWN" + "\tUNKNOWN" + "\n"
				+ "close_rules:UNKNOWN" + "\tUNKNOWN";
	}

	public static String getAlwaysClose() {
		return "open_rules:NO" + "\tNO" + "\n" + "close_rules:ALWAYS"
				+ "\tALWAYS";
	}

	public static String getAlwaysOpen() {
		return "open_rules:ALWAYS" + "\tALWAYS" + "\n" + "close_rules:NO"
				+ "\tNO";
	}

	@Override
	public String toString() {
		return this.openRule.toString() + "\n" + this.closeRule.toString();
	}

	public static void main(String[] args) {
		System.out.println(SpotRule.getEmptyFormat());
	}

}
