package com.mioji.ds;

/**
 * 表示区间信息.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年6月9日 下午1:48:01
 * 
 */
public class MiojiPair {
	public String start;
	public String end;

	public static MiojiPair createDefaultTimePair() {
		return new MiojiPair("00:00", "24:00");
	}

	public static MiojiPair createDefaultOpenTimePair() {
		return MiojiPair.createInstance("08:00", "18:00");
	}

	public static MiojiPair createWhiteDayPair() {
		return new MiojiPair("06:00", "18:00");
	}

	public static MiojiPair createDefaultDayPair() {
		return new MiojiPair("01月01日", "12月31日");
	}

	public static MiojiPair createDefaultWeekPair() {
		return new MiojiPair("周1", "周7");
	}

	public static MiojiPair createDefaultMonthPair() {
		return new MiojiPair("01月", "12月");
	}

	public static MiojiPair createDefaultQuarterPair() {
		return new MiojiPair("春季", "冬季");
	}

	public static MiojiPair createInstance(String start, String end) {
		return new MiojiPair(start, end);
	}

	public MiojiPair(String start, String end) {
		this.start = start;
		this.end = end;
	}

	@Override
	public String toString() {
		return start + "-" + end;
	}

}
