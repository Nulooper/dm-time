package com.mioji.ds;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014-6-8 下午7:36:50
 * 
 */
public class DayTimeRule {
	private MiojiPair dayPair;
	private List<MiojiPair> timePairList;

	public static DayTimeRule createDefault() {
		List<MiojiPair> list = new ArrayList<MiojiPair>();
		list.add(MiojiPair.createDefaultTimePair());

		return new DayTimeRule(MiojiPair.createDefaultDayPair(), list);
	}

	public static DayTimeRule createDefaultDay() {
		List<MiojiPair> list = new ArrayList<MiojiPair>();

		return new DayTimeRule(MiojiPair.createDefaultDayPair(), list);
	}

	public static DayTimeRule createInstance(MiojiPair dayPair) {
		List<MiojiPair> list = new ArrayList<MiojiPair>();

		return new DayTimeRule(dayPair, list);
	}

	public static DayTimeRule createInstance(MiojiPair dayPair,
			List<MiojiPair> timePairList) {
		return new DayTimeRule(dayPair, timePairList);
	}

	public DayTimeRule(MiojiPair dayPair, List<MiojiPair> timePairList) {
		this.dayPair = dayPair;
		this.timePairList = timePairList;
	}

	public MiojiPair getDayPair() {
		return dayPair;
	}

	public List<MiojiPair> getTimePairList() {
		return Collections.unmodifiableList(timePairList);
	}

	public String getEmptyFormat() {
		return "{}";
	}

	@Override
	public String toString() {
		String day = this.dayPair.toString();
		String times = StringUtils.join(this.timePairList.iterator(), ",");

		if (times.isEmpty()) {
			return "{" + day + "}";
		}

		return "{" + day + ":" + times + "}";
	}

	public static void main(String[] args) {
		System.out.println(DayTimeRule.createDefaultDay());
	}

}
