package com.mioji.miner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mioji.ds.MiojiPair;

/**
 * 
 *
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年6月28日 下午4:51:21
 *
 */
public class OnlyTimeCleaner {
	private static final Pattern PAT_1 = Pattern
			.compile("^\\d{1,2}[ap][m][-至到达和及]\\d{1,2}[ap][m]$");
	public static final Map<String, String> MAP_1 = new HashMap<String, String>();
	static {
		MAP_1.put("[0]?11pm", "23:00");
		MAP_1.put("[0]?1am", "01:00");
		MAP_1.put("[0]?2am", "02:00");
		MAP_1.put("[0]?3am", "03:00");
		MAP_1.put("[0]?4am", "04:00");
		MAP_1.put("[0]?5am", "05:00");
		MAP_1.put("[0]?6am", "06:00");
		MAP_1.put("[0]?7am", "07:00");
		MAP_1.put("[0]?8am", "08:00");
		MAP_1.put("[0]?9am", "09:00");
		MAP_1.put("[0]?10am", "10:00");
		MAP_1.put("[0]?11am", "11:00");
		MAP_1.put("[0]?1pm", "13:00");
		MAP_1.put("[0]?2pm", "14:00");
		MAP_1.put("[0]?3pm", "15:00");
		MAP_1.put("[0]?4pm", "16:00");
		MAP_1.put("[0]?5pm", "17:00");
		MAP_1.put("[0]?6pm", "18:00");
		MAP_1.put("[0]?7pm", "19:00");
		MAP_1.put("[0]?8pm", "20:00");
		MAP_1.put("[0]?9pm", "21:00");
		MAP_1.put("[0]?10pm", "22:00");

	}

	private static final Pattern PAT_2 = Pattern
			.compile("(每日|每天|营业时间|开放时间)[:]?\\d{1,2}[.:]\\d{1,2}-\\d{1,2}[.:]\\d{1,2}");
	private static final List<MiojiPair> MAP_LIST_2 = new ArrayList<MiojiPair>();
	static {
		MAP_LIST_2.add(MiojiPair.createInstance("每天:", ""));
		MAP_LIST_2.add(MiojiPair.createInstance("每天", ""));
		MAP_LIST_2.add(MiojiPair.createInstance("每日:", ""));
		MAP_LIST_2.add(MiojiPair.createInstance("每日", ""));
		MAP_LIST_2.add(MiojiPair.createInstance("营业时间	:", ""));
		MAP_LIST_2.add(MiojiPair.createInstance("营业时间", ""));
		MAP_LIST_2.add(MiojiPair.createInstance("开放时间:", ""));
		MAP_LIST_2.add(MiojiPair.createInstance("开放时间", ""));
	}

	/**
	 * 检查格式:7am-10pm或7AM-10PM
	 * 
	 * @param timeTextInfo
	 * @return
	 */
	public static boolean check1(String timeTextInfo) {
		Matcher matcher = PAT_1.matcher(timeTextInfo);

		return matcher.matches();
	}

	/**
	 * 7am-10pm或7AM-10PM
	 */
	public static String doClean1(String timeTextInfo) {
		for (String regex : MAP_1.keySet()) {
			timeTextInfo = timeTextInfo.replaceAll(regex, MAP_1.get(regex));
		}
		return timeTextInfo;
	}

	/**
	 * 检查格式:每天8:00-16:00或者每日8:00-16:00或者营业时间:8:00-16:00
	 * 
	 * @param timeTextInfo
	 * @return
	 */
	public static boolean check2(String timeTextInfo) {
		Matcher matcher = PAT_2.matcher(timeTextInfo);
		return matcher.matches();
	}

	public static String doClean2(String timeTextInfo) {
		for (MiojiPair pair : MAP_LIST_2) {
			timeTextInfo = timeTextInfo.replaceAll(pair.start, pair.end);
		}
		return timeTextInfo;
	}

	public static boolean check3(String timeTextInfo) {
		return false;
	}

	public static String doClean3(String timeTextInfo) {
		return null;
	}

	public static void main(String[] args) {
		String text = "8am-6pm";
		System.out.println(check1(text));
	}
}
