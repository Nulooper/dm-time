package com.mioji.miner;

import java.util.ArrayList;
import java.util.List;

import com.mioji.ds.MiojiPair;
import com.mioji.parse.SpotTimeText;

public class WeekTimeCleaner {
	private static final List<MiojiPair> replacePairList = new ArrayList<MiojiPair>();
	static {
		replacePairList.add(MiojiPair.createInstance("周末", "周6-周7"));
		replacePairList.add(MiojiPair.createInstance("休息日", "周6-周7"));
		replacePairList.add(MiojiPair.createInstance("星期天", "周7"));
		replacePairList.add(MiojiPair.createInstance("星期日", "周7"));
		replacePairList.add(MiojiPair.createInstance("礼拜天", "周7"));
		replacePairList.add(MiojiPair.createInstance("礼拜日", "周7"));

		replacePairList.add(MiojiPair.createInstance("星期一", "周1"));
		replacePairList.add(MiojiPair.createInstance("星期二", "周2"));
		replacePairList.add(MiojiPair.createInstance("星期三", "周3"));
		replacePairList.add(MiojiPair.createInstance("星期四", "周4"));
		replacePairList.add(MiojiPair.createInstance("星期五", "周5"));
		replacePairList.add(MiojiPair.createInstance("星期六", "周6"));

		replacePairList.add(MiojiPair.createInstance("礼拜一", "周1"));
		replacePairList.add(MiojiPair.createInstance("礼拜二", "周2"));
		replacePairList.add(MiojiPair.createInstance("礼拜三", "周3"));
		replacePairList.add(MiojiPair.createInstance("礼拜四", "周4"));
		replacePairList.add(MiojiPair.createInstance("礼拜五", "周5"));
		replacePairList.add(MiojiPair.createInstance("礼拜六", "周6"));

		replacePairList.add(MiojiPair.createInstance("周一", "周1"));
		replacePairList.add(MiojiPair.createInstance("周二", "周2"));
		replacePairList.add(MiojiPair.createInstance("周三", "周3"));
		replacePairList.add(MiojiPair.createInstance("周四", "周4"));
		replacePairList.add(MiojiPair.createInstance("周五", "周5"));
		replacePairList.add(MiojiPair.createInstance("周六", "周6"));
		replacePairList.add(MiojiPair.createInstance("周日", "周7"));

	}

	public static SpotTimeText preClean(SpotTimeText stt) {
		String timeText = stt.getTimeText();
		for (MiojiPair regex : replacePairList) {
			timeText = timeText.replaceAll(regex.start, regex.end);
		}

		return SpotTimeText.createInstance(stt.getId(), stt.getPrice(),
				timeText);
	}

	public static boolean check1(String timeTextInfo) {
		return false;
	}

	public static String clean1(String timeTextInfo) {
		return null;
	}

	public static boolean check2(String timeTextInfo) {
		return false;
	}

	public static String clean2(String timeTextInfo) {
		return null;
	}

	public static boolean check3(String timeTextInfo) {
		return false;
	}

	public static String clean3(String timeTextInfo) {
		return null;
	}

	public static boolean check4(String timeTextInfo) {
		return false;
	}

	public static String clean4(String timeTextInfo) {
		return null;
	}
}
