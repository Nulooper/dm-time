package com.mioji.miner;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

/**
 * 
 *
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年6月28日 下午5:44:11
 *
 */
public class Miner {

	public static void mine(String srcPath) {
		try {
			Pipeline2 pipeLine = new Pipeline2();
			List<String> lines = FileUtils.readLines(new File(srcPath));
			for (String line : lines) {
				pipeLine.parse(line);
			}
			FileUtils.writeLines(new File("data/recog.log"),
					pipeLine.getRuleList());

			System.out.println("识别率:\t" + pipeLine.getRecogRate());
			// 打印详细信息
			Map<String, Integer> patterMatchMap = pipeLine.getMatchMap();
			for (String pattern : patterMatchMap.keySet()) {
				System.out.println("匹配" + pattern + ":\t"
						+ patterMatchMap.get(pattern));
			}
			System.out.println("识别数量:\t" + pipeLine.getRecogCount());
			System.out.println("总共数量:\t" + pipeLine.getTotalCount());
			//
			for (String parserName : pipeLine.getRecogMap().keySet()) {
				System.out.println(parserName + "识别数量:\t"
						+ pipeLine.getRecogMap().get(parserName));
			}
			FileUtils.writeLines(new File("data/unrecog.log"),
					pipeLine.getUnrecognizedList());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Miner.mine("data/qyer2.csv");
	}

}
