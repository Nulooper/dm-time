package com.mioji.miner;

import java.util.regex.Pattern;

/**
 * 
 *
 * @author Wang Zhiwei
 * @version 0.0.1
 * @datetime 2014年6月28日 下午4:51:29
 *
 */
public class PatternManager {
	public static final Pattern ONLY_TIME_PAT = Pattern
			.compile("^(一般店铺|一般商铺|全年|推荐|全年开放|一般为|各商铺营业时间不同,一般为|各层时间各不相同大致是|每天开放|每天早上|营业时间:从|营业时间:全年|需预约,|每日开放|每日约|开放时间|)[,:,]?\\d{1,2}[.:]\\d{1,2}([-–—到至达和及]|营业到|开放到)\\d{1,2}[.:]\\d{1,2}[.,;]?(全年无休|全年开放|无休|周末不休息|无休息日|最好提前预订|)$");

	public static final Pattern TIME_BLACK_DAY_PAT = Pattern
			.compile("^\\d{1,2}[:.]\\d{1,2}[-–—到至达和及]\\d{1,2}[.:]\\d{1,2}[,;.]?[\\(]?周\\d(歇业|关门休息|休息|闭馆|不开放|不接待游客|不对外开放|休业|不開放|关闭|休馆|不营业|关店|店休)[\\)]?[.]?$");

	public static final Pattern ONLY_MONTH_PAIR_PAT = Pattern
			.compile("^\\d{1,2}月[-至到达和及]\\d{1,2}月$");

	public static final Pattern ONLY_WEEK_PAIR_PAT = Pattern
			.compile("^周\\d[-–—到至达和及]周\\d$");

	public static final Pattern WEEK_N_TIMES_PAT = Pattern
			.compile("^(营业时间|)[:;]?周\\d[-–—到至达和及]周\\d[,;]?(\\d{1,2}[:.]\\d{1,2}[-–—到至达和及]\\d{1,2}[.:]\\d{1,2}[,;.]?)+$");

	/**
	 * 营业时间:周1至周5:从07:00至20:00周6:从07:00至20:00周7:从07:00至20:00
	 */
	public static final Pattern DETAILED_ALL_WEEKDAY_PAT = Pattern
			.compile("^(营业时间|)[:]?周\\d[-–—到至达和及]周\\d[.:,]?[从]?\\d{1,2}[:.]\\d{1,2}[-–—到至达和及]\\d{1,2}[.:]\\d{1,2}周\\d[.:,]?[从]?\\d{1,2}[:.]\\d{1,2}[-–—到至达和及]\\d{1,2}[.:]\\d{1,2}周\\d[.,;:]?[从]?\\d{1,2}[:.]\\d{1,2}[-–—到至达和及]\\d{1,2}[.:]\\d{1,2}[.;]?$");

	/**
	 * 营业时间:周1至周5:从08:00至18:00周6:从08:00至18:00周7:休息
	 */
	public static final Pattern WEEKEND_WORK_PAT = Pattern
			.compile("^(营业时间|)[:]?周\\d[-–—到至达和及]周\\d[.:,]?[从]?\\d{1,2}[:.]\\d{1,2}[-–—到至达和及]\\d{1,2}[.:]\\d{1,2}周\\d[.:,]?[从]?\\d{1,2}[:.]\\d{1,2}[-–—到至达和及]\\d{1,2}[.:]\\d{1,2}周\\d[:]?休息$");

	/**
	 * 10:00-次日3:00
	 */
	public static final Pattern TIME_NEXT_DAY_TIME = Pattern
			.compile("\\d{1,2}[.:]\\d{1,2}[-–—到至达和及]次日");

	public static final Pattern MONTH_TIME_PAT = Pattern
			.compile("\\d{1,2}月[-–—到至达和及]\\d{1,2}月[.:;]?\\d{1,2}[:.]\\d{1,2}[-]\\d{1,2}[:.]\\d{1,2}");

	/**
	 * 11:30-14:30,17:30-21:30;周3休息
	 */
	public static final Pattern N_TIMES_WEEK_CLOSE_PAT = Pattern
			.compile("(\\d{1,2}[.:;]\\d{1,2}[-–—到至达和及]\\d{1,2}[.:;]\\d{1,2}[.;,]?)+[;]?周\\d(休息)");
}
